package org.uniprot.purl.core.domain;

import java.lang.Integer;
import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://purl.uniprot.org/core/ ontology
 */
public interface Taxon extends OWLThing {
  String getSynonym();

  void setSynonym(String val);

  Taxon getSubClassOf();

  void setSubClassOf(Taxon val);

  Integer getTaxid();

  void setTaxid(Integer val);

  String getScientificName();

  void setScientificName(String val);

  String getRank();

  void setRank(String val);
}
