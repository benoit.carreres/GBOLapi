package org.uniprot.purl.core.domain.impl;

import java.lang.Integer;
import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.uniprot.purl.core.domain.Taxon;

/**
 * Code generated from http://purl.uniprot.org/core/ ontology
 */
public class TaxonImpl extends OWLThingImpl implements Taxon {
  public static final String TypeIRI = "http://purl.uniprot.org/core/Taxon";

  protected TaxonImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Taxon make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Taxon.class);
      if(toRet == null) {
        toRet = new TaxonImpl(domain,iri);;
      }
      else if(!(toRet instanceof Taxon)) {
        throw new RuntimeException("Instance of org.uniprot.purl.core.domain.impl.TaxonImpl expected");
      }
      return (Taxon)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#taxid");
  }

  public String getSynonym() {
    return this.getStringLit("http://purl.uniprot.org/core/synonym",true);
  }

  public void setSynonym(String val) {
    this.setStringLit("http://purl.uniprot.org/core/synonym",val);
  }

  public Taxon getSubClassOf() {
    return this.getRef("http://www.w3.org/2000/01/rdf-schema#subClassOf",true,Taxon.class);
  }

  public void setSubClassOf(Taxon val) {
    this.setRef("http://www.w3.org/2000/01/rdf-schema#subClassOf",val,Taxon.class);
  }

  public Integer getTaxid() {
    return this.getIntegerLit("http://gbol.life#taxid",false);
  }

  public void setTaxid(Integer val) {
    this.setIntegerLit("http://gbol.life#taxid",val);
  }

  public String getScientificName() {
    return this.getStringLit("http://purl.uniprot.org/core/scientificName",true);
  }

  public void setScientificName(String val) {
    this.setStringLit("http://purl.uniprot.org/core/scientificName",val);
  }

  public String getRank() {
    return this.getExternalRef("http://purl.uniprot.org/core/rank",true);
  }

  public void setRank(String val) {
    this.setExternalRef("http://purl.uniprot.org/core/rank",val);
  }
}
