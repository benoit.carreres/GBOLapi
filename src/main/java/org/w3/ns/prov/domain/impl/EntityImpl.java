package org.w3.ns.prov.domain.impl;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.w3.ns.prov.domain.Activity;
import org.w3.ns.prov.domain.Agent;
import org.w3.ns.prov.domain.Entity;

/**
 * Code generated from http://www.w3.org/ns/prov# ontology
 */
public class EntityImpl extends OWLThingImpl implements Entity {
  public static final String TypeIRI = "http://www.w3.org/ns/prov#Entity";

  protected EntityImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Entity make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Entity.class);
      if(toRet == null) {
        toRet = new EntityImpl(domain,iri);;
      }
      else if(!(toRet instanceof Entity)) {
        throw new RuntimeException("Instance of org.w3.ns.prov.domain.impl.EntityImpl expected");
      }
      return (Entity)toRet;
    }
  }

  public void validate() {
  }

  public Activity getWasGeneratedBy() {
    return this.getRef("http://www.w3.org/ns/prov#wasGeneratedBy",true,Activity.class);
  }

  public void setWasGeneratedBy(Activity val) {
    this.setRef("http://www.w3.org/ns/prov#wasGeneratedBy",val,Activity.class);
  }

  public Agent getWasAttributedTo() {
    return this.getRef("http://www.w3.org/ns/prov#wasAttributedTo",true,Agent.class);
  }

  public void setWasAttributedTo(Agent val) {
    this.setRef("http://www.w3.org/ns/prov#wasAttributedTo",val,Agent.class);
  }
}
