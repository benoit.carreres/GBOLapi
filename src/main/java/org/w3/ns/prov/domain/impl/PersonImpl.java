package org.w3.ns.prov.domain.impl;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.w3.ns.prov.domain.Person;

/**
 * Code generated from http://www.w3.org/ns/prov# ontology
 */
public class PersonImpl extends AgentImpl implements Person {
  public static final String TypeIRI = "http://www.w3.org/ns/prov#Person";

  protected PersonImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Person make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Person.class);
      if(toRet == null) {
        toRet = new PersonImpl(domain,iri);;
      }
      else if(!(toRet instanceof Person)) {
        throw new RuntimeException("Instance of org.w3.ns.prov.domain.impl.PersonImpl expected");
      }
      return (Person)toRet;
    }
  }

  public void validate() {
  }
}
