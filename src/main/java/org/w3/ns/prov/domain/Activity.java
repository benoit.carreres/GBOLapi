package org.w3.ns.prov.domain;

import java.time.LocalDate;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://www.w3.org/ns/prov# ontology
 */
public interface Activity extends OWLThing {
  LocalDate getStartedAtTime();

  void setStartedAtTime(LocalDate val);

  LocalDate getEndedAtTime();

  void setEndedAtTime(LocalDate val);
}
