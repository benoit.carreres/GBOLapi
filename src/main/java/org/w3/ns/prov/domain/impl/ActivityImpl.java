package org.w3.ns.prov.domain.impl;

import java.lang.String;
import java.time.LocalDate;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.w3.ns.prov.domain.Activity;

/**
 * Code generated from http://www.w3.org/ns/prov# ontology
 */
public class ActivityImpl extends OWLThingImpl implements Activity {
  public static final String TypeIRI = "http://www.w3.org/ns/prov#Activity";

  protected ActivityImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Activity make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Activity.class);
      if(toRet == null) {
        toRet = new ActivityImpl(domain,iri);;
      }
      else if(!(toRet instanceof Activity)) {
        throw new RuntimeException("Instance of org.w3.ns.prov.domain.impl.ActivityImpl expected");
      }
      return (Activity)toRet;
    }
  }

  public void validate() {
  }

  public LocalDate getStartedAtTime() {
    return this.getDateLit("http://www.w3.org/ns/prov#startedAtTime",true);
  }

  public void setStartedAtTime(LocalDate val) {
    this.setDateLit("http://www.w3.org/ns/prov#startedAtTime",val);
  }

  public LocalDate getEndedAtTime() {
    return this.getDateLit("http://www.w3.org/ns/prov#endedAtTime",true);
  }

  public void setEndedAtTime(LocalDate val) {
    this.setDateLit("http://www.w3.org/ns/prov#endedAtTime",val);
  }
}
