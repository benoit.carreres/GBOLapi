package org.w3.ns.prov.domain.impl;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.w3.ns.prov.domain.Agent;

/**
 * Code generated from http://www.w3.org/ns/prov# ontology
 */
public class AgentImpl extends OWLThingImpl implements Agent {
  public static final String TypeIRI = "http://www.w3.org/ns/prov#Agent";

  protected AgentImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Agent make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Agent.class);
      if(toRet == null) {
        toRet = new AgentImpl(domain,iri);;
      }
      else if(!(toRet instanceof Agent)) {
        throw new RuntimeException("Instance of org.w3.ns.prov.domain.impl.AgentImpl expected");
      }
      return (Agent)toRet;
    }
  }

  public void validate() {
  }

  public Agent getActedOnBehalfOf() {
    return this.getRef("http://www.w3.org/ns/prov#actedOnBehalfOf",true,Agent.class);
  }

  public void setActedOnBehalfOf(Agent val) {
    this.setRef("http://www.w3.org/ns/prov#actedOnBehalfOf",val,Agent.class);
  }
}
