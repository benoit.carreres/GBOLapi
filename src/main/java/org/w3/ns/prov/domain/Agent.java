package org.w3.ns.prov.domain;

import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://www.w3.org/ns/prov# ontology
 */
public interface Agent extends OWLThing {
  Agent getActedOnBehalfOf();

  void setActedOnBehalfOf(Agent val);
}
