package org.w3.ns.prov.domain.impl;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.w3.ns.prov.domain.SoftwareAgent;

/**
 * Code generated from http://www.w3.org/ns/prov# ontology
 */
public class SoftwareAgentImpl extends AgentImpl implements SoftwareAgent {
  public static final String TypeIRI = "http://www.w3.org/ns/prov#SoftwareAgent";

  protected SoftwareAgentImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static SoftwareAgent make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,SoftwareAgent.class);
      if(toRet == null) {
        toRet = new SoftwareAgentImpl(domain,iri);;
      }
      else if(!(toRet instanceof SoftwareAgent)) {
        throw new RuntimeException("Instance of org.w3.ns.prov.domain.impl.SoftwareAgentImpl expected");
      }
      return (SoftwareAgent)toRet;
    }
  }

  public void validate() {
  }
}
