package org.w3.ns.prov.domain.impl;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.w3.ns.prov.domain.Organization;

/**
 * Code generated from http://www.w3.org/ns/prov# ontology
 */
public class OrganizationImpl extends AgentImpl implements Organization {
  public static final String TypeIRI = "http://www.w3.org/ns/prov#Organization";

  protected OrganizationImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Organization make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Organization.class);
      if(toRet == null) {
        toRet = new OrganizationImpl(domain,iri);;
      }
      else if(!(toRet instanceof Organization)) {
        throw new RuntimeException("Instance of org.w3.ns.prov.domain.impl.OrganizationImpl expected");
      }
      return (Organization)toRet;
    }
  }

  public void validate() {
  }
}
