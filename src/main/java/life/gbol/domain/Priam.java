package life.gbol.domain;

import java.lang.Float;
import java.lang.Integer;
import java.lang.String;
import java.util.List;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Priam extends ProteinFeatureProvenance {
  Integer getQueryTo();

  void setQueryTo(Integer val);

  Float getEvalue();

  void setEvalue(Float val);

  void remXref(XRef val);

  List<? extends XRef> getAllXref();

  void addXref(XRef val);

  String getQueryStrand();

  void setQueryStrand(String val);

  Float getProfileProportion();

  void setProfileProportion(Float val);

  Float getBitScore();

  void setBitScore(Float val);

  Integer getAlignLength();

  void setAlignLength(Integer val);

  Integer getQueryLength();

  void setQueryLength(Integer val);

  Float getPositiveHitProbability();

  void setPositiveHitProbability(Float val);

  Integer getProfileFrom();

  void setProfileFrom(Integer val);

  String getIsBestOverlap();

  void setIsBestOverlap(String val);

  String getFoundCatalyticPattern();

  void setFoundCatalyticPattern(String val);

  Integer getProfileLength();

  void setProfileLength(Integer val);

  Integer getQueryFrom();

  void setQueryFrom(Integer val);

  String getProfileId();

  void setProfileId(String val);

  Integer getProfileTo();

  void setProfileTo(Integer val);
}
