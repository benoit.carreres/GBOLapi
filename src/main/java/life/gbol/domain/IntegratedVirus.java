package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface IntegratedVirus extends GeneticElement {
  IntegratedVirusProvenance getProvenance();

  void setProvenance(IntegratedVirusProvenance val);
}
