package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface ArtificialRecognizedRegion extends RecognizedRegion {
  ArtificialRecognizedRegionProvenance getProvenance();

  void setProvenance(ArtificialRecognizedRegionProvenance val);
}
