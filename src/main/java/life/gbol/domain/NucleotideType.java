package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life# ontology
 */
public enum NucleotideType implements EnumClass {
  TypeDNA("http://gbol.life#TypeDNA",new NucleotideType[]{}),

  TypeRNA("http://gbol.life#TypeRNA",new NucleotideType[]{});

  private NucleotideType[] parents;

  private String iri;

  private NucleotideType(String iri, NucleotideType[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static NucleotideType make(String iri) {
    for(NucleotideType item : NucleotideType.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
