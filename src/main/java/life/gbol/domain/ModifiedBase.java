package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface ModifiedBase extends ChangeFeature {
  ModifiedBaseProvenance getProvenance();

  void setProvenance(ModifiedBaseProvenance val);
}
