package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface ThreePrimeUTR extends TranscriptFeature {
  ThreePrimeUTRProvenance getProvenance();

  void setProvenance(ThreePrimeUTRProvenance val);
}
