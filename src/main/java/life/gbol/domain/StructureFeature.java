package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface StructureFeature extends NonGeneRelatedElements {
  StructureFeatureProvenance getProvenance();

  void setProvenance(StructureFeatureProvenance val);
}
