package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface MobileElement extends RepeatFeature {
  MobileElementProvenance getProvenance();

  void setProvenance(MobileElementProvenance val);

  String getMobile_element_type();

  void setMobile_element_type(String val);
}
