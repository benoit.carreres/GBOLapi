package life.gbol.domain;

import java.lang.Boolean;
import java.lang.Double;
import java.lang.Integer;
import java.lang.String;
import java.time.LocalDate;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Sample extends OWLThing {
  Boolean getFocus();

  void setFocus(Boolean val);

  String getSerovar();

  void setSerovar(String val);

  String getLat_lon();

  void setLat_lon(String val);

  String getEcotype();

  void setEcotype(String val);

  void remXref(XRef val);

  List<? extends XRef> getAllXref();

  void addXref(XRef val);

  Boolean getTransgenic();

  void setTransgenic(Boolean val);

  String getClone_lib();

  void setClone_lib(String val);

  String getIsolate();

  void setIsolate(String val);

  String getCultivar();

  void setCultivar(String val);

  Boolean getGermline();

  void setGermline(Boolean val);

  LocalDate getHolddate();

  void setHolddate(LocalDate val);

  LocalDate getCollection_date();

  void setCollection_date(LocalDate val);

  String getSerotype();

  void setSerotype(String val);

  String getLab_host();

  void setLab_host(String val);

  Boolean getMacronuclear();

  void setMacronuclear(Boolean val);

  Boolean getProviral();

  void setProviral(Boolean val);

  String getVariety();

  void setVariety(String val);

  String getDev_stage();

  void setDev_stage(String val);

  String getCountry();

  void setCountry(String val);

  LocalDate getFirst_public();

  void setFirst_public(LocalDate val);

  String getTissue_lib();

  void setTissue_lib(String val);

  String getCollected_by();

  void setCollected_by(String val);

  Integer getFirst_public_release();

  void setFirst_public_release(Integer val);

  String getCell_type();

  void setCell_type(String val);

  String getPCR_primers();

  void setPCR_primers(String val);

  String getCulture_collection();

  void setCulture_collection(String val);

  LocalDate getLast_updated();

  void setLast_updated(LocalDate val);

  String getBio_material();

  void setBio_material(String val);

  Integer getLast_updated_release();

  void setLast_updated_release(Integer val);

  String getSegment();

  void setSegment(String val);

  String getIdentified_by();

  void setIdentified_by(String val);

  Boolean getRearranged();

  void setRearranged(Boolean val);

  String getPop_variant();

  void setPop_variant(String val);

  String getClone();

  void setClone(String val);

  String getSex();

  void setSex(String val);

  String getTissue_type();

  void setTissue_type(String val);

  String getType_material();

  void setType_material(String val);

  Boolean getEnvironmental_sample();

  void setEnvironmental_sample(Boolean val);

  String getHaplogroup();

  void setHaplogroup(String val);

  String getHaplotype();

  void setHaplotype(String val);

  void remKeywords(String val);

  List<? extends String> getAllKeywords();

  void addKeywords(String val);

  LocalDate getFfdate();

  void setFfdate(LocalDate val);

  Double getAltitude();

  void setAltitude(Double val);

  String getIsolation_source();

  void setIsolation_source(String val);

  String getHost();

  void setHost(String val);

  void remPublisheddataset(PublishedDataset val);

  List<? extends PublishedDataset> getAllPublisheddataset();

  void addPublisheddataset(PublishedDataset val);

  String getCell_line();

  void setCell_line(String val);

  String getMating_type();

  void setMating_type(String val);
}
