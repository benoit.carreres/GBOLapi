package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface TATASignal extends RegulationSite {
  TATASignalProvenance getProvenance();

  void setProvenance(TATASignalProvenance val);
}
