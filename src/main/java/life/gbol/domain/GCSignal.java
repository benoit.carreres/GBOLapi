package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface GCSignal extends RegulationSite {
  GCSignalProvenance getProvenance();

  void setProvenance(GCSignalProvenance val);
}
