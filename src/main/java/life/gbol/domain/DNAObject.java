package life.gbol.domain;

import java.lang.Integer;
import java.lang.String;
import org.uniprot.purl.core.domain.Taxon;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface DNAObject extends NAObject {
  Location getLocation();

  void setLocation(Location val);

  String getStrain();

  void setStrain(String val);

  Integer getSequenceversion();

  void setSequenceversion(Integer val);

  String getSub_clone();

  void setSub_clone(String val);

  String getOrganelle();

  void setOrganelle(String val);

  String getPlasmid();

  void setPlasmid(String val);

  Integer getLength();

  void setLength(Integer val);

  String getOrganism();

  void setOrganism(String val);

  String getSub_strain();

  void setSub_strain(String val);

  String getSub_species();

  void setSub_species(String val);

  String getSpecimen_voucher();

  void setSpecimen_voucher(String val);

  String getLineage();

  void setLineage(String val);

  String getChromosome();

  void setChromosome(String val);

  Taxon getTaxonomy();

  void setTaxonomy(Taxon val);
}
