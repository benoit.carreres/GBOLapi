package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Promoter extends RegulationSite {
  PromoterProvenance getProvenance();

  void setProvenance(PromoterProvenance val);
}
