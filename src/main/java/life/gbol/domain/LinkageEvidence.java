package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life# ontology
 */
public enum LinkageEvidence implements EnumClass {
  Strobe("http://gbol.life#Strobe",new LinkageEvidence[]{}),

  NA_("http://gbol.life#NA",new LinkageEvidence[]{}),

  PairedEnds("http://gbol.life#PairedEnds",new LinkageEvidence[]{}),

  AlignXGenus("http://gbol.life#AlignXGenus",new LinkageEvidence[]{}),

  Unspecified("http://gbol.life#Unspecified",new LinkageEvidence[]{}),

  Map("http://gbol.life#Map",new LinkageEvidence[]{}),

  AlignGenus("http://gbol.life#AlignGenus",new LinkageEvidence[]{}),

  AlignTranscript("http://gbol.life#AlignTranscript",new LinkageEvidence[]{}),

  PCR("http://gbol.life#PCR",new LinkageEvidence[]{}),

  CloneContig("http://gbol.life#CloneContig",new LinkageEvidence[]{}),

  WithinClone("http://gbol.life#WithinClone",new LinkageEvidence[]{});

  private LinkageEvidence[] parents;

  private String iri;

  private LinkageEvidence(String iri, LinkageEvidence[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static LinkageEvidence make(String iri) {
    for(LinkageEvidence item : LinkageEvidence.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
