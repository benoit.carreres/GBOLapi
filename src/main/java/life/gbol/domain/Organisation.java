package life.gbol.domain;

import java.lang.String;
import org.w3.ns.prov.domain.Organization;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Organisation extends Organization {
  String getLegalName();

  void setLegalName(String val);
}
