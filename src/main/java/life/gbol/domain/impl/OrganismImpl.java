package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Organism;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;

/**
 * Code generated from http://gbol.life# ontology
 */
public class OrganismImpl extends OWLThingImpl implements Organism {
  public static final String TypeIRI = "http://gbol.life#Organism";

  protected OrganismImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Organism make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Organism.class);
      if(toRet == null) {
        toRet = new OrganismImpl(domain,iri);;
      }
      else if(!(toRet instanceof Organism)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.OrganismImpl expected");
      }
      return (Organism)toRet;
    }
  }

  public void validate() {
  }
}
