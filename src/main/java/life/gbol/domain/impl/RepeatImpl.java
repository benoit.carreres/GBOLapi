package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Region;
import life.gbol.domain.Repeat;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class RepeatImpl extends TranscriptFeatureImpl implements Repeat {
  public static final String TypeIRI = "http://gbol.life#Repeat";

  protected RepeatImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Repeat make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Repeat.class);
      if(toRet == null) {
        toRet = new RepeatImpl(domain,iri);;
      }
      else if(!(toRet instanceof Repeat)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.RepeatImpl expected");
      }
      return (Repeat)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#region");
  }

  public Region getRegion() {
    return this.getRef("http://gbol.life#region",false,Region.class);
  }

  public void setRegion(Region val) {
    this.setRef("http://gbol.life#region",val,Region.class);
  }
}
