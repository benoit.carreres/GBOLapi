package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.IntronProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class IntronProvenanceImpl extends GeneticElementProvenanceImpl implements IntronProvenance {
  public static final String TypeIRI = "http://gbol.life#IntronProvenance";

  protected IntronProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static IntronProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,IntronProvenance.class);
      if(toRet == null) {
        toRet = new IntronProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof IntronProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.IntronProvenanceImpl expected");
      }
      return (IntronProvenance)toRet;
    }
  }

  public void validate() {
  }
}
