package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.TaxonomyRef;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class TaxonomyRefImpl extends XRefImpl implements TaxonomyRef {
  public static final String TypeIRI = "http://gbol.life#TaxonomyRef";

  protected TaxonomyRefImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static TaxonomyRef make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,TaxonomyRef.class);
      if(toRet == null) {
        toRet = new TaxonomyRefImpl(domain,iri);;
      }
      else if(!(toRet instanceof TaxonomyRef)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.TaxonomyRefImpl expected");
      }
      return (TaxonomyRef)toRet;
    }
  }

  public void validate() {
  }
}
