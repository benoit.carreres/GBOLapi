package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.GCSignalProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class GCSignalProvenanceImpl extends RegulationSiteProvenanceImpl implements GCSignalProvenance {
  public static final String TypeIRI = "http://gbol.life#GCSignalProvenance";

  protected GCSignalProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static GCSignalProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,GCSignalProvenance.class);
      if(toRet == null) {
        toRet = new GCSignalProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof GCSignalProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.GCSignalProvenanceImpl expected");
      }
      return (GCSignalProvenance)toRet;
    }
  }

  public void validate() {
  }
}
