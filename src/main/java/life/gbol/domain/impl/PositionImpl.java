package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Position;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;

/**
 * Code generated from http://gbol.life# ontology
 */
public class PositionImpl extends OWLThingImpl implements Position {
  public static final String TypeIRI = "http://gbol.life#Position";

  protected PositionImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Position make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Position.class);
      if(toRet == null) {
        toRet = new PositionImpl(domain,iri);;
      }
      else if(!(toRet instanceof Position)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.PositionImpl expected");
      }
      return (Position)toRet;
    }
  }

  public void validate() {
  }
}
