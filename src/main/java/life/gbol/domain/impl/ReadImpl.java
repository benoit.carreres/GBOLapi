package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Read;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ReadImpl extends UncompleteNAObjectImpl implements Read {
  public static final String TypeIRI = "http://gbol.life#Read";

  protected ReadImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Read make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Read.class);
      if(toRet == null) {
        toRet = new ReadImpl(domain,iri);;
      }
      else if(!(toRet instanceof Read)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ReadImpl expected");
      }
      return (Read)toRet;
    }
  }

  public void validate() {
  }
}
