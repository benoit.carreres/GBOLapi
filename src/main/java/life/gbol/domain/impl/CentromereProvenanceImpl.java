package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.CentromereProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class CentromereProvenanceImpl extends RepeatFeatureProvenanceImpl implements CentromereProvenance {
  public static final String TypeIRI = "http://gbol.life#CentromereProvenance";

  protected CentromereProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static CentromereProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,CentromereProvenance.class);
      if(toRet == null) {
        toRet = new CentromereProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof CentromereProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.CentromereProvenanceImpl expected");
      }
      return (CentromereProvenance)toRet;
    }
  }

  public void validate() {
  }
}
