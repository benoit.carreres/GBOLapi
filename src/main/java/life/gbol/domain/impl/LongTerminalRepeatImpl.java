package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.LongTerminalRepeat;
import life.gbol.domain.LongTerminalRepeatProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class LongTerminalRepeatImpl extends RepeatFeatureImpl implements LongTerminalRepeat {
  public static final String TypeIRI = "http://gbol.life#LongTerminalRepeat";

  protected LongTerminalRepeatImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static LongTerminalRepeat make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,LongTerminalRepeat.class);
      if(toRet == null) {
        toRet = new LongTerminalRepeatImpl(domain,iri);;
      }
      else if(!(toRet instanceof LongTerminalRepeat)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.LongTerminalRepeatImpl expected");
      }
      return (LongTerminalRepeat)toRet;
    }
  }

  public void validate() {
  }

  public LongTerminalRepeatProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,LongTerminalRepeatProvenance.class);
  }

  public void setProvenance(LongTerminalRepeatProvenance val) {
    this.setRef("http://gbol.life#provenance",val,LongTerminalRepeatProvenance.class);
  }
}
