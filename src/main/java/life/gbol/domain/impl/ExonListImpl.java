package life.gbol.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.Exon;
import life.gbol.domain.ExonList;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ExonListImpl extends OWLThingImpl implements ExonList {
  public static final String TypeIRI = "http://gbol.life#ExonList";

  protected ExonListImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static ExonList make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,ExonList.class);
      if(toRet == null) {
        toRet = new ExonListImpl(domain,iri);;
      }
      else if(!(toRet instanceof ExonList)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ExonListImpl expected");
      }
      return (ExonList)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#exon");
  }

  public Exon getExon(int index) {
    return this.getRefListAtIndex("http://gbol.life#exon",false,Exon.class,index);
  }

  public List<? extends Exon> getAllExon() {
    return this.getRefList("http://gbol.life#exon",false,Exon.class);
  }

  public void addExon(Exon val) {
    this.addRefList("http://gbol.life#exon",val);
  }

  public void setExon(Exon val, int index) {
    this.setRefList("http://gbol.life#exon",val,false,index);
  }

  public void remExon(Exon val) {
    this.remRefList("http://gbol.life#exon",val,false);
  }
}
