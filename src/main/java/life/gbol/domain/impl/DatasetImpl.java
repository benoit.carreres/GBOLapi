package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Dataset;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.w3.ns.prov.domain.impl.EntityImpl;

/**
 * Code generated from http://gbol.life# ontology
 */
public class DatasetImpl extends EntityImpl implements Dataset {
  public static final String TypeIRI = "http://gbol.life#Dataset";

  protected DatasetImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Dataset make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Dataset.class);
      if(toRet == null) {
        toRet = new DatasetImpl(domain,iri);;
      }
      else if(!(toRet instanceof Dataset)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.DatasetImpl expected");
      }
      return (Dataset)toRet;
    }
  }

  public void validate() {
  }
}
