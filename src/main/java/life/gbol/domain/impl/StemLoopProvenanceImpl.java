package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.StemLoopProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class StemLoopProvenanceImpl extends StructureFeatureProvenanceImpl implements StemLoopProvenance {
  public static final String TypeIRI = "http://gbol.life#StemLoopProvenance";

  protected StemLoopProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static StemLoopProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,StemLoopProvenance.class);
      if(toRet == null) {
        toRet = new StemLoopProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof StemLoopProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.StemLoopProvenanceImpl expected");
      }
      return (StemLoopProvenance)toRet;
    }
  }

  public void validate() {
  }
}
