package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.IntegratedVirus;
import life.gbol.domain.IntegratedVirusProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class IntegratedVirusImpl extends GeneticElementImpl implements IntegratedVirus {
  public static final String TypeIRI = "http://gbol.life#IntegratedVirus";

  protected IntegratedVirusImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static IntegratedVirus make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,IntegratedVirus.class);
      if(toRet == null) {
        toRet = new IntegratedVirusImpl(domain,iri);;
      }
      else if(!(toRet instanceof IntegratedVirus)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.IntegratedVirusImpl expected");
      }
      return (IntegratedVirus)toRet;
    }
  }

  public void validate() {
  }

  public IntegratedVirusProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,IntegratedVirusProvenance.class);
  }

  public void setProvenance(IntegratedVirusProvenance val) {
    this.setRef("http://gbol.life#provenance",val,IntegratedVirusProvenance.class);
  }
}
