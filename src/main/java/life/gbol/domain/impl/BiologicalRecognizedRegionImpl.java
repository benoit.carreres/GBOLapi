package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.BiologicalRecognizedRegion;
import life.gbol.domain.BiologicalRecognizedRegionProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class BiologicalRecognizedRegionImpl extends RecognizedRegionImpl implements BiologicalRecognizedRegion {
  public static final String TypeIRI = "http://gbol.life#BiologicalRecognizedRegion";

  protected BiologicalRecognizedRegionImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static BiologicalRecognizedRegion make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,BiologicalRecognizedRegion.class);
      if(toRet == null) {
        toRet = new BiologicalRecognizedRegionImpl(domain,iri);;
      }
      else if(!(toRet instanceof BiologicalRecognizedRegion)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.BiologicalRecognizedRegionImpl expected");
      }
      return (BiologicalRecognizedRegion)toRet;
    }
  }

  public void validate() {
  }

  public String getBound_moiety() {
    return this.getStringLit("http://gbol.life#bound_moiety",true);
  }

  public void setBound_moiety(String val) {
    this.setStringLit("http://gbol.life#bound_moiety",val);
  }

  public BiologicalRecognizedRegionProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,BiologicalRecognizedRegionProvenance.class);
  }

  public void setProvenance(BiologicalRecognizedRegionProvenance val) {
    this.setRef("http://gbol.life#provenance",val,BiologicalRecognizedRegionProvenance.class);
  }
}
