package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.TransferOriginProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class TransferOriginProvenanceImpl extends BiologicalRecognizedRegionProvenanceImpl implements TransferOriginProvenance {
  public static final String TypeIRI = "http://gbol.life#TransferOriginProvenance";

  protected TransferOriginProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static TransferOriginProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,TransferOriginProvenance.class);
      if(toRet == null) {
        toRet = new TransferOriginProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof TransferOriginProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.TransferOriginProvenanceImpl expected");
      }
      return (TransferOriginProvenance)toRet;
    }
  }

  public void validate() {
  }
}
