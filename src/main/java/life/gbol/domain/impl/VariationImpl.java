package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Variation;
import life.gbol.domain.VariationProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class VariationImpl extends ChangeFeatureImpl implements Variation {
  public static final String TypeIRI = "http://gbol.life#Variation";

  protected VariationImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Variation make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Variation.class);
      if(toRet == null) {
        toRet = new VariationImpl(domain,iri);;
      }
      else if(!(toRet instanceof Variation)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.VariationImpl expected");
      }
      return (Variation)toRet;
    }
  }

  public void validate() {
  }

  public VariationProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,VariationProvenance.class);
  }

  public void setProvenance(VariationProvenance val) {
    this.setRef("http://gbol.life#provenance",val,VariationProvenance.class);
  }
}
