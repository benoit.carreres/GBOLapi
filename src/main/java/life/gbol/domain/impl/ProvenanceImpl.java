package life.gbol.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.Provenance;
import life.gbol.domain.Publication;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.w3.ns.prov.domain.Entity;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ProvenanceImpl extends OWLThingImpl implements Provenance {
  public static final String TypeIRI = "http://gbol.life#Provenance";

  protected ProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Provenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Provenance.class);
      if(toRet == null) {
        toRet = new ProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof Provenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ProvenanceImpl expected");
      }
      return (Provenance)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#origin");
  }

  public String getNote() {
    return this.getStringLit("http://gbol.life#note",true);
  }

  public void setNote(String val) {
    this.setStringLit("http://gbol.life#note",val);
  }

  public Entity getOrigin() {
    return this.getRef("http://gbol.life#origin",false,Entity.class);
  }

  public void setOrigin(Entity val) {
    this.setRef("http://gbol.life#origin",val,Entity.class);
  }

  public void remReference(Publication val) {
    this.remRef("http://gbol.life#reference",val,true);
  }

  public List<? extends Publication> getAllReference() {
    return this.getRefSet("http://gbol.life#reference",true,Publication.class);
  }

  public void addReference(Publication val) {
    this.addRef("http://gbol.life#reference",val);
  }
}
