package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.GeneRelatedElements;
import life.gbol.domain.GeneRelatedElementsProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class GeneRelatedElementsImpl extends NAFeatureImpl implements GeneRelatedElements {
  public static final String TypeIRI = "http://gbol.life#GeneRelatedElements";

  protected GeneRelatedElementsImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static GeneRelatedElements make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,GeneRelatedElements.class);
      if(toRet == null) {
        toRet = new GeneRelatedElementsImpl(domain,iri);;
      }
      else if(!(toRet instanceof GeneRelatedElements)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.GeneRelatedElementsImpl expected");
      }
      return (GeneRelatedElements)toRet;
    }
  }

  public void validate() {
  }

  public String getTrans_splicing() {
    return this.getStringLit("http://gbol.life#trans_splicing",true);
  }

  public void setTrans_splicing(String val) {
    this.setStringLit("http://gbol.life#trans_splicing",val);
  }

  public GeneRelatedElementsProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,GeneRelatedElementsProvenance.class);
  }

  public void setProvenance(GeneRelatedElementsProvenance val) {
    this.setRef("http://gbol.life#provenance",val,GeneRelatedElementsProvenance.class);
  }

  public String getPseudogene() {
    return this.getStringLit("http://gbol.life#pseudogene",true);
  }

  public void setPseudogene(String val) {
    this.setStringLit("http://gbol.life#pseudogene",val);
  }
}
