package life.gbol.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.RepeatRegion;
import life.gbol.domain.RepeatRegionProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class RepeatRegionImpl extends RepeatFeatureImpl implements RepeatRegion {
  public static final String TypeIRI = "http://gbol.life#RepeatRegion";

  protected RepeatRegionImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static RepeatRegion make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,RepeatRegion.class);
      if(toRet == null) {
        toRet = new RepeatRegionImpl(domain,iri);;
      }
      else if(!(toRet instanceof RepeatRegion)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.RepeatRegionImpl expected");
      }
      return (RepeatRegion)toRet;
    }
  }

  public void validate() {
  }

  public RepeatRegionProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,RepeatRegionProvenance.class);
  }

  public void setProvenance(RepeatRegionProvenance val) {
    this.setRef("http://gbol.life#provenance",val,RepeatRegionProvenance.class);
  }

  public void remInference(String val) {
    this.remStringLit("http://gbol.life#inference",val,true);
  }

  public List<? extends String> getAllInference() {
    return this.getStringLitSet("http://gbol.life#inference",true);
  }

  public void addInference(String val) {
    this.addStringLit("http://gbol.life#inference",val);
  }
}
