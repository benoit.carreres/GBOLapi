package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.MiscStructure;
import life.gbol.domain.MiscStructureProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class MiscStructureImpl extends StructureFeatureImpl implements MiscStructure {
  public static final String TypeIRI = "http://gbol.life#MiscStructure";

  protected MiscStructureImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static MiscStructure make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,MiscStructure.class);
      if(toRet == null) {
        toRet = new MiscStructureImpl(domain,iri);;
      }
      else if(!(toRet instanceof MiscStructure)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.MiscStructureImpl expected");
      }
      return (MiscStructure)toRet;
    }
  }

  public void validate() {
  }

  public MiscStructureProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,MiscStructureProvenance.class);
  }

  public void setProvenance(MiscStructureProvenance val) {
    this.setRef("http://gbol.life#provenance",val,MiscStructureProvenance.class);
  }
}
