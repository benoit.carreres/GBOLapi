package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.GeneRelatedElementsProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class GeneRelatedElementsProvenanceImpl extends NAFeatureProvenanceImpl implements GeneRelatedElementsProvenance {
  public static final String TypeIRI = "http://gbol.life#GeneRelatedElementsProvenance";

  protected GeneRelatedElementsProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static GeneRelatedElementsProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,GeneRelatedElementsProvenance.class);
      if(toRet == null) {
        toRet = new GeneRelatedElementsProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof GeneRelatedElementsProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.GeneRelatedElementsProvenanceImpl expected");
      }
      return (GeneRelatedElementsProvenance)toRet;
    }
  }

  public void validate() {
  }
}
