package life.gbol.domain.impl;

import java.lang.Double;
import java.lang.String;
import life.gbol.domain.RNAmmer;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class RNAmmerImpl extends GeneProvenanceImpl implements RNAmmer {
  public static final String TypeIRI = "http://gbol.life#RNAmmer";

  protected RNAmmerImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static RNAmmer make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,RNAmmer.class);
      if(toRet == null) {
        toRet = new RNAmmerImpl(domain,iri);;
      }
      else if(!(toRet instanceof RNAmmer)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.RNAmmerImpl expected");
      }
      return (RNAmmer)toRet;
    }
  }

  public void validate() {
  }

  public Double getScore() {
    return this.getDoubleLit("http://gbol.life#score",true);
  }

  public void setScore(Double val) {
    this.setDoubleLit("http://gbol.life#score",val);
  }

  public String getAttribute() {
    return this.getStringLit("http://gbol.life#attribute",true);
  }

  public void setAttribute(String val) {
    this.setStringLit("http://gbol.life#attribute",val);
  }
}
