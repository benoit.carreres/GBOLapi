package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.SequenceConstructionItem;
import life.gbol.domain.SequenceContructionInfo;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;

/**
 * Code generated from http://gbol.life# ontology
 */
public class SequenceContructionInfoImpl extends OWLThingImpl implements SequenceContructionInfo {
  public static final String TypeIRI = "http://gbol.life#SequenceContructionInfo";

  protected SequenceContructionInfoImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static SequenceContructionInfo make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,SequenceContructionInfo.class);
      if(toRet == null) {
        toRet = new SequenceContructionInfoImpl(domain,iri);;
      }
      else if(!(toRet instanceof SequenceContructionInfo)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.SequenceContructionInfoImpl expected");
      }
      return (SequenceContructionInfo)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#sequenceItem");
  }

  public SequenceConstructionItem getSequenceItem() {
    return this.getRef("http://gbol.life#sequenceItem",false,SequenceConstructionItem.class);
  }

  public void setSequenceItem(SequenceConstructionItem val) {
    this.setRef("http://gbol.life#sequenceItem",val,SequenceConstructionItem.class);
  }
}
