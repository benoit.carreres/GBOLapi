package life.gbol.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.PublishedDataset;
import life.gbol.domain.Reference;
import life.gbol.domain.XRef;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class PublishedDatasetImpl extends DatasetImpl implements PublishedDataset {
  public static final String TypeIRI = "http://gbol.life#PublishedDataset";

  protected PublishedDatasetImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static PublishedDataset make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,PublishedDataset.class);
      if(toRet == null) {
        toRet = new PublishedDatasetImpl(domain,iri);;
      }
      else if(!(toRet instanceof PublishedDataset)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.PublishedDatasetImpl expected");
      }
      return (PublishedDataset)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#definition");
  }

  public void remKeywords(String val) {
    this.remStringLit("http://gbol.life#keywords",val,true);
  }

  public List<? extends String> getAllKeywords() {
    return this.getStringLitSet("http://gbol.life#keywords",true);
  }

  public void addKeywords(String val) {
    this.addStringLit("http://gbol.life#keywords",val);
  }

  public String getDefinition() {
    return this.getStringLit("http://gbol.life#definition",false);
  }

  public void setDefinition(String val) {
    this.setStringLit("http://gbol.life#definition",val);
  }

  public String getComment() {
    return this.getStringLit("http://www.w3.org/2000/01/rdf-schema#comment",true);
  }

  public void setComment(String val) {
    this.setStringLit("http://www.w3.org/2000/01/rdf-schema#comment",val);
  }

  public void remDblinks(XRef val) {
    this.remRef("http://gbol.life#dblinks",val,true);
  }

  public List<? extends XRef> getAllDblinks() {
    return this.getRefSet("http://gbol.life#dblinks",true,XRef.class);
  }

  public void addDblinks(XRef val) {
    this.addRef("http://gbol.life#dblinks",val);
  }

  public void remReferences(Reference val) {
    this.remRef("http://gbol.life#references",val,true);
  }

  public List<? extends Reference> getAllReferences() {
    return this.getRefSet("http://gbol.life#references",true,Reference.class);
  }

  public void addReferences(Reference val) {
    this.addRef("http://gbol.life#references",val);
  }
}
