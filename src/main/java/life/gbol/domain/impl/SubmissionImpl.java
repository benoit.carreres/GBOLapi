package life.gbol.domain.impl;

import java.lang.String;
import java.time.LocalDate;
import life.gbol.domain.Submission;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class SubmissionImpl extends PublicationImpl implements Submission {
  public static final String TypeIRI = "http://gbol.life#Submission";

  protected SubmissionImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Submission make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Submission.class);
      if(toRet == null) {
        toRet = new SubmissionImpl(domain,iri);;
      }
      else if(!(toRet instanceof Submission)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.SubmissionImpl expected");
      }
      return (Submission)toRet;
    }
  }

  public void validate() {
  }

  public LocalDate getDate() {
    return this.getDateLit("http://gbol.life#date",true);
  }

  public void setDate(LocalDate val) {
    this.setDateLit("http://gbol.life#date",val);
  }

  public String getSubmitterAddress() {
    return this.getStringLit("http://gbol.life#submitterAddress",true);
  }

  public void setSubmitterAddress(String val) {
    this.setStringLit("http://gbol.life#submitterAddress",val);
  }
}
