package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.PromoterProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class PromoterProvenanceImpl extends RegulationSiteProvenanceImpl implements PromoterProvenance {
  public static final String TypeIRI = "http://gbol.life#PromoterProvenance";

  protected PromoterProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static PromoterProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,PromoterProvenance.class);
      if(toRet == null) {
        toRet = new PromoterProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof PromoterProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.PromoterProvenanceImpl expected");
      }
      return (PromoterProvenance)toRet;
    }
  }

  public void validate() {
  }
}
