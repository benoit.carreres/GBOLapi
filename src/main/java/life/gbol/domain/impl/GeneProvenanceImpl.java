package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.GeneProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class GeneProvenanceImpl extends GeneticElementProvenanceImpl implements GeneProvenance {
  public static final String TypeIRI = "http://gbol.life#GeneProvenance";

  protected GeneProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static GeneProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,GeneProvenance.class);
      if(toRet == null) {
        toRet = new GeneProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof GeneProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.GeneProvenanceImpl expected");
      }
      return (GeneProvenance)toRet;
    }
  }

  public void validate() {
  }
}
