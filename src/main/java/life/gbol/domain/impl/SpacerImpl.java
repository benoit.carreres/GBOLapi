package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Region;
import life.gbol.domain.Spacer;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class SpacerImpl extends TranscriptFeatureImpl implements Spacer {
  public static final String TypeIRI = "http://gbol.life#Spacer";

  protected SpacerImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Spacer make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Spacer.class);
      if(toRet == null) {
        toRet = new SpacerImpl(domain,iri);;
      }
      else if(!(toRet instanceof Spacer)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.SpacerImpl expected");
      }
      return (Spacer)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#region");
  }

  public Region getRegion() {
    return this.getRef("http://gbol.life#region",false,Region.class);
  }

  public void setRegion(Region val) {
    this.setRef("http://gbol.life#region",val,Region.class);
  }
}
