package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.MiscDifferenceProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class MiscDifferenceProvenanceImpl extends ChangeFeatureProvenanceImpl implements MiscDifferenceProvenance {
  public static final String TypeIRI = "http://gbol.life#MiscDifferenceProvenance";

  protected MiscDifferenceProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static MiscDifferenceProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,MiscDifferenceProvenance.class);
      if(toRet == null) {
        toRet = new MiscDifferenceProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof MiscDifferenceProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.MiscDifferenceProvenanceImpl expected");
      }
      return (MiscDifferenceProvenance)toRet;
    }
  }

  public void validate() {
  }
}
