package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.RegulationSiteProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class RegulationSiteProvenanceImpl extends BiologicalRecognizedRegionProvenanceImpl implements RegulationSiteProvenance {
  public static final String TypeIRI = "http://gbol.life#RegulationSiteProvenance";

  protected RegulationSiteProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static RegulationSiteProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,RegulationSiteProvenance.class);
      if(toRet == null) {
        toRet = new RegulationSiteProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof RegulationSiteProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.RegulationSiteProvenanceImpl expected");
      }
      return (RegulationSiteProvenance)toRet;
    }
  }

  public void validate() {
  }
}
