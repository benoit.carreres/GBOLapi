package life.gbol.domain.impl;

import java.lang.Boolean;
import java.lang.Integer;
import java.lang.String;
import java.util.List;
import life.gbol.domain.CDS;
import life.gbol.domain.ExonList;
import life.gbol.domain.Transcript;
import life.gbol.domain.TranscriptFeature;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class TranscriptImpl extends NAObjectImpl implements Transcript {
  public static final String TypeIRI = "http://gbol.life#Transcript";

  protected TranscriptImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Transcript make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Transcript.class);
      if(toRet == null) {
        toRet = new TranscriptImpl(domain,iri);;
      }
      else if(!(toRet instanceof Transcript)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.TranscriptImpl expected");
      }
      return (Transcript)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#locus_tag");
    this.checkCardMin1("http://gbol.life#cds");
  }

  public String getOnt() {
    return this.getStringLit("http://gbol.life#ont",true);
  }

  public void setOnt(String val) {
    this.setStringLit("http://gbol.life#ont",val);
  }

  public String getGene_type() {
    return this.getStringLit("http://gbol.life#gene_type",true);
  }

  public void setGene_type(String val) {
    this.setStringLit("http://gbol.life#gene_type",val);
  }

  public String getFunction() {
    return this.getStringLit("http://gbol.life#function",true);
  }

  public void setFunction(String val) {
    this.setStringLit("http://gbol.life#function",val);
  }

  public String getTranscript_name() {
    return this.getStringLit("http://gbol.life#transcript_name",true);
  }

  public void setTranscript_name(String val) {
    this.setStringLit("http://gbol.life#transcript_name",val);
  }

  public void remFeature(TranscriptFeature val) {
    this.remRef("http://gbol.life#feature",val,true);
  }

  public List<? extends TranscriptFeature> getAllFeature() {
    return this.getRefSet("http://gbol.life#feature",true,TranscriptFeature.class);
  }

  public void addFeature(TranscriptFeature val) {
    this.addRef("http://gbol.life#feature",val);
  }

  public String getTag() {
    return this.getStringLit("http://gbol.life#tag",true);
  }

  public void setTag(String val) {
    this.setStringLit("http://gbol.life#tag",val);
  }

  public String getExon_number() {
    return this.getStringLit("http://gbol.life#exon_number",true);
  }

  public void setExon_number(String val) {
    this.setStringLit("http://gbol.life#exon_number",val);
  }

  public Integer getLevel() {
    return this.getIntegerLit("http://gbol.life#level",true);
  }

  public void setLevel(Integer val) {
    this.setIntegerLit("http://gbol.life#level",val);
  }

  public String getHavana_transcript() {
    return this.getStringLit("http://gbol.life#havana_transcript",true);
  }

  public void setHavana_transcript(String val) {
    this.setStringLit("http://gbol.life#havana_transcript",val);
  }

  public String getLocus_tag() {
    return this.getStringLit("http://gbol.life#locus_tag",false);
  }

  public void setLocus_tag(String val) {
    this.setStringLit("http://gbol.life#locus_tag",val);
  }

  public String getGene_status() {
    return this.getStringLit("http://gbol.life#gene_status",true);
  }

  public void setGene_status(String val) {
    this.setStringLit("http://gbol.life#gene_status",val);
  }

  public String getGene() {
    return this.getStringLit("http://gbol.life#gene",true);
  }

  public void setGene(String val) {
    this.setStringLit("http://gbol.life#gene",val);
  }

  public Boolean getPseudo() {
    return this.getBooleanLit("http://gbol.life#pseudo",true);
  }

  public void setPseudo(Boolean val) {
    this.setBooleanLit("http://gbol.life#pseudo",val);
  }

  public String getGene_id() {
    return this.getStringLit("http://gbol.life#gene_id",true);
  }

  public void setGene_id(String val) {
    this.setStringLit("http://gbol.life#gene_id",val);
  }

  public String getGene_name() {
    return this.getStringLit("http://gbol.life#gene_name",true);
  }

  public void setGene_name(String val) {
    this.setStringLit("http://gbol.life#gene_name",val);
  }

  public String getOld_locus_tag() {
    return this.getStringLit("http://gbol.life#old_locus_tag",true);
  }

  public void setOld_locus_tag(String val) {
    this.setStringLit("http://gbol.life#old_locus_tag",val);
  }

  public String getGene_synonym() {
    return this.getStringLit("http://gbol.life#gene_synonym",true);
  }

  public void setGene_synonym(String val) {
    this.setStringLit("http://gbol.life#gene_synonym",val);
  }

  public String getHavana_gene() {
    return this.getStringLit("http://gbol.life#havana_gene",true);
  }

  public void setHavana_gene(String val) {
    this.setStringLit("http://gbol.life#havana_gene",val);
  }

  public String getTranscript_type() {
    return this.getStringLit("http://gbol.life#transcript_type",true);
  }

  public void setTranscript_type(String val) {
    this.setStringLit("http://gbol.life#transcript_type",val);
  }

  public String getTranscript_id() {
    return this.getStringLit("http://gbol.life#transcript_id",true);
  }

  public void setTranscript_id(String val) {
    this.setStringLit("http://gbol.life#transcript_id",val);
  }

  public String getTranscript_status() {
    return this.getStringLit("http://gbol.life#transcript_status",true);
  }

  public void setTranscript_status(String val) {
    this.setStringLit("http://gbol.life#transcript_status",val);
  }

  public String getExon_id() {
    return this.getStringLit("http://gbol.life#exon_id",true);
  }

  public void setExon_id(String val) {
    this.setStringLit("http://gbol.life#exon_id",val);
  }

  public ExonList getExonList() {
    return this.getRef("http://gbol.life#exonList",true,ExonList.class);
  }

  public void setExonList(ExonList val) {
    this.setRef("http://gbol.life#exonList",val,ExonList.class);
  }

  public CDS getCds() {
    return this.getRef("http://gbol.life#cds",false,CDS.class);
  }

  public void setCds(CDS val) {
    this.setRef("http://gbol.life#cds",val,CDS.class);
  }
}
