package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Intron;
import life.gbol.domain.IntronProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class IntronImpl extends GeneticElementImpl implements Intron {
  public static final String TypeIRI = "http://gbol.life#Intron";

  protected IntronImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Intron make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Intron.class);
      if(toRet == null) {
        toRet = new IntronImpl(domain,iri);;
      }
      else if(!(toRet instanceof Intron)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.IntronImpl expected");
      }
      return (Intron)toRet;
    }
  }

  public void validate() {
  }

  public IntronProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,IntronProvenance.class);
  }

  public void setProvenance(IntronProvenance val) {
    this.setRef("http://gbol.life#provenance",val,IntronProvenance.class);
  }
}
