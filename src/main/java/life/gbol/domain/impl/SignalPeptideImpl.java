package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.SignalPeptide;
import life.gbol.domain.SignalPeptideProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class SignalPeptideImpl extends ProteinFeatureImpl implements SignalPeptide {
  public static final String TypeIRI = "http://gbol.life#SignalPeptide";

  protected SignalPeptideImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static SignalPeptide make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,SignalPeptide.class);
      if(toRet == null) {
        toRet = new SignalPeptideImpl(domain,iri);;
      }
      else if(!(toRet instanceof SignalPeptide)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.SignalPeptideImpl expected");
      }
      return (SignalPeptide)toRet;
    }
  }

  public void validate() {
  }

  public SignalPeptideProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,SignalPeptideProvenance.class);
  }

  public void setProvenance(SignalPeptideProvenance val) {
    this.setRef("http://gbol.life#provenance",val,SignalPeptideProvenance.class);
  }
}
