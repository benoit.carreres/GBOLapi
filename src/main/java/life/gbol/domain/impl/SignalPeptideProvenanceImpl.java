package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.SignalPeptideProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class SignalPeptideProvenanceImpl extends ProteinFeatureProvenanceImpl implements SignalPeptideProvenance {
  public static final String TypeIRI = "http://gbol.life#SignalPeptideProvenance";

  protected SignalPeptideProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static SignalPeptideProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,SignalPeptideProvenance.class);
      if(toRet == null) {
        toRet = new SignalPeptideProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof SignalPeptideProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.SignalPeptideProvenanceImpl expected");
      }
      return (SignalPeptideProvenance)toRet;
    }
  }

  public void validate() {
  }
}
