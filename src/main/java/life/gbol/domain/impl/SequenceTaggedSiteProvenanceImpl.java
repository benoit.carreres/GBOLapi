package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.SequenceTaggedSiteProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class SequenceTaggedSiteProvenanceImpl extends ArtificialRecognizedRegionProvenanceImpl implements SequenceTaggedSiteProvenance {
  public static final String TypeIRI = "http://gbol.life#SequenceTaggedSiteProvenance";

  protected SequenceTaggedSiteProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static SequenceTaggedSiteProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,SequenceTaggedSiteProvenance.class);
      if(toRet == null) {
        toRet = new SequenceTaggedSiteProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof SequenceTaggedSiteProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.SequenceTaggedSiteProvenanceImpl expected");
      }
      return (SequenceTaggedSiteProvenance)toRet;
    }
  }

  public void validate() {
  }
}
