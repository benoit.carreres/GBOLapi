package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.PolyASiteProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class PolyASiteProvenanceImpl extends TranscriptFeatureProvenanceImpl implements PolyASiteProvenance {
  public static final String TypeIRI = "http://gbol.life#PolyASiteProvenance";

  protected PolyASiteProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static PolyASiteProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,PolyASiteProvenance.class);
      if(toRet == null) {
        toRet = new PolyASiteProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof PolyASiteProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.PolyASiteProvenanceImpl expected");
      }
      return (PolyASiteProvenance)toRet;
    }
  }

  public void validate() {
  }
}
