package life.gbol.domain.impl;

import java.lang.Integer;
import java.lang.String;
import java.util.List;
import life.gbol.domain.Feature;
import life.gbol.domain.FeatureProvenance;
import life.gbol.domain.Location;
import life.gbol.domain.Note;
import life.gbol.domain.XRef;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;

/**
 * Code generated from http://gbol.life# ontology
 */
public class FeatureImpl extends OWLThingImpl implements Feature {
  public static final String TypeIRI = "http://gbol.life#Feature";

  protected FeatureImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Feature make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Feature.class);
      if(toRet == null) {
        toRet = new FeatureImpl(domain,iri);;
      }
      else if(!(toRet instanceof Feature)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.FeatureImpl expected");
      }
      return (Feature)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#location");
  }

  public void remNote(Note val) {
    this.remRef("http://gbol.life#note",val,true);
  }

  public List<? extends Note> getAllNote() {
    return this.getRefSet("http://gbol.life#note",true,Note.class);
  }

  public void addNote(Note val) {
    this.addRef("http://gbol.life#note",val);
  }

  public String getFunction() {
    return this.getStringLit("http://gbol.life#function",true);
  }

  public void setFunction(String val) {
    this.setStringLit("http://gbol.life#function",val);
  }

  public void remXref(XRef val) {
    this.remRef("http://gbol.life#xref",val,true);
  }

  public List<? extends XRef> getAllXref() {
    return this.getRefSet("http://gbol.life#xref",true,XRef.class);
  }

  public void addXref(XRef val) {
    this.addRef("http://gbol.life#xref",val);
  }

  public Location getLocation() {
    return this.getRef("http://gbol.life#location",false,Location.class);
  }

  public void setLocation(Location val) {
    this.setRef("http://gbol.life#location",val,Location.class);
  }

  public String getMap() {
    return this.getStringLit("http://gbol.life#map",true);
  }

  public void setMap(String val) {
    this.setStringLit("http://gbol.life#map",val);
  }

  public Integer getNumber() {
    return this.getIntegerLit("http://gbol.life#number",true);
  }

  public void setNumber(Integer val) {
    this.setIntegerLit("http://gbol.life#number",val);
  }

  public String getExperiment() {
    return this.getStringLit("http://gbol.life#experiment",true);
  }

  public void setExperiment(String val) {
    this.setStringLit("http://gbol.life#experiment",val);
  }

  public String getStandard_name() {
    return this.getStringLit("http://gbol.life#standard_name",true);
  }

  public void setStandard_name(String val) {
    this.setStringLit("http://gbol.life#standard_name",val);
  }

  public String getAllele() {
    return this.getStringLit("http://gbol.life#allele",true);
  }

  public void setAllele(String val) {
    this.setStringLit("http://gbol.life#allele",val);
  }

  public Integer getCitation() {
    return this.getIntegerLit("http://gbol.life#citation",true);
  }

  public void setCitation(Integer val) {
    this.setIntegerLit("http://gbol.life#citation",val);
  }

  public FeatureProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,FeatureProvenance.class);
  }

  public void setProvenance(FeatureProvenance val) {
    this.setRef("http://gbol.life#provenance",val,FeatureProvenance.class);
  }

  public String getOperon() {
    return this.getStringLit("http://gbol.life#operon",true);
  }

  public void setOperon(String val) {
    this.setStringLit("http://gbol.life#operon",val);
  }

  public String getInference() {
    return this.getStringLit("http://gbol.life#inference",true);
  }

  public void setInference(String val) {
    this.setStringLit("http://gbol.life#inference",val);
  }
}
