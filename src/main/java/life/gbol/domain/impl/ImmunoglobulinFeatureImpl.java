package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.ImmunoglobulinFeature;
import life.gbol.domain.ImmunoglobulinFeatureProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ImmunoglobulinFeatureImpl extends ProteinFeatureImpl implements ImmunoglobulinFeature {
  public static final String TypeIRI = "http://gbol.life#ImmunoglobulinFeature";

  protected ImmunoglobulinFeatureImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static ImmunoglobulinFeature make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,ImmunoglobulinFeature.class);
      if(toRet == null) {
        toRet = new ImmunoglobulinFeatureImpl(domain,iri);;
      }
      else if(!(toRet instanceof ImmunoglobulinFeature)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ImmunoglobulinFeatureImpl expected");
      }
      return (ImmunoglobulinFeature)toRet;
    }
  }

  public void validate() {
  }

  public ImmunoglobulinFeatureProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,ImmunoglobulinFeatureProvenance.class);
  }

  public void setProvenance(ImmunoglobulinFeatureProvenance val) {
    this.setRef("http://gbol.life#provenance",val,ImmunoglobulinFeatureProvenance.class);
  }
}
