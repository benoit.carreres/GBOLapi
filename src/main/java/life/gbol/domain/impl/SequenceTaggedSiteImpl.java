package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.SequenceTaggedSite;
import life.gbol.domain.SequenceTaggedSiteProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class SequenceTaggedSiteImpl extends ArtificialRecognizedRegionImpl implements SequenceTaggedSite {
  public static final String TypeIRI = "http://gbol.life#SequenceTaggedSite";

  protected SequenceTaggedSiteImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static SequenceTaggedSite make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,SequenceTaggedSite.class);
      if(toRet == null) {
        toRet = new SequenceTaggedSiteImpl(domain,iri);;
      }
      else if(!(toRet instanceof SequenceTaggedSite)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.SequenceTaggedSiteImpl expected");
      }
      return (SequenceTaggedSite)toRet;
    }
  }

  public void validate() {
  }

  public SequenceTaggedSiteProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,SequenceTaggedSiteProvenance.class);
  }

  public void setProvenance(SequenceTaggedSiteProvenance val) {
    this.setRef("http://gbol.life#provenance",val,SequenceTaggedSiteProvenance.class);
  }
}
