package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.IntegratedVirusProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class IntegratedVirusProvenanceImpl extends GeneticElementProvenanceImpl implements IntegratedVirusProvenance {
  public static final String TypeIRI = "http://gbol.life#IntegratedVirusProvenance";

  protected IntegratedVirusProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static IntegratedVirusProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,IntegratedVirusProvenance.class);
      if(toRet == null) {
        toRet = new IntegratedVirusProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof IntegratedVirusProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.IntegratedVirusProvenanceImpl expected");
      }
      return (IntegratedVirusProvenance)toRet;
    }
  }

  public void validate() {
  }
}
