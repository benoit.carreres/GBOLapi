package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.ModifiedBase;
import life.gbol.domain.ModifiedBaseProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ModifiedBaseImpl extends ChangeFeatureImpl implements ModifiedBase {
  public static final String TypeIRI = "http://gbol.life#ModifiedBase";

  protected ModifiedBaseImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static ModifiedBase make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,ModifiedBase.class);
      if(toRet == null) {
        toRet = new ModifiedBaseImpl(domain,iri);;
      }
      else if(!(toRet instanceof ModifiedBase)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ModifiedBaseImpl expected");
      }
      return (ModifiedBase)toRet;
    }
  }

  public void validate() {
  }

  public ModifiedBaseProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,ModifiedBaseProvenance.class);
  }

  public void setProvenance(ModifiedBaseProvenance val) {
    this.setRef("http://gbol.life#provenance",val,ModifiedBaseProvenance.class);
  }
}
