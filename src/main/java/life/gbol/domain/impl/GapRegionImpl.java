package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.GapRegion;
import life.gbol.domain.GapRegionProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class GapRegionImpl extends ImmunoglobulinFeatureImpl implements GapRegion {
  public static final String TypeIRI = "http://gbol.life#GapRegion";

  protected GapRegionImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static GapRegion make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,GapRegion.class);
      if(toRet == null) {
        toRet = new GapRegionImpl(domain,iri);;
      }
      else if(!(toRet instanceof GapRegion)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.GapRegionImpl expected");
      }
      return (GapRegion)toRet;
    }
  }

  public void validate() {
  }

  public GapRegionProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,GapRegionProvenance.class);
  }

  public void setProvenance(GapRegionProvenance val) {
    this.setRef("http://gbol.life#provenance",val,GapRegionProvenance.class);
  }
}
