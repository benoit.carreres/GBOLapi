package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.RibosomalBindingSite;
import life.gbol.domain.RibosomalBindingSiteProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class RibosomalBindingSiteImpl extends RegulationSiteImpl implements RibosomalBindingSite {
  public static final String TypeIRI = "http://gbol.life#RibosomalBindingSite";

  protected RibosomalBindingSiteImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static RibosomalBindingSite make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,RibosomalBindingSite.class);
      if(toRet == null) {
        toRet = new RibosomalBindingSiteImpl(domain,iri);;
      }
      else if(!(toRet instanceof RibosomalBindingSite)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.RibosomalBindingSiteImpl expected");
      }
      return (RibosomalBindingSite)toRet;
    }
  }

  public void validate() {
  }

  public RibosomalBindingSiteProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,RibosomalBindingSiteProvenance.class);
  }

  public void setProvenance(RibosomalBindingSiteProvenance val) {
    this.setRef("http://gbol.life#provenance",val,RibosomalBindingSiteProvenance.class);
  }
}
