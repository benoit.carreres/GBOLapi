package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.TerminatorProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class TerminatorProvenanceImpl extends RegulationSiteProvenanceImpl implements TerminatorProvenance {
  public static final String TypeIRI = "http://gbol.life#TerminatorProvenance";

  protected TerminatorProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static TerminatorProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,TerminatorProvenance.class);
      if(toRet == null) {
        toRet = new TerminatorProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof TerminatorProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.TerminatorProvenanceImpl expected");
      }
      return (TerminatorProvenance)toRet;
    }
  }

  public void validate() {
  }
}
