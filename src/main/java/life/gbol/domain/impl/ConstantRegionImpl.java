package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.ConstantRegion;
import life.gbol.domain.ConstantRegionProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ConstantRegionImpl extends ImmunoglobulinFeatureImpl implements ConstantRegion {
  public static final String TypeIRI = "http://gbol.life#ConstantRegion";

  protected ConstantRegionImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static ConstantRegion make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,ConstantRegion.class);
      if(toRet == null) {
        toRet = new ConstantRegionImpl(domain,iri);;
      }
      else if(!(toRet instanceof ConstantRegion)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ConstantRegionImpl expected");
      }
      return (ConstantRegion)toRet;
    }
  }

  public void validate() {
  }

  public ConstantRegionProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,ConstantRegionProvenance.class);
  }

  public void setProvenance(ConstantRegionProvenance val) {
    this.setRef("http://gbol.life#provenance",val,ConstantRegionProvenance.class);
  }
}
