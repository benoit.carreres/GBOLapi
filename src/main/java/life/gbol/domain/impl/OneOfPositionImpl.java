package life.gbol.domain.impl;

import java.lang.Integer;
import java.lang.String;
import java.util.List;
import life.gbol.domain.OneOfPosition;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class OneOfPositionImpl extends FuzzyPositionImpl implements OneOfPosition {
  public static final String TypeIRI = "http://gbol.life#OneOfPosition";

  protected OneOfPositionImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static OneOfPosition make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,OneOfPosition.class);
      if(toRet == null) {
        toRet = new OneOfPositionImpl(domain,iri);;
      }
      else if(!(toRet instanceof OneOfPosition)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.OneOfPositionImpl expected");
      }
      return (OneOfPosition)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#position");
  }

  public void remPosition(Integer val) {
    this.remIntegerLit("http://gbol.life#position",val,false);
  }

  public List<? extends Integer> getAllPosition() {
    return this.getIntegerLitSet("http://gbol.life#position",false);
  }

  public void addPosition(Integer val) {
    this.addIntegerLit("http://gbol.life#position",val);
  }
}
