package life.gbol.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.BagOfRegions;
import life.gbol.domain.Region;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class BagOfRegionsImpl extends CollectionOfRegionsImpl implements BagOfRegions {
  public static final String TypeIRI = "http://gbol.life#BagOfRegions";

  protected BagOfRegionsImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static BagOfRegions make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,BagOfRegions.class);
      if(toRet == null) {
        toRet = new BagOfRegionsImpl(domain,iri);;
      }
      else if(!(toRet instanceof BagOfRegions)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.BagOfRegionsImpl expected");
      }
      return (BagOfRegions)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#members");
  }

  public void remMembers(Region val) {
    this.remRef("http://gbol.life#members",val,false);
  }

  public List<? extends Region> getAllMembers() {
    return this.getRefSet("http://gbol.life#members",false,Region.class);
  }

  public void addMembers(Region val) {
    this.addRef("http://gbol.life#members",val);
  }
}
