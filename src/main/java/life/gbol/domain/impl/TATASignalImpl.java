package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.TATASignal;
import life.gbol.domain.TATASignalProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class TATASignalImpl extends RegulationSiteImpl implements TATASignal {
  public static final String TypeIRI = "http://gbol.life#TATASignal";

  protected TATASignalImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static TATASignal make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,TATASignal.class);
      if(toRet == null) {
        toRet = new TATASignalImpl(domain,iri);;
      }
      else if(!(toRet instanceof TATASignal)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.TATASignalImpl expected");
      }
      return (TATASignal)toRet;
    }
  }

  public void validate() {
  }

  public TATASignalProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,TATASignalProvenance.class);
  }

  public void setProvenance(TATASignalProvenance val) {
    this.setRef("http://gbol.life#provenance",val,TATASignalProvenance.class);
  }
}
