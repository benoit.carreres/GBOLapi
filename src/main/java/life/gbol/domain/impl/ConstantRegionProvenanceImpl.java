package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.ConstantRegionProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ConstantRegionProvenanceImpl extends ImmunoglobulinFeatureProvenanceImpl implements ConstantRegionProvenance {
  public static final String TypeIRI = "http://gbol.life#ConstantRegionProvenance";

  protected ConstantRegionProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static ConstantRegionProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,ConstantRegionProvenance.class);
      if(toRet == null) {
        toRet = new ConstantRegionProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof ConstantRegionProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ConstantRegionProvenanceImpl expected");
      }
      return (ConstantRegionProvenance)toRet;
    }
  }

  public void validate() {
  }
}
