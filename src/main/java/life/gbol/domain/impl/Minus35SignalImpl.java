package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Minus35Signal;
import life.gbol.domain.Minus35SignalProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class Minus35SignalImpl extends RegulationSiteImpl implements Minus35Signal {
  public static final String TypeIRI = "http://gbol.life#Minus35Signal";

  protected Minus35SignalImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Minus35Signal make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Minus35Signal.class);
      if(toRet == null) {
        toRet = new Minus35SignalImpl(domain,iri);;
      }
      else if(!(toRet instanceof Minus35Signal)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.Minus35SignalImpl expected");
      }
      return (Minus35Signal)toRet;
    }
  }

  public void validate() {
  }

  public Minus35SignalProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,Minus35SignalProvenance.class);
  }

  public void setProvenance(Minus35SignalProvenance val) {
    this.setRef("http://gbol.life#provenance",val,Minus35SignalProvenance.class);
  }
}
