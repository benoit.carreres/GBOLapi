package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Minus10Signal;
import life.gbol.domain.Minus10SignalProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class Minus10SignalImpl extends RegulationSiteImpl implements Minus10Signal {
  public static final String TypeIRI = "http://gbol.life#Minus10Signal";

  protected Minus10SignalImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Minus10Signal make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Minus10Signal.class);
      if(toRet == null) {
        toRet = new Minus10SignalImpl(domain,iri);;
      }
      else if(!(toRet instanceof Minus10Signal)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.Minus10SignalImpl expected");
      }
      return (Minus10Signal)toRet;
    }
  }

  public void validate() {
  }

  public Minus10SignalProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,Minus10SignalProvenance.class);
  }

  public void setProvenance(Minus10SignalProvenance val) {
    this.setRef("http://gbol.life#provenance",val,Minus10SignalProvenance.class);
  }
}
