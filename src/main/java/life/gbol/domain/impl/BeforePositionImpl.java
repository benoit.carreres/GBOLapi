package life.gbol.domain.impl;

import java.lang.Integer;
import java.lang.String;
import life.gbol.domain.BeforePosition;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class BeforePositionImpl extends FuzzyPositionImpl implements BeforePosition {
  public static final String TypeIRI = "http://gbol.life#BeforePosition";

  protected BeforePositionImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static BeforePosition make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,BeforePosition.class);
      if(toRet == null) {
        toRet = new BeforePositionImpl(domain,iri);;
      }
      else if(!(toRet instanceof BeforePosition)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.BeforePositionImpl expected");
      }
      return (BeforePosition)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#position");
  }

  public Integer getPosition() {
    return this.getIntegerLit("http://gbol.life#position",false);
  }

  public void setPosition(Integer val) {
    this.setIntegerLit("http://gbol.life#position",val);
  }
}
