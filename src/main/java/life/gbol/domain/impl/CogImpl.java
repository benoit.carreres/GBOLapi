package life.gbol.domain.impl;

import java.lang.Integer;
import java.lang.String;
import life.gbol.domain.Cog;
import life.gbol.domain.CogInfo;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class CogImpl extends BlastImpl implements Cog {
  public static final String TypeIRI = "http://gbol.life#Cog";

  protected CogImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Cog make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Cog.class);
      if(toRet == null) {
        toRet = new CogImpl(domain,iri);;
      }
      else if(!(toRet instanceof Cog)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.CogImpl expected");
      }
      return (Cog)toRet;
    }
  }

  public void validate() {
  }

  public String getLetter() {
    return this.getStringLit("http://gbol.life#letter",true);
  }

  public void setLetter(String val) {
    this.setStringLit("http://gbol.life#letter",val);
  }

  public String getLabel() {
    return this.getStringLit("http://gbol.life#label",true);
  }

  public void setLabel(String val) {
    this.setStringLit("http://gbol.life#label",val);
  }

  public String getOrganism() {
    return this.getStringLit("http://gbol.life#organism",true);
  }

  public void setOrganism(String val) {
    this.setStringLit("http://gbol.life#organism",val);
  }

  public String getProteinid() {
    return this.getStringLit("http://gbol.life#proteinid",true);
  }

  public void setProteinid(String val) {
    this.setStringLit("http://gbol.life#proteinid",val);
  }

  public String getMembershipclass() {
    return this.getStringLit("http://gbol.life#membershipclass",true);
  }

  public void setMembershipclass(String val) {
    this.setStringLit("http://gbol.life#membershipclass",val);
  }

  public String getDescription() {
    return this.getStringLit("http://gbol.life#description",true);
  }

  public void setDescription(String val) {
    this.setStringLit("http://gbol.life#description",val);
  }

  public CogInfo getCoginfo() {
    return this.getRef("http://gbol.life#coginfo",true,CogInfo.class);
  }

  public void setCoginfo(CogInfo val) {
    this.setRef("http://gbol.life#coginfo",val,CogInfo.class);
  }

  public String getCogid() {
    return this.getStringLit("http://gbol.life#cogid",true);
  }

  public void setCogid(String val) {
    this.setStringLit("http://gbol.life#cogid",val);
  }

  public Integer getEnd() {
    return this.getIntegerLit("http://gbol.life#end",true);
  }

  public void setEnd(Integer val) {
    this.setIntegerLit("http://gbol.life#end",val);
  }

  public Integer getProteinlength() {
    return this.getIntegerLit("http://gbol.life#proteinlength",true);
  }

  public void setProteinlength(Integer val) {
    this.setIntegerLit("http://gbol.life#proteinlength",val);
  }

  public Integer getBegin() {
    return this.getIntegerLit("http://gbol.life#begin",true);
  }

  public void setBegin(Integer val) {
    this.setIntegerLit("http://gbol.life#begin",val);
  }
}
