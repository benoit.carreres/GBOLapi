package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.PolyASite;
import life.gbol.domain.PolyASiteProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class PolyASiteImpl extends TranscriptFeatureImpl implements PolyASite {
  public static final String TypeIRI = "http://gbol.life#PolyASite";

  protected PolyASiteImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static PolyASite make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,PolyASite.class);
      if(toRet == null) {
        toRet = new PolyASiteImpl(domain,iri);;
      }
      else if(!(toRet instanceof PolyASite)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.PolyASiteImpl expected");
      }
      return (PolyASite)toRet;
    }
  }

  public void validate() {
  }

  public PolyASiteProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,PolyASiteProvenance.class);
  }

  public void setProvenance(PolyASiteProvenance val) {
    this.setRef("http://gbol.life#provenance",val,PolyASiteProvenance.class);
  }
}
