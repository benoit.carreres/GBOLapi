package life.gbol.domain.impl;

import java.lang.Integer;
import java.lang.String;
import life.gbol.domain.DNAObject;
import life.gbol.domain.Location;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.uniprot.purl.core.domain.Taxon;

/**
 * Code generated from http://gbol.life# ontology
 */
public class DNAObjectImpl extends NAObjectImpl implements DNAObject {
  public static final String TypeIRI = "http://gbol.life#DNAObject";

  protected DNAObjectImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static DNAObject make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,DNAObject.class);
      if(toRet == null) {
        toRet = new DNAObjectImpl(domain,iri);;
      }
      else if(!(toRet instanceof DNAObject)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.DNAObjectImpl expected");
      }
      return (DNAObject)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#location");
  }

  public Location getLocation() {
    return this.getRef("http://gbol.life#location",false,Location.class);
  }

  public void setLocation(Location val) {
    this.setRef("http://gbol.life#location",val,Location.class);
  }

  public String getStrain() {
    return this.getStringLit("http://gbol.life#strain",true);
  }

  public void setStrain(String val) {
    this.setStringLit("http://gbol.life#strain",val);
  }

  public Integer getSequenceversion() {
    return this.getIntegerLit("http://gbol.life#sequenceversion",true);
  }

  public void setSequenceversion(Integer val) {
    this.setIntegerLit("http://gbol.life#sequenceversion",val);
  }

  public String getSub_clone() {
    return this.getStringLit("http://gbol.life#sub_clone",true);
  }

  public void setSub_clone(String val) {
    this.setStringLit("http://gbol.life#sub_clone",val);
  }

  public String getOrganelle() {
    return this.getStringLit("http://gbol.life#organelle",true);
  }

  public void setOrganelle(String val) {
    this.setStringLit("http://gbol.life#organelle",val);
  }

  public String getPlasmid() {
    return this.getStringLit("http://gbol.life#plasmid",true);
  }

  public void setPlasmid(String val) {
    this.setStringLit("http://gbol.life#plasmid",val);
  }

  public Integer getLength() {
    return this.getIntegerLit("http://gbol.life#length",true);
  }

  public void setLength(Integer val) {
    this.setIntegerLit("http://gbol.life#length",val);
  }

  public String getOrganism() {
    return this.getStringLit("http://gbol.life#organism",true);
  }

  public void setOrganism(String val) {
    this.setStringLit("http://gbol.life#organism",val);
  }

  public String getSub_strain() {
    return this.getStringLit("http://gbol.life#sub_strain",true);
  }

  public void setSub_strain(String val) {
    this.setStringLit("http://gbol.life#sub_strain",val);
  }

  public String getSub_species() {
    return this.getStringLit("http://gbol.life#sub_species",true);
  }

  public void setSub_species(String val) {
    this.setStringLit("http://gbol.life#sub_species",val);
  }

  public String getSpecimen_voucher() {
    return this.getStringLit("http://gbol.life#specimen_voucher",true);
  }

  public void setSpecimen_voucher(String val) {
    this.setStringLit("http://gbol.life#specimen_voucher",val);
  }

  public String getLineage() {
    return this.getStringLit("http://gbol.life#lineage",true);
  }

  public void setLineage(String val) {
    this.setStringLit("http://gbol.life#lineage",val);
  }

  public String getChromosome() {
    return this.getStringLit("http://gbol.life#chromosome",true);
  }

  public void setChromosome(String val) {
    this.setStringLit("http://gbol.life#chromosome",val);
  }

  public Taxon getTaxonomy() {
    return this.getRef("http://gbol.life#taxonomy",true,Taxon.class);
  }

  public void setTaxonomy(Taxon val) {
    this.setRef("http://gbol.life#taxonomy",val,Taxon.class);
  }
}
