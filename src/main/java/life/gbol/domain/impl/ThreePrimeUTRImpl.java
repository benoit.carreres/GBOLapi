package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.ThreePrimeUTR;
import life.gbol.domain.ThreePrimeUTRProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ThreePrimeUTRImpl extends TranscriptFeatureImpl implements ThreePrimeUTR {
  public static final String TypeIRI = "http://gbol.life#ThreePrimeUTR";

  protected ThreePrimeUTRImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static ThreePrimeUTR make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,ThreePrimeUTR.class);
      if(toRet == null) {
        toRet = new ThreePrimeUTRImpl(domain,iri);;
      }
      else if(!(toRet instanceof ThreePrimeUTR)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ThreePrimeUTRImpl expected");
      }
      return (ThreePrimeUTR)toRet;
    }
  }

  public void validate() {
  }

  public ThreePrimeUTRProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,ThreePrimeUTRProvenance.class);
  }

  public void setProvenance(ThreePrimeUTRProvenance val) {
    this.setRef("http://gbol.life#provenance",val,ThreePrimeUTRProvenance.class);
  }
}
