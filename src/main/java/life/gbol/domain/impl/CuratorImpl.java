package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Curator;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.w3.ns.prov.domain.impl.PersonImpl;

/**
 * Code generated from http://gbol.life# ontology
 */
public class CuratorImpl extends PersonImpl implements Curator {
  public static final String TypeIRI = "http://gbol.life#Curator";

  protected CuratorImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Curator make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Curator.class);
      if(toRet == null) {
        toRet = new CuratorImpl(domain,iri);;
      }
      else if(!(toRet instanceof Curator)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.CuratorImpl expected");
      }
      return (Curator)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#orcid");
  }

  public String getName() {
    return this.getStringLit("http://gbol.life#name",true);
  }

  public void setName(String val) {
    this.setStringLit("http://gbol.life#name",val);
  }

  public String getOrcid() {
    return this.getExternalRef("http://gbol.life#orcid",false);
  }

  public void setOrcid(String val) {
    this.setExternalRef("http://gbol.life#orcid",val);
  }
}
