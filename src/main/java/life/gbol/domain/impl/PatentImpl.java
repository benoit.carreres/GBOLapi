package life.gbol.domain.impl;

import java.lang.Integer;
import java.lang.String;
import java.time.LocalDate;
import java.util.List;
import life.gbol.domain.Patent;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class PatentImpl extends PublicationImpl implements Patent {
  public static final String TypeIRI = "http://gbol.life#Patent";

  protected PatentImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Patent make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Patent.class);
      if(toRet == null) {
        toRet = new PatentImpl(domain,iri);;
      }
      else if(!(toRet instanceof Patent)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.PatentImpl expected");
      }
      return (Patent)toRet;
    }
  }

  public void validate() {
  }

  public LocalDate getDate() {
    return this.getDateLit("http://gbol.life#date",true);
  }

  public void setDate(LocalDate val) {
    this.setDateLit("http://gbol.life#date",val);
  }

  public String getPatentType() {
    return this.getStringLit("http://gbol.life#patentType",true);
  }

  public void setPatentType(String val) {
    this.setStringLit("http://gbol.life#patentType",val);
  }

  public Integer getSequenceNumber() {
    return this.getIntegerLit("http://gbol.life#sequenceNumber",true);
  }

  public void setSequenceNumber(Integer val) {
    this.setIntegerLit("http://gbol.life#sequenceNumber",val);
  }

  public String getPatentOffice() {
    return this.getStringLit("http://gbol.life#patentOffice",true);
  }

  public void setPatentOffice(String val) {
    this.setStringLit("http://gbol.life#patentOffice",val);
  }

  public void remApplicants(String val) {
    this.remStringLit("http://gbol.life#applicants",val,true);
  }

  public List<? extends String> getAllApplicants() {
    return this.getStringLitSet("http://gbol.life#applicants",true);
  }

  public void addApplicants(String val) {
    this.addStringLit("http://gbol.life#applicants",val);
  }

  public String getPatenNumber() {
    return this.getStringLit("http://gbol.life#patenNumber",true);
  }

  public void setPatenNumber(String val) {
    this.setStringLit("http://gbol.life#patenNumber",val);
  }
}
