package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.RepeatFeatureProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class RepeatFeatureProvenanceImpl extends NonGeneRelatedElementsProvenanceImpl implements RepeatFeatureProvenance {
  public static final String TypeIRI = "http://gbol.life#RepeatFeatureProvenance";

  protected RepeatFeatureProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static RepeatFeatureProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,RepeatFeatureProvenance.class);
      if(toRet == null) {
        toRet = new RepeatFeatureProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof RepeatFeatureProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.RepeatFeatureProvenanceImpl expected");
      }
      return (RepeatFeatureProvenance)toRet;
    }
  }

  public void validate() {
  }
}
