package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Terminator;
import life.gbol.domain.TerminatorProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class TerminatorImpl extends RegulationSiteImpl implements Terminator {
  public static final String TypeIRI = "http://gbol.life#Terminator";

  protected TerminatorImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Terminator make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Terminator.class);
      if(toRet == null) {
        toRet = new TerminatorImpl(domain,iri);;
      }
      else if(!(toRet instanceof Terminator)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.TerminatorImpl expected");
      }
      return (Terminator)toRet;
    }
  }

  public void validate() {
  }

  public TerminatorProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,TerminatorProvenance.class);
  }

  public void setProvenance(TerminatorProvenance val) {
    this.setRef("http://gbol.life#provenance",val,TerminatorProvenance.class);
  }
}
