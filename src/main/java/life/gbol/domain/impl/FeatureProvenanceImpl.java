package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.AnnotationResult;
import life.gbol.domain.FeatureProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class FeatureProvenanceImpl extends ProvenanceImpl implements FeatureProvenance {
  public static final String TypeIRI = "http://gbol.life#FeatureProvenance";

  protected FeatureProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static FeatureProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,FeatureProvenance.class);
      if(toRet == null) {
        toRet = new FeatureProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof FeatureProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.FeatureProvenanceImpl expected");
      }
      return (FeatureProvenance)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#origin");
  }

  public AnnotationResult getOrigin() {
    return this.getRef("http://gbol.life#origin",false,AnnotationResult.class);
  }

  public void setOrigin(AnnotationResult val) {
    this.setRef("http://gbol.life#origin",val,AnnotationResult.class);
  }
}
