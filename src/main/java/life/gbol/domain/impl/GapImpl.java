package life.gbol.domain.impl;

import java.lang.Integer;
import java.lang.String;
import life.gbol.domain.Gap;
import life.gbol.domain.GapProvenance;
import life.gbol.domain.LinkageEvidence;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class GapImpl extends AssemblyAnnotationImpl implements Gap {
  public static final String TypeIRI = "http://gbol.life#Gap";

  protected GapImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Gap make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Gap.class);
      if(toRet == null) {
        toRet = new GapImpl(domain,iri);;
      }
      else if(!(toRet instanceof Gap)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.GapImpl expected");
      }
      return (Gap)toRet;
    }
  }

  public void validate() {
  }

  public LinkageEvidence getLinkage_evidence() {
    return this.getEnum("http://gbol.life#linkage_evidence",true,LinkageEvidence.class);
  }

  public void setLinkage_evidence(LinkageEvidence val) {
    this.setEnum("http://gbol.life#linkage_evidence",val,LinkageEvidence.class);
  }

  public Integer getEstimated_length() {
    return this.getIntegerLit("http://gbol.life#estimated_length",true);
  }

  public void setEstimated_length(Integer val) {
    this.setIntegerLit("http://gbol.life#estimated_length",val);
  }

  public GapProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,GapProvenance.class);
  }

  public void setProvenance(GapProvenance val) {
    this.setRef("http://gbol.life#provenance",val,GapProvenance.class);
  }
}
