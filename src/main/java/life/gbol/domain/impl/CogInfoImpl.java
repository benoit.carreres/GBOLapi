package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.CogInfo;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class CogInfoImpl extends ProteinFeatureProvenanceImpl implements CogInfo {
  public static final String TypeIRI = "http://gbol.life#CogInfo";

  protected CogInfoImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static CogInfo make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,CogInfo.class);
      if(toRet == null) {
        toRet = new CogInfoImpl(domain,iri);;
      }
      else if(!(toRet instanceof CogInfo)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.CogInfoImpl expected");
      }
      return (CogInfo)toRet;
    }
  }

  public void validate() {
  }

  public String getLetter() {
    return this.getStringLit("http://gbol.life#letter",true);
  }

  public void setLetter(String val) {
    this.setStringLit("http://gbol.life#letter",val);
  }

  public String getLabel() {
    return this.getStringLit("http://gbol.life#label",true);
  }

  public void setLabel(String val) {
    this.setStringLit("http://gbol.life#label",val);
  }

  public String getDescription() {
    return this.getStringLit("http://gbol.life#description",true);
  }

  public void setDescription(String val) {
    this.setStringLit("http://gbol.life#description",val);
  }
}
