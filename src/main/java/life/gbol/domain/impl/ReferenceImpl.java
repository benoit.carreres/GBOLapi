package life.gbol.domain.impl;

import java.lang.Integer;
import java.lang.String;
import java.util.List;
import life.gbol.domain.Location;
import life.gbol.domain.Publication;
import life.gbol.domain.Reference;
import life.gbol.domain.XRef;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ReferenceImpl extends OWLThingImpl implements Reference {
  public static final String TypeIRI = "http://gbol.life#Reference";

  protected ReferenceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Reference make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Reference.class);
      if(toRet == null) {
        toRet = new ReferenceImpl(domain,iri);;
      }
      else if(!(toRet instanceof Reference)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ReferenceImpl expected");
      }
      return (Reference)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#locations");
    this.checkCardMin1("http://www.w3.org/2000/01/rdf-schema#comment");
  }

  public void remLocations(Location val) {
    this.remRef("http://gbol.life#locations",val,false);
  }

  public List<? extends Location> getAllLocations() {
    return this.getRefSet("http://gbol.life#locations",false,Location.class);
  }

  public void addLocations(Location val) {
    this.addRef("http://gbol.life#locations",val);
  }

  public void remXref(XRef val) {
    this.remRef("http://gbol.life#xref",val,true);
  }

  public List<? extends XRef> getAllXref() {
    return this.getRefSet("http://gbol.life#xref",true,XRef.class);
  }

  public void addXref(XRef val) {
    this.addRef("http://gbol.life#xref",val);
  }

  public String getComment() {
    return this.getStringLit("http://www.w3.org/2000/01/rdf-schema#comment",false);
  }

  public void setComment(String val) {
    this.setStringLit("http://www.w3.org/2000/01/rdf-schema#comment",val);
  }

  public String getId() {
    return this.getStringLit("http://gbol.life#id",true);
  }

  public void setId(String val) {
    this.setStringLit("http://gbol.life#id",val);
  }

  public Publication getPublication() {
    return this.getRef("http://gbol.life#publication",true,Publication.class);
  }

  public void setPublication(Publication val) {
    this.setRef("http://gbol.life#publication",val,Publication.class);
  }

  public Integer getReferenceNumber() {
    return this.getIntegerLit("http://gbol.life#referenceNumber",true);
  }

  public void setReferenceNumber(Integer val) {
    this.setIntegerLit("http://gbol.life#referenceNumber",val);
  }
}
