package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.MiscFeatureProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class MiscFeatureProvenanceImpl extends NAFeatureProvenanceImpl implements MiscFeatureProvenance {
  public static final String TypeIRI = "http://gbol.life#MiscFeatureProvenance";

  protected MiscFeatureProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static MiscFeatureProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,MiscFeatureProvenance.class);
      if(toRet == null) {
        toRet = new MiscFeatureProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof MiscFeatureProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.MiscFeatureProvenanceImpl expected");
      }
      return (MiscFeatureProvenance)toRet;
    }
  }

  public void validate() {
  }
}
