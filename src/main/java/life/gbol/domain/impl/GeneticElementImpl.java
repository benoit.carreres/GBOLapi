package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.GeneticElement;
import life.gbol.domain.GeneticElementProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class GeneticElementImpl extends GeneRelatedElementsImpl implements GeneticElement {
  public static final String TypeIRI = "http://gbol.life#GeneticElement";

  protected GeneticElementImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static GeneticElement make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,GeneticElement.class);
      if(toRet == null) {
        toRet = new GeneticElementImpl(domain,iri);;
      }
      else if(!(toRet instanceof GeneticElement)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.GeneticElementImpl expected");
      }
      return (GeneticElement)toRet;
    }
  }

  public void validate() {
  }

  public GeneticElementProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,GeneticElementProvenance.class);
  }

  public void setProvenance(GeneticElementProvenance val) {
    this.setRef("http://gbol.life#provenance",val,GeneticElementProvenance.class);
  }
}
