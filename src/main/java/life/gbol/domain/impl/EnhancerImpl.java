package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Enhancer;
import life.gbol.domain.EnhancerProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class EnhancerImpl extends RegulationSiteImpl implements Enhancer {
  public static final String TypeIRI = "http://gbol.life#Enhancer";

  protected EnhancerImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Enhancer make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Enhancer.class);
      if(toRet == null) {
        toRet = new EnhancerImpl(domain,iri);;
      }
      else if(!(toRet instanceof Enhancer)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.EnhancerImpl expected");
      }
      return (Enhancer)toRet;
    }
  }

  public void validate() {
  }

  public EnhancerProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,EnhancerProvenance.class);
  }

  public void setProvenance(EnhancerProvenance val) {
    this.setRef("http://gbol.life#provenance",val,EnhancerProvenance.class);
  }
}
