package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.UnPublished;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class UnPublishedImpl extends PublicationImpl implements UnPublished {
  public static final String TypeIRI = "http://gbol.life#UnPublished";

  protected UnPublishedImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static UnPublished make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,UnPublished.class);
      if(toRet == null) {
        toRet = new UnPublishedImpl(domain,iri);;
      }
      else if(!(toRet instanceof UnPublished)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.UnPublishedImpl expected");
      }
      return (UnPublished)toRet;
    }
  }

  public void validate() {
  }
}
