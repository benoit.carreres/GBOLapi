package life.gbol.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.Person;
import life.gbol.domain.Publication;
import life.gbol.domain.XRef;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;

/**
 * Code generated from http://gbol.life# ontology
 */
public class PublicationImpl extends OWLThingImpl implements Publication {
  public static final String TypeIRI = "http://gbol.life#Publication";

  protected PublicationImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Publication make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Publication.class);
      if(toRet == null) {
        toRet = new PublicationImpl(domain,iri);;
      }
      else if(!(toRet instanceof Publication)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.PublicationImpl expected");
      }
      return (Publication)toRet;
    }
  }

  public void validate() {
  }

  public void remXRefs(XRef val) {
    this.remRef("http://gbol.life#xRefs",val,true);
  }

  public List<? extends XRef> getAllXRefs() {
    return this.getRefSet("http://gbol.life#xRefs",true,XRef.class);
  }

  public void addXRefs(XRef val) {
    this.addRef("http://gbol.life#xRefs",val);
  }

  public String getId() {
    return this.getStringLit("http://gbol.life#id",true);
  }

  public void setId(String val) {
    this.setStringLit("http://gbol.life#id",val);
  }

  public String getConsortium() {
    return this.getStringLit("http://gbol.life#consortium",true);
  }

  public void setConsortium(String val) {
    this.setStringLit("http://gbol.life#consortium",val);
  }

  public String getTitle() {
    return this.getStringLit("http://gbol.life#title",true);
  }

  public void setTitle(String val) {
    this.setStringLit("http://gbol.life#title",val);
  }

  public void remAuthors(Person val) {
    this.remRef("http://gbol.life#authors",val,true);
  }

  public List<? extends Person> getAllAuthors() {
    return this.getRefSet("http://gbol.life#authors",true,Person.class);
  }

  public void addAuthors(Person val) {
    this.addRef("http://gbol.life#authors",val);
  }
}
