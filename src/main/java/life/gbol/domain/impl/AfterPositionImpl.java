package life.gbol.domain.impl;

import java.lang.Integer;
import java.lang.String;
import life.gbol.domain.AfterPosition;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class AfterPositionImpl extends FuzzyPositionImpl implements AfterPosition {
  public static final String TypeIRI = "http://gbol.life#AfterPosition";

  protected AfterPositionImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static AfterPosition make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,AfterPosition.class);
      if(toRet == null) {
        toRet = new AfterPositionImpl(domain,iri);;
      }
      else if(!(toRet instanceof AfterPosition)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.AfterPositionImpl expected");
      }
      return (AfterPosition)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#position");
  }

  public Integer getPosition() {
    return this.getIntegerLit("http://gbol.life#position",false);
  }

  public void setPosition(Integer val) {
    this.setIntegerLit("http://gbol.life#position",val);
  }
}
