package life.gbol.domain.impl;

import java.lang.Double;
import java.lang.Integer;
import java.lang.String;
import life.gbol.domain.HMM;
import life.gbol.domain.Location;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class HMMImpl extends ProteinFeatureProvenanceImpl implements HMM {
  public static final String TypeIRI = "http://gbol.life#HMM";

  protected HMMImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static HMM make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,HMM.class);
      if(toRet == null) {
        toRet = new HMMImpl(domain,iri);;
      }
      else if(!(toRet instanceof HMM)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.HMMImpl expected");
      }
      return (HMM)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#TDbias");
    this.checkCardMin1("http://gbol.life#TDcevalue");
    this.checkCardMin1("http://gbol.life#TDievalue");
    this.checkCardMin1("http://gbol.life#FSscore");
    this.checkCardMin1("http://gbol.life#qlen");
    this.checkCardMin1("http://gbol.life#FSevalue");
    this.checkCardMin1("http://gbol.life#TDscore");
    this.checkCardMin1("http://gbol.life#FSbias");
    this.checkCardMin1("http://gbol.life#TDdomain_number");
    this.checkCardMin1("http://gbol.life#ENVlocation");
    this.checkCardMin1("http://gbol.life#ALIlocation");
    this.checkCardMin1("http://gbol.life#query_name");
    this.checkCardMin1("http://gbol.life#TDdomains_total");
    this.checkCardMin1("http://gbol.life#accuracy");
    this.checkCardMin1("http://gbol.life#HMMlocation");
  }

  public Double getTDbias() {
    return this.getDoubleLit("http://gbol.life#TDbias",false);
  }

  public void setTDbias(Double val) {
    this.setDoubleLit("http://gbol.life#TDbias",val);
  }

  public Double getTDcevalue() {
    return this.getDoubleLit("http://gbol.life#TDcevalue",false);
  }

  public void setTDcevalue(Double val) {
    this.setDoubleLit("http://gbol.life#TDcevalue",val);
  }

  public Double getTDievalue() {
    return this.getDoubleLit("http://gbol.life#TDievalue",false);
  }

  public void setTDievalue(Double val) {
    this.setDoubleLit("http://gbol.life#TDievalue",val);
  }

  public String getQuery_accession() {
    return this.getStringLit("http://gbol.life#query_accession",true);
  }

  public void setQuery_accession(String val) {
    this.setStringLit("http://gbol.life#query_accession",val);
  }

  public Double getFSscore() {
    return this.getDoubleLit("http://gbol.life#FSscore",false);
  }

  public void setFSscore(Double val) {
    this.setDoubleLit("http://gbol.life#FSscore",val);
  }

  public Integer getQlen() {
    return this.getIntegerLit("http://gbol.life#qlen",false);
  }

  public void setQlen(Integer val) {
    this.setIntegerLit("http://gbol.life#qlen",val);
  }

  public Double getFSevalue() {
    return this.getDoubleLit("http://gbol.life#FSevalue",false);
  }

  public void setFSevalue(Double val) {
    this.setDoubleLit("http://gbol.life#FSevalue",val);
  }

  public Double getTDscore() {
    return this.getDoubleLit("http://gbol.life#TDscore",false);
  }

  public void setTDscore(Double val) {
    this.setDoubleLit("http://gbol.life#TDscore",val);
  }

  public Double getFSbias() {
    return this.getDoubleLit("http://gbol.life#FSbias",false);
  }

  public void setFSbias(Double val) {
    this.setDoubleLit("http://gbol.life#FSbias",val);
  }

  public String getDescription() {
    return this.getStringLit("http://gbol.life#description",true);
  }

  public void setDescription(String val) {
    this.setStringLit("http://gbol.life#description",val);
  }

  public Integer getTDdomain_number() {
    return this.getIntegerLit("http://gbol.life#TDdomain_number",false);
  }

  public void setTDdomain_number(Integer val) {
    this.setIntegerLit("http://gbol.life#TDdomain_number",val);
  }

  public Location getENVlocation() {
    return this.getRef("http://gbol.life#ENVlocation",false,Location.class);
  }

  public void setENVlocation(Location val) {
    this.setRef("http://gbol.life#ENVlocation",val,Location.class);
  }

  public Location getALIlocation() {
    return this.getRef("http://gbol.life#ALIlocation",false,Location.class);
  }

  public void setALIlocation(Location val) {
    this.setRef("http://gbol.life#ALIlocation",val,Location.class);
  }

  public String getQuery_name() {
    return this.getStringLit("http://gbol.life#query_name",false);
  }

  public void setQuery_name(String val) {
    this.setStringLit("http://gbol.life#query_name",val);
  }

  public Integer getTDdomains_total() {
    return this.getIntegerLit("http://gbol.life#TDdomains_total",false);
  }

  public void setTDdomains_total(Integer val) {
    this.setIntegerLit("http://gbol.life#TDdomains_total",val);
  }

  public Double getAccuracy() {
    return this.getDoubleLit("http://gbol.life#accuracy",false);
  }

  public void setAccuracy(Double val) {
    this.setDoubleLit("http://gbol.life#accuracy",val);
  }

  public Location getHMMlocation() {
    return this.getRef("http://gbol.life#HMMlocation",false,Location.class);
  }

  public void setHMMlocation(Location val) {
    this.setRef("http://gbol.life#HMMlocation",val,Location.class);
  }
}
