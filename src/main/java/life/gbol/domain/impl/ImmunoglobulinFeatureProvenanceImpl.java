package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.ImmunoglobulinFeatureProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ImmunoglobulinFeatureProvenanceImpl extends ProteinFeatureProvenanceImpl implements ImmunoglobulinFeatureProvenance {
  public static final String TypeIRI = "http://gbol.life#ImmunoglobulinFeatureProvenance";

  protected ImmunoglobulinFeatureProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static ImmunoglobulinFeatureProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,ImmunoglobulinFeatureProvenance.class);
      if(toRet == null) {
        toRet = new ImmunoglobulinFeatureProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof ImmunoglobulinFeatureProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ImmunoglobulinFeatureProvenanceImpl expected");
      }
      return (ImmunoglobulinFeatureProvenance)toRet;
    }
  }

  public void validate() {
  }
}
