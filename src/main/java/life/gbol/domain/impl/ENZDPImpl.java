package life.gbol.domain.impl;

import java.lang.Float;
import java.lang.String;
import java.util.List;
import life.gbol.domain.ENZDP;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ENZDPImpl extends ProteinFeatureProvenanceImpl implements ENZDP {
  public static final String TypeIRI = "http://gbol.life#ENZDP";

  protected ENZDPImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static ENZDP make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,ENZDP.class);
      if(toRet == null) {
        toRet = new ENZDPImpl(domain,iri);;
      }
      else if(!(toRet instanceof ENZDP)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ENZDPImpl expected");
      }
      return (ENZDP)toRet;
    }
  }

  public void validate() {
  }

  public String getAmbigous() {
    return this.getStringLit("http://gbol.life#ambigous",true);
  }

  public void setAmbigous(String val) {
    this.setStringLit("http://gbol.life#ambigous",val);
  }

  public String getRatio() {
    return this.getStringLit("http://gbol.life#ratio",true);
  }

  public void setRatio(String val) {
    this.setStringLit("http://gbol.life#ratio",val);
  }

  public void remPattern(String val) {
    this.remStringLit("http://gbol.life#pattern",val,true);
  }

  public List<? extends String> getAllPattern() {
    return this.getStringLitSet("http://gbol.life#pattern",true);
  }

  public void addPattern(String val) {
    this.addStringLit("http://gbol.life#pattern",val);
  }

  public Float getMaxbitscore() {
    return this.getFloatLit("http://gbol.life#maxbitscore",true);
  }

  public void setMaxbitscore(Float val) {
    this.setFloatLit("http://gbol.life#maxbitscore",val);
  }

  public String getSupport() {
    return this.getStringLit("http://gbol.life#support",true);
  }

  public void setSupport(String val) {
    this.setStringLit("http://gbol.life#support",val);
  }

  public Float getLikelihoodscore() {
    return this.getFloatLit("http://gbol.life#likelihoodscore",true);
  }

  public void setLikelihoodscore(Float val) {
    this.setFloatLit("http://gbol.life#likelihoodscore",val);
  }
}
