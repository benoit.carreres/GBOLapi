package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.JoiningSegment;
import life.gbol.domain.JoiningSegmentProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class JoiningSegmentImpl extends ImmunoglobulinFeatureImpl implements JoiningSegment {
  public static final String TypeIRI = "http://gbol.life#JoiningSegment";

  protected JoiningSegmentImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static JoiningSegment make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,JoiningSegment.class);
      if(toRet == null) {
        toRet = new JoiningSegmentImpl(domain,iri);;
      }
      else if(!(toRet instanceof JoiningSegment)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.JoiningSegmentImpl expected");
      }
      return (JoiningSegment)toRet;
    }
  }

  public void validate() {
  }

  public JoiningSegmentProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,JoiningSegmentProvenance.class);
  }

  public void setProvenance(JoiningSegmentProvenance val) {
    this.setRef("http://gbol.life#provenance",val,JoiningSegmentProvenance.class);
  }
}
