package life.gbol.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.Feature;
import life.gbol.domain.NAObject;
import life.gbol.domain.StrandType;
import life.gbol.domain.Topology;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class NAObjectImpl extends SequenceObjectImpl implements NAObject {
  public static final String TypeIRI = "http://gbol.life#NAObject";

  protected NAObjectImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static NAObject make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,NAObject.class);
      if(toRet == null) {
        toRet = new NAObjectImpl(domain,iri);;
      }
      else if(!(toRet instanceof NAObject)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.NAObjectImpl expected");
      }
      return (NAObject)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#strandType");
    this.checkCardMin1("http://gbol.life#accession");
  }

  public String getAnticodonmessage() {
    return this.getStringLit("http://gbol.life#anticodonmessage",true);
  }

  public void setAnticodonmessage(String val) {
    this.setStringLit("http://gbol.life#anticodonmessage",val);
  }

  public void remFeature(Feature val) {
    this.remRef("http://gbol.life#feature",val,true);
  }

  public List<? extends Feature> getAllFeature() {
    return this.getRefSet("http://gbol.life#feature",true,Feature.class);
  }

  public void addFeature(Feature val) {
    this.addRef("http://gbol.life#feature",val);
  }

  public String getScientificname() {
    return this.getStringLit("http://gbol.life#scientificname",true);
  }

  public void setScientificname(String val) {
    this.setStringLit("http://gbol.life#scientificname",val);
  }

  public StrandType getStrandType() {
    return this.getEnum("http://gbol.life#strandType",false,StrandType.class);
  }

  public void setStrandType(StrandType val) {
    this.setEnum("http://gbol.life#strandType",val,StrandType.class);
  }

  public Topology getTopology() {
    return this.getEnum("http://gbol.life#topology",true,Topology.class);
  }

  public void setTopology(Topology val) {
    this.setEnum("http://gbol.life#topology",val,Topology.class);
  }

  public String getSecondaryAccession() {
    return this.getStringLit("http://gbol.life#secondaryAccession",true);
  }

  public void setSecondaryAccession(String val) {
    this.setStringLit("http://gbol.life#secondaryAccession",val);
  }

  public String getMol_type() {
    return this.getStringLit("http://gbol.life#mol_type",true);
  }

  public void setMol_type(String val) {
    this.setStringLit("http://gbol.life#mol_type",val);
  }

  public String getCommonname() {
    return this.getStringLit("http://gbol.life#commonname",true);
  }

  public void setCommonname(String val) {
    this.setStringLit("http://gbol.life#commonname",val);
  }

  public String getDescription() {
    return this.getStringLit("http://gbol.life#description",true);
  }

  public void setDescription(String val) {
    this.setStringLit("http://gbol.life#description",val);
  }

  public String getAccession() {
    return this.getStringLit("http://gbol.life#accession",false);
  }

  public void setAccession(String val) {
    this.setStringLit("http://gbol.life#accession",val);
  }

  public String getCitation() {
    return this.getStringLit("http://gbol.life#citation",true);
  }

  public void setCitation(String val) {
    this.setStringLit("http://gbol.life#citation",val);
  }

  public String getDataclass() {
    return this.getStringLit("http://gbol.life#dataclass",true);
  }

  public void setDataclass(String val) {
    this.setStringLit("http://gbol.life#dataclass",val);
  }

  public String getDivision() {
    return this.getStringLit("http://gbol.life#division",true);
  }

  public void setDivision(String val) {
    this.setStringLit("http://gbol.life#division",val);
  }
}
