package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.CompleteNAObject;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class CompleteNAObjectImpl extends DNAObjectImpl implements CompleteNAObject {
  public static final String TypeIRI = "http://gbol.life#CompleteNAObject";

  protected CompleteNAObjectImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static CompleteNAObject make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,CompleteNAObject.class);
      if(toRet == null) {
        toRet = new CompleteNAObjectImpl(domain,iri);;
      }
      else if(!(toRet instanceof CompleteNAObject)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.CompleteNAObjectImpl expected");
      }
      return (CompleteNAObject)toRet;
    }
  }

  public void validate() {
  }
}
