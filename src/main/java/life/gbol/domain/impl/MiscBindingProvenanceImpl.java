package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.MiscBindingProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class MiscBindingProvenanceImpl extends BiologicalRecognizedRegionProvenanceImpl implements MiscBindingProvenance {
  public static final String TypeIRI = "http://gbol.life#MiscBindingProvenance";

  protected MiscBindingProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static MiscBindingProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,MiscBindingProvenance.class);
      if(toRet == null) {
        toRet = new MiscBindingProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof MiscBindingProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.MiscBindingProvenanceImpl expected");
      }
      return (MiscBindingProvenance)toRet;
    }
  }

  public void validate() {
  }
}
