package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.ncRNA;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ncRNAImpl extends MaturedRNAImpl implements ncRNA {
  public static final String TypeIRI = "http://gbol.life#ncRNA";

  protected ncRNAImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static ncRNA make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,ncRNA.class);
      if(toRet == null) {
        toRet = new ncRNAImpl(domain,iri);;
      }
      else if(!(toRet instanceof ncRNA)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ncRNAImpl expected");
      }
      return (ncRNA)toRet;
    }
  }

  public void validate() {
  }

  public String getNcrna_class() {
    return this.getStringLit("http://gbol.life#ncrna_class",true);
  }

  public void setNcrna_class(String val) {
    this.setStringLit("http://gbol.life#ncrna_class",val);
  }
}
