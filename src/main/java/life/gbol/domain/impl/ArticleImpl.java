package life.gbol.domain.impl;

import java.lang.Integer;
import java.lang.String;
import life.gbol.domain.Article;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ArticleImpl extends PublicationImpl implements Article {
  public static final String TypeIRI = "http://gbol.life#Article";

  protected ArticleImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Article make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Article.class);
      if(toRet == null) {
        toRet = new ArticleImpl(domain,iri);;
      }
      else if(!(toRet instanceof Article)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ArticleImpl expected");
      }
      return (Article)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#journal");
  }

  public String getIssue() {
    return this.getStringLit("http://gbol.life#issue",true);
  }

  public void setIssue(String val) {
    this.setStringLit("http://gbol.life#issue",val);
  }

  public String getLastPage() {
    return this.getStringLit("http://gbol.life#lastPage",true);
  }

  public void setLastPage(String val) {
    this.setStringLit("http://gbol.life#lastPage",val);
  }

  public String getVolume() {
    return this.getStringLit("http://gbol.life#volume",true);
  }

  public void setVolume(String val) {
    this.setStringLit("http://gbol.life#volume",val);
  }

  public Integer getYear() {
    return this.getIntegerLit("http://gbol.life#year",true);
  }

  public void setYear(Integer val) {
    this.setIntegerLit("http://gbol.life#year",val);
  }

  public String getFirstPage() {
    return this.getStringLit("http://gbol.life#firstPage",true);
  }

  public void setFirstPage(String val) {
    this.setStringLit("http://gbol.life#firstPage",val);
  }

  public String getJournal() {
    return this.getStringLit("http://gbol.life#journal",false);
  }

  public void setJournal(String val) {
    this.setStringLit("http://gbol.life#journal",val);
  }
}
