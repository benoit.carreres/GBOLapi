package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.TransferOrigin;
import life.gbol.domain.TransferOriginProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class TransferOriginImpl extends BiologicalRecognizedRegionImpl implements TransferOrigin {
  public static final String TypeIRI = "http://gbol.life#TransferOrigin";

  protected TransferOriginImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static TransferOrigin make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,TransferOrigin.class);
      if(toRet == null) {
        toRet = new TransferOriginImpl(domain,iri);;
      }
      else if(!(toRet instanceof TransferOrigin)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.TransferOriginImpl expected");
      }
      return (TransferOrigin)toRet;
    }
  }

  public void validate() {
  }

  public TransferOriginProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,TransferOriginProvenance.class);
  }

  public void setProvenance(TransferOriginProvenance val) {
    this.setRef("http://gbol.life#provenance",val,TransferOriginProvenance.class);
  }
}
