package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.VariableRegion;
import life.gbol.domain.VariableRegionProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class VariableRegionImpl extends ImmunoglobulinFeatureImpl implements VariableRegion {
  public static final String TypeIRI = "http://gbol.life#VariableRegion";

  protected VariableRegionImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static VariableRegion make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,VariableRegion.class);
      if(toRet == null) {
        toRet = new VariableRegionImpl(domain,iri);;
      }
      else if(!(toRet instanceof VariableRegion)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.VariableRegionImpl expected");
      }
      return (VariableRegion)toRet;
    }
  }

  public void validate() {
  }

  public VariableRegionProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,VariableRegionProvenance.class);
  }

  public void setProvenance(VariableRegionProvenance val) {
    this.setRef("http://gbol.life#provenance",val,VariableRegionProvenance.class);
  }
}
