package life.gbol.domain.impl;

import java.lang.Double;
import java.lang.String;
import life.gbol.domain.Interproscan;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class InterproscanImpl extends ProteinFeatureProvenanceImpl implements Interproscan {
  public static final String TypeIRI = "http://gbol.life#Interproscan";

  protected InterproscanImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Interproscan make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Interproscan.class);
      if(toRet == null) {
        toRet = new InterproscanImpl(domain,iri);;
      }
      else if(!(toRet instanceof Interproscan)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.InterproscanImpl expected");
      }
      return (Interproscan)toRet;
    }
  }

  public void validate() {
  }

  public String getSignature_description() {
    return this.getStringLit("http://gbol.life#signature_description",true);
  }

  public void setSignature_description(String val) {
    this.setStringLit("http://gbol.life#signature_description",val);
  }

  public Double getScore() {
    return this.getDoubleLit("http://gbol.life#score",true);
  }

  public void setScore(Double val) {
    this.setDoubleLit("http://gbol.life#score",val);
  }

  public String getVersion() {
    return this.getStringLit("http://gbol.life#version",true);
  }

  public void setVersion(String val) {
    this.setStringLit("http://gbol.life#version",val);
  }

  public String getInterpro_description() {
    return this.getStringLit("http://gbol.life#interpro_description",true);
  }

  public void setInterpro_description(String val) {
    this.setStringLit("http://gbol.life#interpro_description",val);
  }

  public String getAnalysis() {
    return this.getStringLit("http://gbol.life#analysis",true);
  }

  public void setAnalysis(String val) {
    this.setStringLit("http://gbol.life#analysis",val);
  }
}
