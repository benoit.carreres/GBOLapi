package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.GCSignal;
import life.gbol.domain.GCSignalProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class GCSignalImpl extends RegulationSiteImpl implements GCSignal {
  public static final String TypeIRI = "http://gbol.life#GCSignal";

  protected GCSignalImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static GCSignal make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,GCSignal.class);
      if(toRet == null) {
        toRet = new GCSignalImpl(domain,iri);;
      }
      else if(!(toRet instanceof GCSignal)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.GCSignalImpl expected");
      }
      return (GCSignal)toRet;
    }
  }

  public void validate() {
  }

  public GCSignalProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,GCSignalProvenance.class);
  }

  public void setProvenance(GCSignalProvenance val) {
    this.setRef("http://gbol.life#provenance",val,GCSignalProvenance.class);
  }
}
