package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.OldSequence;
import life.gbol.domain.OldSequenceProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class OldSequenceImpl extends AssemblyAnnotationImpl implements OldSequence {
  public static final String TypeIRI = "http://gbol.life#OldSequence";

  protected OldSequenceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static OldSequence make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,OldSequence.class);
      if(toRet == null) {
        toRet = new OldSequenceImpl(domain,iri);;
      }
      else if(!(toRet instanceof OldSequence)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.OldSequenceImpl expected");
      }
      return (OldSequence)toRet;
    }
  }

  public void validate() {
  }

  public OldSequenceProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,OldSequenceProvenance.class);
  }

  public void setProvenance(OldSequenceProvenance val) {
    this.setRef("http://gbol.life#provenance",val,OldSequenceProvenance.class);
  }
}
