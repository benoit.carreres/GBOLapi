package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Minus10SignalProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class Minus10SignalProvenanceImpl extends RegulationSiteProvenanceImpl implements Minus10SignalProvenance {
  public static final String TypeIRI = "http://gbol.life#Minus10SignalProvenance";

  protected Minus10SignalProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Minus10SignalProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Minus10SignalProvenance.class);
      if(toRet == null) {
        toRet = new Minus10SignalProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof Minus10SignalProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.Minus10SignalProvenanceImpl expected");
      }
      return (Minus10SignalProvenance)toRet;
    }
  }

  public void validate() {
  }
}
