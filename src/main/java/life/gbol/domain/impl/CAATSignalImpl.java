package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.CAATSignal;
import life.gbol.domain.CAATSignalProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class CAATSignalImpl extends RegulationSiteImpl implements CAATSignal {
  public static final String TypeIRI = "http://gbol.life#CAATSignal";

  protected CAATSignalImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static CAATSignal make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,CAATSignal.class);
      if(toRet == null) {
        toRet = new CAATSignalImpl(domain,iri);;
      }
      else if(!(toRet instanceof CAATSignal)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.CAATSignalImpl expected");
      }
      return (CAATSignal)toRet;
    }
  }

  public void validate() {
  }

  public CAATSignalProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,CAATSignalProvenance.class);
  }

  public void setProvenance(CAATSignalProvenance val) {
    this.setRef("http://gbol.life#provenance",val,CAATSignalProvenance.class);
  }
}
