package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Telomere;
import life.gbol.domain.TelomereProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class TelomereImpl extends RepeatFeatureImpl implements Telomere {
  public static final String TypeIRI = "http://gbol.life#Telomere";

  protected TelomereImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Telomere make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Telomere.class);
      if(toRet == null) {
        toRet = new TelomereImpl(domain,iri);;
      }
      else if(!(toRet instanceof Telomere)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.TelomereImpl expected");
      }
      return (Telomere)toRet;
    }
  }

  public void validate() {
  }

  public TelomereProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,TelomereProvenance.class);
  }

  public void setProvenance(TelomereProvenance val) {
    this.setRef("http://gbol.life#provenance",val,TelomereProvenance.class);
  }
}
