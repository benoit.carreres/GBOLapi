package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.ReplicationOrigin;
import life.gbol.domain.ReplicationOriginProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ReplicationOriginImpl extends BiologicalRecognizedRegionImpl implements ReplicationOrigin {
  public static final String TypeIRI = "http://gbol.life#ReplicationOrigin";

  protected ReplicationOriginImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static ReplicationOrigin make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,ReplicationOrigin.class);
      if(toRet == null) {
        toRet = new ReplicationOriginImpl(domain,iri);;
      }
      else if(!(toRet instanceof ReplicationOrigin)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ReplicationOriginImpl expected");
      }
      return (ReplicationOrigin)toRet;
    }
  }

  public void validate() {
  }

  public String getDirection() {
    return this.getStringLit("http://gbol.life#direction",true);
  }

  public void setDirection(String val) {
    this.setStringLit("http://gbol.life#direction",val);
  }

  public ReplicationOriginProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,ReplicationOriginProvenance.class);
  }

  public void setProvenance(ReplicationOriginProvenance val) {
    this.setRef("http://gbol.life#provenance",val,ReplicationOriginProvenance.class);
  }
}
