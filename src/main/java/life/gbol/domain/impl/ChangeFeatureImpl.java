package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.ChangeFeature;
import life.gbol.domain.ChangeFeatureProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ChangeFeatureImpl extends SequenceAnnotationImpl implements ChangeFeature {
  public static final String TypeIRI = "http://gbol.life#ChangeFeature";

  protected ChangeFeatureImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static ChangeFeature make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,ChangeFeature.class);
      if(toRet == null) {
        toRet = new ChangeFeatureImpl(domain,iri);;
      }
      else if(!(toRet instanceof ChangeFeature)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ChangeFeatureImpl expected");
      }
      return (ChangeFeature)toRet;
    }
  }

  public void validate() {
  }

  public ChangeFeatureProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,ChangeFeatureProvenance.class);
  }

  public void setProvenance(ChangeFeatureProvenance val) {
    this.setRef("http://gbol.life#provenance",val,ChangeFeatureProvenance.class);
  }
}
