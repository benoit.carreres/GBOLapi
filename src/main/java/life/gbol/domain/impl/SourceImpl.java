package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Source;
import life.gbol.domain.SourceProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class SourceImpl extends NAFeatureImpl implements Source {
  public static final String TypeIRI = "http://gbol.life#Source";

  protected SourceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Source make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Source.class);
      if(toRet == null) {
        toRet = new SourceImpl(domain,iri);;
      }
      else if(!(toRet instanceof Source)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.SourceImpl expected");
      }
      return (Source)toRet;
    }
  }

  public void validate() {
  }

  public SourceProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,SourceProvenance.class);
  }

  public void setProvenance(SourceProvenance val) {
    this.setRef("http://gbol.life#provenance",val,SourceProvenance.class);
  }
}
