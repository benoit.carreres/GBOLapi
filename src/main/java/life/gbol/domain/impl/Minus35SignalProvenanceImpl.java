package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Minus35SignalProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class Minus35SignalProvenanceImpl extends RegulationSiteProvenanceImpl implements Minus35SignalProvenance {
  public static final String TypeIRI = "http://gbol.life#Minus35SignalProvenance";

  protected Minus35SignalProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Minus35SignalProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Minus35SignalProvenance.class);
      if(toRet == null) {
        toRet = new Minus35SignalProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof Minus35SignalProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.Minus35SignalProvenanceImpl expected");
      }
      return (Minus35SignalProvenance)toRet;
    }
  }

  public void validate() {
  }
}
