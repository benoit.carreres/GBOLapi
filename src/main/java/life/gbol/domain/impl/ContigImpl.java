package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Contig;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ContigImpl extends UncompleteNAObjectImpl implements Contig {
  public static final String TypeIRI = "http://gbol.life#Contig";

  protected ContigImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Contig make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Contig.class);
      if(toRet == null) {
        toRet = new ContigImpl(domain,iri);;
      }
      else if(!(toRet instanceof Contig)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ContigImpl expected");
      }
      return (Contig)toRet;
    }
  }

  public void validate() {
  }
}
