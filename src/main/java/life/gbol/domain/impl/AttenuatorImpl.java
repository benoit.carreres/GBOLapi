package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Attenuator;
import life.gbol.domain.AttenuatorProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class AttenuatorImpl extends RegulationSiteImpl implements Attenuator {
  public static final String TypeIRI = "http://gbol.life#Attenuator";

  protected AttenuatorImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Attenuator make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Attenuator.class);
      if(toRet == null) {
        toRet = new AttenuatorImpl(domain,iri);;
      }
      else if(!(toRet instanceof Attenuator)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.AttenuatorImpl expected");
      }
      return (Attenuator)toRet;
    }
  }

  public void validate() {
  }

  public AttenuatorProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,AttenuatorProvenance.class);
  }

  public void setProvenance(AttenuatorProvenance val) {
    this.setRef("http://gbol.life#provenance",val,AttenuatorProvenance.class);
  }
}
