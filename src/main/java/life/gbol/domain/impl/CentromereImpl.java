package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Centromere;
import life.gbol.domain.CentromereProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class CentromereImpl extends RepeatFeatureImpl implements Centromere {
  public static final String TypeIRI = "http://gbol.life#Centromere";

  protected CentromereImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Centromere make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Centromere.class);
      if(toRet == null) {
        toRet = new CentromereImpl(domain,iri);;
      }
      else if(!(toRet instanceof Centromere)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.CentromereImpl expected");
      }
      return (Centromere)toRet;
    }
  }

  public void validate() {
  }

  public CentromereProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,CentromereProvenance.class);
  }

  public void setProvenance(CentromereProvenance val) {
    this.setRef("http://gbol.life#provenance",val,CentromereProvenance.class);
  }
}
