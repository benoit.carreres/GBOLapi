package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Chromosome;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ChromosomeImpl extends CompleteNAObjectImpl implements Chromosome {
  public static final String TypeIRI = "http://gbol.life#Chromosome";

  protected ChromosomeImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Chromosome make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Chromosome.class);
      if(toRet == null) {
        toRet = new ChromosomeImpl(domain,iri);;
      }
      else if(!(toRet instanceof Chromosome)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ChromosomeImpl expected");
      }
      return (Chromosome)toRet;
    }
  }

  public void validate() {
  }
}
