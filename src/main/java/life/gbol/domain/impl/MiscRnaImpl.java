package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.MiscRna;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class MiscRnaImpl extends TranscriptImpl implements MiscRna {
  public static final String TypeIRI = "http://gbol.life#MiscRna";

  protected MiscRnaImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static MiscRna make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,MiscRna.class);
      if(toRet == null) {
        toRet = new MiscRnaImpl(domain,iri);;
      }
      else if(!(toRet instanceof MiscRna)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.MiscRnaImpl expected");
      }
      return (MiscRna)toRet;
    }
  }

  public void validate() {
  }
}
