package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.LinkSet;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class LinkSetImpl extends DatasetImpl implements LinkSet {
  public static final String TypeIRI = "http://gbol.life#LinkSet";

  protected LinkSetImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static LinkSet make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,LinkSet.class);
      if(toRet == null) {
        toRet = new LinkSetImpl(domain,iri);;
      }
      else if(!(toRet instanceof LinkSet)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.LinkSetImpl expected");
      }
      return (LinkSet)toRet;
    }
  }

  public void validate() {
  }
}
