package life.gbol.domain.impl;

import java.lang.String;
import java.time.LocalDate;
import life.gbol.domain.AnnotationActivity;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.w3.ns.prov.domain.impl.ActivityImpl;

/**
 * Code generated from http://gbol.life# ontology
 */
public class AnnotationActivityImpl extends ActivityImpl implements AnnotationActivity {
  public static final String TypeIRI = "http://gbol.life#AnnotationActivity";

  protected AnnotationActivityImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static AnnotationActivity make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,AnnotationActivity.class);
      if(toRet == null) {
        toRet = new AnnotationActivityImpl(domain,iri);;
      }
      else if(!(toRet instanceof AnnotationActivity)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.AnnotationActivityImpl expected");
      }
      return (AnnotationActivity)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://www.w3.org/ns/prov#startedAtTime");
    this.checkCardMin1("http://www.w3.org/ns/prov#endedAtTime");
  }

  public LocalDate getStartedAtTime() {
    return this.getDateLit("http://www.w3.org/ns/prov#startedAtTime",false);
  }

  public void setStartedAtTime(LocalDate val) {
    this.setDateLit("http://www.w3.org/ns/prov#startedAtTime",val);
  }

  public LocalDate getEndedAtTime() {
    return this.getDateLit("http://www.w3.org/ns/prov#endedAtTime",false);
  }

  public void setEndedAtTime(LocalDate val) {
    this.setDateLit("http://www.w3.org/ns/prov#endedAtTime",val);
  }
}
