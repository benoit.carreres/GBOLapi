package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.prim_transcript;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class prim_transcriptImpl extends TranscriptImpl implements prim_transcript {
  public static final String TypeIRI = "http://gbol.life#prim_transcript";

  protected prim_transcriptImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static prim_transcript make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,prim_transcript.class);
      if(toRet == null) {
        toRet = new prim_transcriptImpl(domain,iri);;
      }
      else if(!(toRet instanceof prim_transcript)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.prim_transcriptImpl expected");
      }
      return (prim_transcript)toRet;
    }
  }

  public void validate() {
  }
}
