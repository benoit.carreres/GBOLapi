package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.StemLoop;
import life.gbol.domain.StemLoopProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class StemLoopImpl extends StructureFeatureImpl implements StemLoop {
  public static final String TypeIRI = "http://gbol.life#StemLoop";

  protected StemLoopImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static StemLoop make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,StemLoop.class);
      if(toRet == null) {
        toRet = new StemLoopImpl(domain,iri);;
      }
      else if(!(toRet instanceof StemLoop)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.StemLoopImpl expected");
      }
      return (StemLoop)toRet;
    }
  }

  public void validate() {
  }

  public StemLoopProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,StemLoopProvenance.class);
  }

  public void setProvenance(StemLoopProvenance val) {
    this.setRef("http://gbol.life#provenance",val,StemLoopProvenance.class);
  }
}
