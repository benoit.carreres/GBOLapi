package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.LongTerminalRepeatProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class LongTerminalRepeatProvenanceImpl extends RepeatFeatureProvenanceImpl implements LongTerminalRepeatProvenance {
  public static final String TypeIRI = "http://gbol.life#LongTerminalRepeatProvenance";

  protected LongTerminalRepeatProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static LongTerminalRepeatProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,LongTerminalRepeatProvenance.class);
      if(toRet == null) {
        toRet = new LongTerminalRepeatProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof LongTerminalRepeatProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.LongTerminalRepeatProvenanceImpl expected");
      }
      return (LongTerminalRepeatProvenance)toRet;
    }
  }

  public void validate() {
  }
}
