package life.gbol.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.CollectionOfRegions;
import life.gbol.domain.Region;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class CollectionOfRegionsImpl extends LocationImpl implements CollectionOfRegions {
  public static final String TypeIRI = "http://gbol.life#CollectionOfRegions";

  protected CollectionOfRegionsImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static CollectionOfRegions make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,CollectionOfRegions.class);
      if(toRet == null) {
        toRet = new CollectionOfRegionsImpl(domain,iri);;
      }
      else if(!(toRet instanceof CollectionOfRegions)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.CollectionOfRegionsImpl expected");
      }
      return (CollectionOfRegions)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#members");
  }

  public void remMembers(Region val) {
    this.remRef("http://gbol.life#members",val,false);
  }

  public List<? extends Region> getAllMembers() {
    return this.getRefSet("http://gbol.life#members",false,Region.class);
  }

  public void addMembers(Region val) {
    this.addRef("http://gbol.life#members",val);
  }
}
