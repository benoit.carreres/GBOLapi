package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Database;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;

/**
 * Code generated from http://gbol.life# ontology
 */
public class DatabaseImpl extends OWLThingImpl implements Database {
  public static final String TypeIRI = "http://gbol.life#Database";

  protected DatabaseImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Database make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Database.class);
      if(toRet == null) {
        toRet = new DatabaseImpl(domain,iri);;
      }
      else if(!(toRet instanceof Database)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.DatabaseImpl expected");
      }
      return (Database)toRet;
    }
  }

  public void validate() {
  }

  public String getShortName() {
    return this.getStringLit("http://gbol.life#shortName",true);
  }

  public void setShortName(String val) {
    this.setStringLit("http://gbol.life#shortName",val);
  }

  public String getBase() {
    return this.getExternalRef("http://gbol.life#base",true);
  }

  public void setBase(String val) {
    this.setExternalRef("http://gbol.life#base",val);
  }

  public String getComment() {
    return this.getStringLit("http://www.w3.org/2000/01/rdf-schema#comment",true);
  }

  public void setComment(String val) {
    this.setStringLit("http://www.w3.org/2000/01/rdf-schema#comment",val);
  }
}
