package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Person;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;

/**
 * Code generated from http://gbol.life# ontology
 */
public class PersonImpl extends OWLThingImpl implements Person {
  public static final String TypeIRI = "http://gbol.life#Person";

  protected PersonImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Person make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Person.class);
      if(toRet == null) {
        toRet = new PersonImpl(domain,iri);;
      }
      else if(!(toRet instanceof Person)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.PersonImpl expected");
      }
      return (Person)toRet;
    }
  }

  public void validate() {
  }

  public String getId() {
    return this.getStringLit("http://gbol.life#id",true);
  }

  public void setId(String val) {
    this.setStringLit("http://gbol.life#id",val);
  }

  public String getSurName() {
    return this.getStringLit("http://gbol.life#surName",true);
  }

  public void setSurName(String val) {
    this.setStringLit("http://gbol.life#surName",val);
  }

  public String getFirstName() {
    return this.getStringLit("http://gbol.life#firstName",true);
  }

  public void setFirstName(String val) {
    this.setStringLit("http://gbol.life#firstName",val);
  }
}
