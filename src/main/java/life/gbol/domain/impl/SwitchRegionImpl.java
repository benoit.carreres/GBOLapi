package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.SwitchRegion;
import life.gbol.domain.SwitchRegionProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class SwitchRegionImpl extends ImmunoglobulinFeatureImpl implements SwitchRegion {
  public static final String TypeIRI = "http://gbol.life#SwitchRegion";

  protected SwitchRegionImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static SwitchRegion make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,SwitchRegion.class);
      if(toRet == null) {
        toRet = new SwitchRegionImpl(domain,iri);;
      }
      else if(!(toRet instanceof SwitchRegion)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.SwitchRegionImpl expected");
      }
      return (SwitchRegion)toRet;
    }
  }

  public void validate() {
  }

  public SwitchRegionProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,SwitchRegionProvenance.class);
  }

  public void setProvenance(SwitchRegionProvenance val) {
    this.setRef("http://gbol.life#provenance",val,SwitchRegionProvenance.class);
  }
}
