package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.PrimerBindingProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class PrimerBindingProvenanceImpl extends ArtificialRecognizedRegionProvenanceImpl implements PrimerBindingProvenance {
  public static final String TypeIRI = "http://gbol.life#PrimerBindingProvenance";

  protected PrimerBindingProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static PrimerBindingProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,PrimerBindingProvenance.class);
      if(toRet == null) {
        toRet = new PrimerBindingProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof PrimerBindingProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.PrimerBindingProvenanceImpl expected");
      }
      return (PrimerBindingProvenance)toRet;
    }
  }

  public void validate() {
  }
}
