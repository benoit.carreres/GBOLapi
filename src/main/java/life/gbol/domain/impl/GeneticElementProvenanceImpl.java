package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.GeneticElementProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class GeneticElementProvenanceImpl extends GeneRelatedElementsProvenanceImpl implements GeneticElementProvenance {
  public static final String TypeIRI = "http://gbol.life#GeneticElementProvenance";

  protected GeneticElementProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static GeneticElementProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,GeneticElementProvenance.class);
      if(toRet == null) {
        toRet = new GeneticElementProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof GeneticElementProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.GeneticElementProvenanceImpl expected");
      }
      return (GeneticElementProvenance)toRet;
    }
  }

  public void validate() {
  }
}
