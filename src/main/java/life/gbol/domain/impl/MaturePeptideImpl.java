package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.MaturePeptide;
import life.gbol.domain.MaturePeptideProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class MaturePeptideImpl extends ProteinFeatureImpl implements MaturePeptide {
  public static final String TypeIRI = "http://gbol.life#MaturePeptide";

  protected MaturePeptideImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static MaturePeptide make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,MaturePeptide.class);
      if(toRet == null) {
        toRet = new MaturePeptideImpl(domain,iri);;
      }
      else if(!(toRet instanceof MaturePeptide)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.MaturePeptideImpl expected");
      }
      return (MaturePeptide)toRet;
    }
  }

  public void validate() {
  }

  public MaturePeptideProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,MaturePeptideProvenance.class);
  }

  public void setProvenance(MaturePeptideProvenance val) {
    this.setRef("http://gbol.life#provenance",val,MaturePeptideProvenance.class);
  }
}
