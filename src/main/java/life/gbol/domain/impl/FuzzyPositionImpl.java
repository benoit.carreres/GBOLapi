package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.FuzzyPosition;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class FuzzyPositionImpl extends PositionImpl implements FuzzyPosition {
  public static final String TypeIRI = "http://gbol.life#FuzzyPosition";

  protected FuzzyPositionImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static FuzzyPosition make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,FuzzyPosition.class);
      if(toRet == null) {
        toRet = new FuzzyPositionImpl(domain,iri);;
      }
      else if(!(toRet instanceof FuzzyPosition)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.FuzzyPositionImpl expected");
      }
      return (FuzzyPosition)toRet;
    }
  }

  public void validate() {
  }
}
