package life.gbol.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.ListOfRegions;
import life.gbol.domain.Region;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ListOfRegionsImpl extends CollectionOfRegionsImpl implements ListOfRegions {
  public static final String TypeIRI = "http://gbol.life#ListOfRegions";

  protected ListOfRegionsImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static ListOfRegions make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,ListOfRegions.class);
      if(toRet == null) {
        toRet = new ListOfRegionsImpl(domain,iri);;
      }
      else if(!(toRet instanceof ListOfRegions)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ListOfRegionsImpl expected");
      }
      return (ListOfRegions)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#members");
  }

  public Region getMembers(int index) {
    return this.getRefListAtIndex("http://gbol.life#members",false,Region.class,index);
  }

  public List<? extends Region> getAllMembers() {
    return this.getRefList("http://gbol.life#members",false,Region.class);
  }

  public void addMembers(Region val) {
    this.addRefList("http://gbol.life#members",val);
  }

  public void setMembers(Region val, int index) {
    this.setRefList("http://gbol.life#members",val,false,index);
  }

  public void remMembers(Region val) {
    this.remRefList("http://gbol.life#members",val,false);
  }
}
