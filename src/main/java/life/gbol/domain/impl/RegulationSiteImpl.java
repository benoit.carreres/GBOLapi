package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.RegulationSite;
import life.gbol.domain.RegulationSiteProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class RegulationSiteImpl extends BiologicalRecognizedRegionImpl implements RegulationSite {
  public static final String TypeIRI = "http://gbol.life#RegulationSite";

  protected RegulationSiteImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static RegulationSite make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,RegulationSite.class);
      if(toRet == null) {
        toRet = new RegulationSiteImpl(domain,iri);;
      }
      else if(!(toRet instanceof RegulationSite)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.RegulationSiteImpl expected");
      }
      return (RegulationSite)toRet;
    }
  }

  public void validate() {
  }

  public String getRegulatory_class() {
    return this.getStringLit("http://gbol.life#regulatory_class",true);
  }

  public void setRegulatory_class(String val) {
    this.setStringLit("http://gbol.life#regulatory_class",val);
  }

  public RegulationSiteProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,RegulationSiteProvenance.class);
  }

  public void setProvenance(RegulationSiteProvenance val) {
    this.setRef("http://gbol.life#provenance",val,RegulationSiteProvenance.class);
  }
}
