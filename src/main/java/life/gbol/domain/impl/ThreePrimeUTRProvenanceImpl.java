package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.ThreePrimeUTRProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ThreePrimeUTRProvenanceImpl extends TranscriptFeatureProvenanceImpl implements ThreePrimeUTRProvenance {
  public static final String TypeIRI = "http://gbol.life#ThreePrimeUTRProvenance";

  protected ThreePrimeUTRProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static ThreePrimeUTRProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,ThreePrimeUTRProvenance.class);
      if(toRet == null) {
        toRet = new ThreePrimeUTRProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof ThreePrimeUTRProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ThreePrimeUTRProvenanceImpl expected");
      }
      return (ThreePrimeUTRProvenance)toRet;
    }
  }

  public void validate() {
  }
}
