package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.TranscribedElement;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class TranscribedElementImpl extends NAObjectImpl implements TranscribedElement {
  public static final String TypeIRI = "http://gbol.life#TranscribedElement";

  protected TranscribedElementImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static TranscribedElement make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,TranscribedElement.class);
      if(toRet == null) {
        toRet = new TranscribedElementImpl(domain,iri);;
      }
      else if(!(toRet instanceof TranscribedElement)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.TranscribedElementImpl expected");
      }
      return (TranscribedElement)toRet;
    }
  }

  public void validate() {
  }
}
