package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.AnnotationSoftware;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.w3.ns.prov.domain.impl.SoftwareAgentImpl;

/**
 * Code generated from http://gbol.life# ontology
 */
public class AnnotationSoftwareImpl extends SoftwareAgentImpl implements AnnotationSoftware {
  public static final String TypeIRI = "http://gbol.life#AnnotationSoftware";

  protected AnnotationSoftwareImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static AnnotationSoftware make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,AnnotationSoftware.class);
      if(toRet == null) {
        toRet = new AnnotationSoftwareImpl(domain,iri);;
      }
      else if(!(toRet instanceof AnnotationSoftware)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.AnnotationSoftwareImpl expected");
      }
      return (AnnotationSoftware)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#name");
    this.checkCardMin1("http://gbol.life#version");
  }

  public String getName() {
    return this.getStringLit("http://gbol.life#name",false);
  }

  public void setName(String val) {
    this.setStringLit("http://gbol.life#name",val);
  }

  public String getVersion() {
    return this.getStringLit("http://gbol.life#version",false);
  }

  public void setVersion(String val) {
    this.setStringLit("http://gbol.life#version",val);
  }
}
