package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.MiscBinding;
import life.gbol.domain.MiscBindingProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class MiscBindingImpl extends BiologicalRecognizedRegionImpl implements MiscBinding {
  public static final String TypeIRI = "http://gbol.life#MiscBinding";

  protected MiscBindingImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static MiscBinding make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,MiscBinding.class);
      if(toRet == null) {
        toRet = new MiscBindingImpl(domain,iri);;
      }
      else if(!(toRet instanceof MiscBinding)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.MiscBindingImpl expected");
      }
      return (MiscBinding)toRet;
    }
  }

  public void validate() {
  }

  public MiscBindingProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,MiscBindingProvenance.class);
  }

  public void setProvenance(MiscBindingProvenance val) {
    this.setRef("http://gbol.life#provenance",val,MiscBindingProvenance.class);
  }
}
