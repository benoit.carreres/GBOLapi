package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Prodigal;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ProdigalImpl extends GeneProvenanceImpl implements Prodigal {
  public static final String TypeIRI = "http://gbol.life#Prodigal";

  protected ProdigalImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Prodigal make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Prodigal.class);
      if(toRet == null) {
        toRet = new ProdigalImpl(domain,iri);;
      }
      else if(!(toRet instanceof Prodigal)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ProdigalImpl expected");
      }
      return (Prodigal)toRet;
    }
  }

  public void validate() {
  }

  public String getRbs_spacer() {
    return this.getStringLit("http://gbol.life#rbs_spacer",true);
  }

  public void setRbs_spacer(String val) {
    this.setStringLit("http://gbol.life#rbs_spacer",val);
  }

  public String getRbs_motif() {
    return this.getStringLit("http://gbol.life#rbs_motif",true);
  }

  public void setRbs_motif(String val) {
    this.setStringLit("http://gbol.life#rbs_motif",val);
  }

  public String getStart_type() {
    return this.getStringLit("http://gbol.life#start_type",true);
  }

  public void setStart_type(String val) {
    this.setStringLit("http://gbol.life#start_type",val);
  }

  public String getGc_cont() {
    return this.getStringLit("http://gbol.life#gc_cont",true);
  }

  public void setGc_cont(String val) {
    this.setStringLit("http://gbol.life#gc_cont",val);
  }
}
