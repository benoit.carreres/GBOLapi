package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.MaturedRNA;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class MaturedRNAImpl extends TranscriptImpl implements MaturedRNA {
  public static final String TypeIRI = "http://gbol.life#MaturedRNA";

  protected MaturedRNAImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static MaturedRNA make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,MaturedRNA.class);
      if(toRet == null) {
        toRet = new MaturedRNAImpl(domain,iri);;
      }
      else if(!(toRet instanceof MaturedRNA)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.MaturedRNAImpl expected");
      }
      return (MaturedRNA)toRet;
    }
  }

  public void validate() {
  }
}
