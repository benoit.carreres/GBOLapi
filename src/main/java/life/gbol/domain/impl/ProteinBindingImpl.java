package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.ProteinBinding;
import life.gbol.domain.ProteinBindingProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ProteinBindingImpl extends BiologicalRecognizedRegionImpl implements ProteinBinding {
  public static final String TypeIRI = "http://gbol.life#ProteinBinding";

  protected ProteinBindingImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static ProteinBinding make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,ProteinBinding.class);
      if(toRet == null) {
        toRet = new ProteinBindingImpl(domain,iri);;
      }
      else if(!(toRet instanceof ProteinBinding)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ProteinBindingImpl expected");
      }
      return (ProteinBinding)toRet;
    }
  }

  public void validate() {
  }

  public ProteinBindingProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,ProteinBindingProvenance.class);
  }

  public void setProvenance(ProteinBindingProvenance val) {
    this.setRef("http://gbol.life#provenance",val,ProteinBindingProvenance.class);
  }
}
