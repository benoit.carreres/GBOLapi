package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Organisation;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.w3.ns.prov.domain.impl.OrganizationImpl;

/**
 * Code generated from http://gbol.life# ontology
 */
public class OrganisationImpl extends OrganizationImpl implements Organisation {
  public static final String TypeIRI = "http://gbol.life#Organisation";

  protected OrganisationImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Organisation make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Organisation.class);
      if(toRet == null) {
        toRet = new OrganisationImpl(domain,iri);;
      }
      else if(!(toRet instanceof Organisation)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.OrganisationImpl expected");
      }
      return (Organisation)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#legalName");
  }

  public String getLegalName() {
    return this.getStringLit("http://gbol.life#legalName",false);
  }

  public void setLegalName(String val) {
    this.setStringLit("http://gbol.life#legalName",val);
  }
}
