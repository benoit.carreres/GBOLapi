package life.gbol.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.CRISPRCassette;
import life.gbol.domain.Repeat;
import life.gbol.domain.Spacer;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class CRISPRCassetteImpl extends GeneImpl implements CRISPRCassette {
  public static final String TypeIRI = "http://gbol.life#CRISPRCassette";

  protected CRISPRCassetteImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static CRISPRCassette make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,CRISPRCassette.class);
      if(toRet == null) {
        toRet = new CRISPRCassetteImpl(domain,iri);;
      }
      else if(!(toRet instanceof CRISPRCassette)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.CRISPRCassetteImpl expected");
      }
      return (CRISPRCassette)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#spacer");
    this.checkCardMin1("http://gbol.life#repeat");
  }

  public void remSpacer(Spacer val) {
    this.remRef("http://gbol.life#spacer",val,false);
  }

  public List<? extends Spacer> getAllSpacer() {
    return this.getRefSet("http://gbol.life#spacer",false,Spacer.class);
  }

  public void addSpacer(Spacer val) {
    this.addRef("http://gbol.life#spacer",val);
  }

  public void remRepeat(Repeat val) {
    this.remRef("http://gbol.life#repeat",val,false);
  }

  public List<? extends Repeat> getAllRepeat() {
    return this.getRefSet("http://gbol.life#repeat",false,Repeat.class);
  }

  public void addRepeat(Repeat val) {
    this.addRef("http://gbol.life#repeat",val);
  }
}
