package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.SwitchRegionProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class SwitchRegionProvenanceImpl extends ImmunoglobulinFeatureProvenanceImpl implements SwitchRegionProvenance {
  public static final String TypeIRI = "http://gbol.life#SwitchRegionProvenance";

  protected SwitchRegionProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static SwitchRegionProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,SwitchRegionProvenance.class);
      if(toRet == null) {
        toRet = new SwitchRegionProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof SwitchRegionProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.SwitchRegionProvenanceImpl expected");
      }
      return (SwitchRegionProvenance)toRet;
    }
  }

  public void validate() {
  }
}
