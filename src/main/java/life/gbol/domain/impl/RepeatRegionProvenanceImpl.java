package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.RepeatRegionProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class RepeatRegionProvenanceImpl extends RepeatFeatureProvenanceImpl implements RepeatRegionProvenance {
  public static final String TypeIRI = "http://gbol.life#RepeatRegionProvenance";

  protected RepeatRegionProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static RepeatRegionProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,RepeatRegionProvenance.class);
      if(toRet == null) {
        toRet = new RepeatRegionProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof RepeatRegionProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.RepeatRegionProvenanceImpl expected");
      }
      return (RepeatRegionProvenance)toRet;
    }
  }

  public void validate() {
  }
}
