package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.NonGeneRelatedElementsProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class NonGeneRelatedElementsProvenanceImpl extends NAFeatureProvenanceImpl implements NonGeneRelatedElementsProvenance {
  public static final String TypeIRI = "http://gbol.life#NonGeneRelatedElementsProvenance";

  protected NonGeneRelatedElementsProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static NonGeneRelatedElementsProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,NonGeneRelatedElementsProvenance.class);
      if(toRet == null) {
        toRet = new NonGeneRelatedElementsProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof NonGeneRelatedElementsProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.NonGeneRelatedElementsProvenanceImpl expected");
      }
      return (NonGeneRelatedElementsProvenance)toRet;
    }
  }

  public void validate() {
  }
}
