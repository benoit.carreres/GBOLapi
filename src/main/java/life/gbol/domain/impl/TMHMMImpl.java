package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Location;
import life.gbol.domain.TMHMM;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class TMHMMImpl extends ProteinFeatureProvenanceImpl implements TMHMM {
  public static final String TypeIRI = "http://gbol.life#TMHMM";

  protected TMHMMImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static TMHMM make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,TMHMM.class);
      if(toRet == null) {
        toRet = new TMHMMImpl(domain,iri);;
      }
      else if(!(toRet instanceof TMHMM)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.TMHMMImpl expected");
      }
      return (TMHMM)toRet;
    }
  }

  public void validate() {
  }

  public String getSequence() {
    return this.getStringLit("http://gbol.life#sequence",true);
  }

  public void setSequence(String val) {
    this.setStringLit("http://gbol.life#sequence",val);
  }

  public String getType() {
    return this.getStringLit("http://gbol.life#type",true);
  }

  public void setType(String val) {
    this.setStringLit("http://gbol.life#type",val);
  }

  public Location getLocation() {
    return this.getRef("http://gbol.life#location",true,Location.class);
  }

  public void setLocation(Location val) {
    this.setRef("http://gbol.life#location",val,Location.class);
  }
}
