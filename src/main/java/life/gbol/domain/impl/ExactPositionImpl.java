package life.gbol.domain.impl;

import java.lang.Integer;
import java.lang.String;
import life.gbol.domain.ExactPosition;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ExactPositionImpl extends PositionImpl implements ExactPosition {
  public static final String TypeIRI = "http://gbol.life#ExactPosition";

  protected ExactPositionImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static ExactPosition make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,ExactPosition.class);
      if(toRet == null) {
        toRet = new ExactPositionImpl(domain,iri);;
      }
      else if(!(toRet instanceof ExactPosition)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ExactPositionImpl expected");
      }
      return (ExactPosition)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#position");
  }

  public Integer getPosition() {
    return this.getIntegerLit("http://gbol.life#position",false);
  }

  public void setPosition(Integer val) {
    this.setIntegerLit("http://gbol.life#position",val);
  }
}
