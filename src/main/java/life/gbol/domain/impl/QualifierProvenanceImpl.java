package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.QualifierProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class QualifierProvenanceImpl extends ProvenanceImpl implements QualifierProvenance {
  public static final String TypeIRI = "http://gbol.life#QualifierProvenance";

  protected QualifierProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static QualifierProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,QualifierProvenance.class);
      if(toRet == null) {
        toRet = new QualifierProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof QualifierProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.QualifierProvenanceImpl expected");
      }
      return (QualifierProvenance)toRet;
    }
  }

  public void validate() {
  }
}
