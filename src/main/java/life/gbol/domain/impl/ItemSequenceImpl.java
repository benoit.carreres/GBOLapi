package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.ItemSequence;
import life.gbol.domain.UncompleteNAObject;
import life.gbol.domain.XRef;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ItemSequenceImpl extends SequenceConstructionItemImpl implements ItemSequence {
  public static final String TypeIRI = "http://gbol.life#ItemSequence";

  protected ItemSequenceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static ItemSequence make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,ItemSequence.class);
      if(toRet == null) {
        toRet = new ItemSequenceImpl(domain,iri);;
      }
      else if(!(toRet instanceof ItemSequence)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ItemSequenceImpl expected");
      }
      return (ItemSequence)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#sequence");
  }

  public UncompleteNAObject getSequence() {
    return this.getRef("http://gbol.life#sequence",false,UncompleteNAObject.class);
  }

  public void setSequence(UncompleteNAObject val) {
    this.setRef("http://gbol.life#sequence",val,UncompleteNAObject.class);
  }

  public XRef getFromXRef() {
    return this.getRef("http://gbol.life#fromXRef",true,XRef.class);
  }

  public void setFromXRef(XRef val) {
    this.setRef("http://gbol.life#fromXRef",val,XRef.class);
  }
}
