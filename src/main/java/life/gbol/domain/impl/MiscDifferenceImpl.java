package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.MiscDifference;
import life.gbol.domain.MiscDifferenceProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class MiscDifferenceImpl extends ChangeFeatureImpl implements MiscDifference {
  public static final String TypeIRI = "http://gbol.life#MiscDifference";

  protected MiscDifferenceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static MiscDifference make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,MiscDifference.class);
      if(toRet == null) {
        toRet = new MiscDifferenceImpl(domain,iri);;
      }
      else if(!(toRet instanceof MiscDifference)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.MiscDifferenceImpl expected");
      }
      return (MiscDifference)toRet;
    }
  }

  public void validate() {
  }

  public String getReplace() {
    return this.getStringLit("http://gbol.life#replace",true);
  }

  public void setReplace(String val) {
    this.setStringLit("http://gbol.life#replace",val);
  }

  public String getCompare() {
    return this.getStringLit("http://gbol.life#compare",true);
  }

  public void setCompare(String val) {
    this.setStringLit("http://gbol.life#compare",val);
  }

  public MiscDifferenceProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,MiscDifferenceProvenance.class);
  }

  public void setProvenance(MiscDifferenceProvenance val) {
    this.setRef("http://gbol.life#provenance",val,MiscDifferenceProvenance.class);
  }
}
