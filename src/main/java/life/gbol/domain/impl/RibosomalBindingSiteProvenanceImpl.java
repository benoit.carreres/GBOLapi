package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.RibosomalBindingSiteProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class RibosomalBindingSiteProvenanceImpl extends RegulationSiteProvenanceImpl implements RibosomalBindingSiteProvenance {
  public static final String TypeIRI = "http://gbol.life#RibosomalBindingSiteProvenance";

  protected RibosomalBindingSiteProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static RibosomalBindingSiteProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,RibosomalBindingSiteProvenance.class);
      if(toRet == null) {
        toRet = new RibosomalBindingSiteProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof RibosomalBindingSiteProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.RibosomalBindingSiteProvenanceImpl expected");
      }
      return (RibosomalBindingSiteProvenance)toRet;
    }
  }

  public void validate() {
  }
}
