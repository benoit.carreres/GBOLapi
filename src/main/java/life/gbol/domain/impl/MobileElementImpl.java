package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.MobileElement;
import life.gbol.domain.MobileElementProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class MobileElementImpl extends RepeatFeatureImpl implements MobileElement {
  public static final String TypeIRI = "http://gbol.life#MobileElement";

  protected MobileElementImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static MobileElement make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,MobileElement.class);
      if(toRet == null) {
        toRet = new MobileElementImpl(domain,iri);;
      }
      else if(!(toRet instanceof MobileElement)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.MobileElementImpl expected");
      }
      return (MobileElement)toRet;
    }
  }

  public void validate() {
  }

  public MobileElementProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,MobileElementProvenance.class);
  }

  public void setProvenance(MobileElementProvenance val) {
    this.setRef("http://gbol.life#provenance",val,MobileElementProvenance.class);
  }

  public String getMobile_element_type() {
    return this.getStringLit("http://gbol.life#mobile_element_type",true);
  }

  public void setMobile_element_type(String val) {
    this.setStringLit("http://gbol.life#mobile_element_type",val);
  }
}
