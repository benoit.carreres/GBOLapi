package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.InterveningDNAProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class InterveningDNAProvenanceImpl extends GeneticElementProvenanceImpl implements InterveningDNAProvenance {
  public static final String TypeIRI = "http://gbol.life#InterveningDNAProvenance";

  protected InterveningDNAProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static InterveningDNAProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,InterveningDNAProvenance.class);
      if(toRet == null) {
        toRet = new InterveningDNAProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof InterveningDNAProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.InterveningDNAProvenanceImpl expected");
      }
      return (InterveningDNAProvenance)toRet;
    }
  }

  public void validate() {
  }
}
