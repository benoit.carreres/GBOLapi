package life.gbol.domain.impl;

import java.lang.Boolean;
import java.lang.Integer;
import java.lang.String;
import life.gbol.domain.CDS;
import life.gbol.domain.CDSProvenance;
import life.gbol.domain.Protein;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class CDSImpl extends TranscriptFeatureImpl implements CDS {
  public static final String TypeIRI = "http://gbol.life#CDS";

  protected CDSImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static CDS make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,CDS.class);
      if(toRet == null) {
        toRet = new CDSImpl(domain,iri);;
      }
      else if(!(toRet instanceof CDS)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.CDSImpl expected");
      }
      return (CDS)toRet;
    }
  }

  public void validate() {
  }

  public String getTransl_except() {
    return this.getStringLit("http://gbol.life#transl_except",true);
  }

  public void setTransl_except(String val) {
    this.setStringLit("http://gbol.life#transl_except",val);
  }

  public Boolean getRibosomal_slippage() {
    return this.getBooleanLit("http://gbol.life#ribosomal_slippage",true);
  }

  public void setRibosomal_slippage(Boolean val) {
    this.setBooleanLit("http://gbol.life#ribosomal_slippage",val);
  }

  public Integer getCodon_start() {
    return this.getIntegerLit("http://gbol.life#codon_start",true);
  }

  public void setCodon_start(Integer val) {
    this.setIntegerLit("http://gbol.life#codon_start",val);
  }

  public String getArtificial_location() {
    return this.getStringLit("http://gbol.life#artificial_location",true);
  }

  public void setArtificial_location(String val) {
    this.setStringLit("http://gbol.life#artificial_location",val);
  }

  public String getProtein_id() {
    return this.getStringLit("http://gbol.life#protein_id",true);
  }

  public void setProtein_id(String val) {
    this.setStringLit("http://gbol.life#protein_id",val);
  }

  public String getException() {
    return this.getStringLit("http://gbol.life#exception",true);
  }

  public void setException(String val) {
    this.setStringLit("http://gbol.life#exception",val);
  }

  public Protein getProtein() {
    return this.getRef("http://gbol.life#protein",true,Protein.class);
  }

  public void setProtein(Protein val) {
    this.setRef("http://gbol.life#protein",val,Protein.class);
  }

  public String getGene() {
    return this.getStringLit("http://gbol.life#gene",true);
  }

  public void setGene(String val) {
    this.setStringLit("http://gbol.life#gene",val);
  }

  public Integer getTransl_table() {
    return this.getIntegerLit("http://gbol.life#transl_table",true);
  }

  public void setTransl_table(Integer val) {
    this.setIntegerLit("http://gbol.life#transl_table",val);
  }

  public CDSProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,CDSProvenance.class);
  }

  public void setProvenance(CDSProvenance val) {
    this.setRef("http://gbol.life#provenance",val,CDSProvenance.class);
  }
}
