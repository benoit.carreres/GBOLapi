package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.AnnotationResult;
import life.gbol.domain.NoteProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class NoteProvenanceImpl extends QualifierProvenanceImpl implements NoteProvenance {
  public static final String TypeIRI = "http://gbol.life#NoteProvenance";

  protected NoteProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static NoteProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,NoteProvenance.class);
      if(toRet == null) {
        toRet = new NoteProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof NoteProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.NoteProvenanceImpl expected");
      }
      return (NoteProvenance)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#origin");
  }

  public AnnotationResult getOrigin() {
    return this.getRef("http://gbol.life#origin",false,AnnotationResult.class);
  }

  public void setOrigin(AnnotationResult val) {
    this.setRef("http://gbol.life#origin",val,AnnotationResult.class);
  }
}
