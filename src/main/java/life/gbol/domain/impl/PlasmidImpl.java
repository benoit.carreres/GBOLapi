package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Plasmid;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class PlasmidImpl extends CompleteNAObjectImpl implements Plasmid {
  public static final String TypeIRI = "http://gbol.life#Plasmid";

  protected PlasmidImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Plasmid make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Plasmid.class);
      if(toRet == null) {
        toRet = new PlasmidImpl(domain,iri);;
      }
      else if(!(toRet instanceof Plasmid)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.PlasmidImpl expected");
      }
      return (Plasmid)toRet;
    }
  }

  public void validate() {
  }
}
