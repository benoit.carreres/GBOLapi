package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.DLoop;
import life.gbol.domain.DLoopProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class DLoopImpl extends BiologicalRecognizedRegionImpl implements DLoop {
  public static final String TypeIRI = "http://gbol.life#DLoop";

  protected DLoopImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static DLoop make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,DLoop.class);
      if(toRet == null) {
        toRet = new DLoopImpl(domain,iri);;
      }
      else if(!(toRet instanceof DLoop)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.DLoopImpl expected");
      }
      return (DLoop)toRet;
    }
  }

  public void validate() {
  }

  public DLoopProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,DLoopProvenance.class);
  }

  public void setProvenance(DLoopProvenance val) {
    this.setRef("http://gbol.life#provenance",val,DLoopProvenance.class);
  }
}
