package life.gbol.domain.impl;

import java.lang.Boolean;
import java.lang.Integer;
import java.lang.String;
import life.gbol.domain.NAFeature;
import life.gbol.domain.NAFeatureProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class NAFeatureImpl extends FeatureImpl implements NAFeature {
  public static final String TypeIRI = "http://gbol.life#NAFeature";

  protected NAFeatureImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static NAFeature make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,NAFeature.class);
      if(toRet == null) {
        toRet = new NAFeatureImpl(domain,iri);;
      }
      else if(!(toRet instanceof NAFeature)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.NAFeatureImpl expected");
      }
      return (NAFeature)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#locus_tag");
  }

  public String getOnt() {
    return this.getStringLit("http://gbol.life#ont",true);
  }

  public void setOnt(String val) {
    this.setStringLit("http://gbol.life#ont",val);
  }

  public String getGene_type() {
    return this.getStringLit("http://gbol.life#gene_type",true);
  }

  public void setGene_type(String val) {
    this.setStringLit("http://gbol.life#gene_type",val);
  }

  public String getTranscript_name() {
    return this.getStringLit("http://gbol.life#transcript_name",true);
  }

  public void setTranscript_name(String val) {
    this.setStringLit("http://gbol.life#transcript_name",val);
  }

  public String getTag() {
    return this.getStringLit("http://gbol.life#tag",true);
  }

  public void setTag(String val) {
    this.setStringLit("http://gbol.life#tag",val);
  }

  public String getExon_number() {
    return this.getStringLit("http://gbol.life#exon_number",true);
  }

  public void setExon_number(String val) {
    this.setStringLit("http://gbol.life#exon_number",val);
  }

  public Integer getLevel() {
    return this.getIntegerLit("http://gbol.life#level",true);
  }

  public void setLevel(Integer val) {
    this.setIntegerLit("http://gbol.life#level",val);
  }

  public String getHavana_transcript() {
    return this.getStringLit("http://gbol.life#havana_transcript",true);
  }

  public void setHavana_transcript(String val) {
    this.setStringLit("http://gbol.life#havana_transcript",val);
  }

  public String getLocus_tag() {
    return this.getStringLit("http://gbol.life#locus_tag",false);
  }

  public void setLocus_tag(String val) {
    this.setStringLit("http://gbol.life#locus_tag",val);
  }

  public String getGene_status() {
    return this.getStringLit("http://gbol.life#gene_status",true);
  }

  public void setGene_status(String val) {
    this.setStringLit("http://gbol.life#gene_status",val);
  }

  public String getGene() {
    return this.getStringLit("http://gbol.life#gene",true);
  }

  public void setGene(String val) {
    this.setStringLit("http://gbol.life#gene",val);
  }

  public String getProduct() {
    return this.getStringLit("http://gbol.life#product",true);
  }

  public void setProduct(String val) {
    this.setStringLit("http://gbol.life#product",val);
  }

  public Boolean getPseudo() {
    return this.getBooleanLit("http://gbol.life#pseudo",true);
  }

  public void setPseudo(Boolean val) {
    this.setBooleanLit("http://gbol.life#pseudo",val);
  }

  public Boolean getPartial() {
    return this.getBooleanLit("http://gbol.life#partial",true);
  }

  public void setPartial(Boolean val) {
    this.setBooleanLit("http://gbol.life#partial",val);
  }

  public String getGene_id() {
    return this.getStringLit("http://gbol.life#gene_id",true);
  }

  public void setGene_id(String val) {
    this.setStringLit("http://gbol.life#gene_id",val);
  }

  public String getGene_name() {
    return this.getStringLit("http://gbol.life#gene_name",true);
  }

  public void setGene_name(String val) {
    this.setStringLit("http://gbol.life#gene_name",val);
  }

  public String getOld_locus_tag() {
    return this.getStringLit("http://gbol.life#old_locus_tag",true);
  }

  public void setOld_locus_tag(String val) {
    this.setStringLit("http://gbol.life#old_locus_tag",val);
  }

  public String getGene_synonym() {
    return this.getStringLit("http://gbol.life#gene_synonym",true);
  }

  public void setGene_synonym(String val) {
    this.setStringLit("http://gbol.life#gene_synonym",val);
  }

  public String getHavana_gene() {
    return this.getStringLit("http://gbol.life#havana_gene",true);
  }

  public void setHavana_gene(String val) {
    this.setStringLit("http://gbol.life#havana_gene",val);
  }

  public String getTranscript_type() {
    return this.getStringLit("http://gbol.life#transcript_type",true);
  }

  public void setTranscript_type(String val) {
    this.setStringLit("http://gbol.life#transcript_type",val);
  }

  public String getTranscript_id() {
    return this.getStringLit("http://gbol.life#transcript_id",true);
  }

  public void setTranscript_id(String val) {
    this.setStringLit("http://gbol.life#transcript_id",val);
  }

  public String getTranscript_status() {
    return this.getStringLit("http://gbol.life#transcript_status",true);
  }

  public void setTranscript_status(String val) {
    this.setStringLit("http://gbol.life#transcript_status",val);
  }

  public String getExon_id() {
    return this.getStringLit("http://gbol.life#exon_id",true);
  }

  public void setExon_id(String val) {
    this.setStringLit("http://gbol.life#exon_id",val);
  }

  public NAFeatureProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,NAFeatureProvenance.class);
  }

  public void setProvenance(NAFeatureProvenance val) {
    this.setRef("http://gbol.life#provenance",val,NAFeatureProvenance.class);
  }
}
