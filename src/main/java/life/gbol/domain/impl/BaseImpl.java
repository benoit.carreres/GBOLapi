package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Base;
import life.gbol.domain.Position;
import life.gbol.domain.StrandType;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class BaseImpl extends LocationImpl implements Base {
  public static final String TypeIRI = "http://gbol.life#Base";

  protected BaseImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Base make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Base.class);
      if(toRet == null) {
        toRet = new BaseImpl(domain,iri);;
      }
      else if(!(toRet instanceof Base)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.BaseImpl expected");
      }
      return (Base)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#position");
  }

  public StrandType getStrand() {
    return this.getEnum("http://gbol.life#strand",true,StrandType.class);
  }

  public void setStrand(StrandType val) {
    this.setEnum("http://gbol.life#strand",val,StrandType.class);
  }

  public Position getPosition() {
    return this.getRef("http://gbol.life#position",false,Position.class);
  }

  public void setPosition(Position val) {
    this.setRef("http://gbol.life#position",val,Position.class);
  }
}
