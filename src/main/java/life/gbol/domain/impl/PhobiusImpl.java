package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Location;
import life.gbol.domain.Phobius;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class PhobiusImpl extends ProteinFeatureProvenanceImpl implements Phobius {
  public static final String TypeIRI = "http://gbol.life#Phobius";

  protected PhobiusImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Phobius make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Phobius.class);
      if(toRet == null) {
        toRet = new PhobiusImpl(domain,iri);;
      }
      else if(!(toRet instanceof Phobius)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.PhobiusImpl expected");
      }
      return (Phobius)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#type");
    this.checkCardMin1("http://gbol.life#description");
    this.checkCardMin1("http://gbol.life#location");
  }

  public String getType() {
    return this.getStringLit("http://gbol.life#type",false);
  }

  public void setType(String val) {
    this.setStringLit("http://gbol.life#type",val);
  }

  public String getDescription() {
    return this.getStringLit("http://gbol.life#description",false);
  }

  public void setDescription(String val) {
    this.setStringLit("http://gbol.life#description",val);
  }

  public Location getLocation() {
    return this.getRef("http://gbol.life#location",false,Location.class);
  }

  public void setLocation(Location val) {
    this.setRef("http://gbol.life#location",val,Location.class);
  }
}
