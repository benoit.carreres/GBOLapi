package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.RecognizedRegion;
import life.gbol.domain.RecognizedRegionProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class RecognizedRegionImpl extends NonGeneRelatedElementsImpl implements RecognizedRegion {
  public static final String TypeIRI = "http://gbol.life#RecognizedRegion";

  protected RecognizedRegionImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static RecognizedRegion make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,RecognizedRegion.class);
      if(toRet == null) {
        toRet = new RecognizedRegionImpl(domain,iri);;
      }
      else if(!(toRet instanceof RecognizedRegion)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.RecognizedRegionImpl expected");
      }
      return (RecognizedRegion)toRet;
    }
  }

  public void validate() {
  }

  public RecognizedRegionProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,RecognizedRegionProvenance.class);
  }

  public void setProvenance(RecognizedRegionProvenance val) {
    this.setRef("http://gbol.life#provenance",val,RecognizedRegionProvenance.class);
  }
}
