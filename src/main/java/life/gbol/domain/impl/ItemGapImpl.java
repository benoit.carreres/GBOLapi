package life.gbol.domain.impl;

import java.lang.Integer;
import java.lang.String;
import life.gbol.domain.ItemGap;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ItemGapImpl extends SequenceConstructionItemImpl implements ItemGap {
  public static final String TypeIRI = "http://gbol.life#ItemGap";

  protected ItemGapImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static ItemGap make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,ItemGap.class);
      if(toRet == null) {
        toRet = new ItemGapImpl(domain,iri);;
      }
      else if(!(toRet instanceof ItemGap)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ItemGapImpl expected");
      }
      return (ItemGap)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#length");
  }

  public Integer getLength() {
    return this.getIntegerLit("http://gbol.life#length",false);
  }

  public void setLength(Integer val) {
    this.setIntegerLit("http://gbol.life#length",val);
  }
}
