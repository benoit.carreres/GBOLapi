package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.OperonProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class OperonProvenanceImpl extends GeneticElementProvenanceImpl implements OperonProvenance {
  public static final String TypeIRI = "http://gbol.life#OperonProvenance";

  protected OperonProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static OperonProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,OperonProvenance.class);
      if(toRet == null) {
        toRet = new OperonProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof OperonProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.OperonProvenanceImpl expected");
      }
      return (OperonProvenance)toRet;
    }
  }

  public void validate() {
  }
}
