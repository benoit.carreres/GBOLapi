package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.VariableSegment;
import life.gbol.domain.VariableSegmentProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class VariableSegmentImpl extends ImmunoglobulinFeatureImpl implements VariableSegment {
  public static final String TypeIRI = "http://gbol.life#VariableSegment";

  protected VariableSegmentImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static VariableSegment make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,VariableSegment.class);
      if(toRet == null) {
        toRet = new VariableSegmentImpl(domain,iri);;
      }
      else if(!(toRet instanceof VariableSegment)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.VariableSegmentImpl expected");
      }
      return (VariableSegment)toRet;
    }
  }

  public void validate() {
  }

  public VariableSegmentProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,VariableSegmentProvenance.class);
  }

  public void setProvenance(VariableSegmentProvenance val) {
    this.setRef("http://gbol.life#provenance",val,VariableSegmentProvenance.class);
  }
}
