package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.ModifiedBaseProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ModifiedBaseProvenanceImpl extends ChangeFeatureProvenanceImpl implements ModifiedBaseProvenance {
  public static final String TypeIRI = "http://gbol.life#ModifiedBaseProvenance";

  protected ModifiedBaseProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static ModifiedBaseProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,ModifiedBaseProvenance.class);
      if(toRet == null) {
        toRet = new ModifiedBaseProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof ModifiedBaseProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ModifiedBaseProvenanceImpl expected");
      }
      return (ModifiedBaseProvenance)toRet;
    }
  }

  public void validate() {
  }
}
