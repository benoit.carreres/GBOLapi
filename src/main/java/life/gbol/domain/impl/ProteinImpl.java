package life.gbol.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.Protein;
import life.gbol.domain.ProteinFeature;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ProteinImpl extends SequenceObjectImpl implements Protein {
  public static final String TypeIRI = "http://gbol.life#Protein";

  protected ProteinImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Protein make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Protein.class);
      if(toRet == null) {
        toRet = new ProteinImpl(domain,iri);;
      }
      else if(!(toRet instanceof Protein)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ProteinImpl expected");
      }
      return (Protein)toRet;
    }
  }

  public void validate() {
  }

  public void remFeature(ProteinFeature val) {
    this.remRef("http://gbol.life#feature",val,true);
  }

  public List<? extends ProteinFeature> getAllFeature() {
    return this.getRefSet("http://gbol.life#feature",true,ProteinFeature.class);
  }

  public void addFeature(ProteinFeature val) {
    this.addRef("http://gbol.life#feature",val);
  }
}
