package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Database;
import life.gbol.domain.XRef;
import life.gbol.domain.XRefProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class XRefImpl extends QualifierImpl implements XRef {
  public static final String TypeIRI = "http://gbol.life#XRef";

  protected XRefImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static XRef make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,XRef.class);
      if(toRet == null) {
        toRet = new XRefImpl(domain,iri);;
      }
      else if(!(toRet instanceof XRef)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.XRefImpl expected");
      }
      return (XRef)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#db");
    this.checkCardMin1("http://gbol.life#accession");
  }

  public String getSecondaryAccession() {
    return this.getStringLit("http://gbol.life#secondaryAccession",true);
  }

  public void setSecondaryAccession(String val) {
    this.setStringLit("http://gbol.life#secondaryAccession",val);
  }

  public Database getDb() {
    return this.getRef("http://gbol.life#db",false,Database.class);
  }

  public void setDb(Database val) {
    this.setRef("http://gbol.life#db",val,Database.class);
  }

  public String getAccession() {
    return this.getStringLit("http://gbol.life#accession",false);
  }

  public void setAccession(String val) {
    this.setStringLit("http://gbol.life#accession",val);
  }

  public XRefProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,XRefProvenance.class);
  }

  public void setProvenance(XRefProvenance val) {
    this.setRef("http://gbol.life#provenance",val,XRefProvenance.class);
  }
}
