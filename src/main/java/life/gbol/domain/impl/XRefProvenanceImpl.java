package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.XRefProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class XRefProvenanceImpl extends QualifierProvenanceImpl implements XRefProvenance {
  public static final String TypeIRI = "http://gbol.life#XRefProvenance";

  protected XRefProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static XRefProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,XRefProvenance.class);
      if(toRet == null) {
        toRet = new XRefProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof XRefProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.XRefProvenanceImpl expected");
      }
      return (XRefProvenance)toRet;
    }
  }

  public void validate() {
  }
}
