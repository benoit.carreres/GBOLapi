package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.AssemblyGapProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class AssemblyGapProvenanceImpl extends AssemblyAnnotationProvenanceImpl implements AssemblyGapProvenance {
  public static final String TypeIRI = "http://gbol.life#AssemblyGapProvenance";

  protected AssemblyGapProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static AssemblyGapProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,AssemblyGapProvenance.class);
      if(toRet == null) {
        toRet = new AssemblyGapProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof AssemblyGapProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.AssemblyGapProvenanceImpl expected");
      }
      return (AssemblyGapProvenance)toRet;
    }
  }

  public void validate() {
  }
}
