package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.GapRegionProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class GapRegionProvenanceImpl extends ImmunoglobulinFeatureProvenanceImpl implements GapRegionProvenance {
  public static final String TypeIRI = "http://gbol.life#GapRegionProvenance";

  protected GapRegionProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static GapRegionProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,GapRegionProvenance.class);
      if(toRet == null) {
        toRet = new GapRegionProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof GapRegionProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.GapRegionProvenanceImpl expected");
      }
      return (GapRegionProvenance)toRet;
    }
  }

  public void validate() {
  }
}
