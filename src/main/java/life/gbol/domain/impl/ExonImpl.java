package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Exon;
import life.gbol.domain.ExonProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ExonImpl extends GeneticElementImpl implements Exon {
  public static final String TypeIRI = "http://gbol.life#Exon";

  protected ExonImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Exon make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Exon.class);
      if(toRet == null) {
        toRet = new ExonImpl(domain,iri);;
      }
      else if(!(toRet instanceof Exon)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ExonImpl expected");
      }
      return (Exon)toRet;
    }
  }

  public void validate() {
  }

  public ExonProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,ExonProvenance.class);
  }

  public void setProvenance(ExonProvenance val) {
    this.setRef("http://gbol.life#provenance",val,ExonProvenance.class);
  }
}
