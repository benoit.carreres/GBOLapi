package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.VariationProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class VariationProvenanceImpl extends ChangeFeatureProvenanceImpl implements VariationProvenance {
  public static final String TypeIRI = "http://gbol.life#VariationProvenance";

  protected VariationProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static VariationProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,VariationProvenance.class);
      if(toRet == null) {
        toRet = new VariationProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof VariationProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.VariationProvenanceImpl expected");
      }
      return (VariationProvenance)toRet;
    }
  }

  public void validate() {
  }
}
