package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.RepeatFeature;
import life.gbol.domain.RepeatFeatureProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class RepeatFeatureImpl extends NonGeneRelatedElementsImpl implements RepeatFeature {
  public static final String TypeIRI = "http://gbol.life#RepeatFeature";

  protected RepeatFeatureImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static RepeatFeature make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,RepeatFeature.class);
      if(toRet == null) {
        toRet = new RepeatFeatureImpl(domain,iri);;
      }
      else if(!(toRet instanceof RepeatFeature)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.RepeatFeatureImpl expected");
      }
      return (RepeatFeature)toRet;
    }
  }

  public void validate() {
  }

  public String getRpt_type() {
    return this.getStringLit("http://gbol.life#rpt_type",true);
  }

  public void setRpt_type(String val) {
    this.setStringLit("http://gbol.life#rpt_type",val);
  }

  public String getRpt_unit_range() {
    return this.getStringLit("http://gbol.life#rpt_unit_range",true);
  }

  public void setRpt_unit_range(String val) {
    this.setStringLit("http://gbol.life#rpt_unit_range",val);
  }

  public String getMobile_element() {
    return this.getStringLit("http://gbol.life#mobile_element",true);
  }

  public void setMobile_element(String val) {
    this.setStringLit("http://gbol.life#mobile_element",val);
  }

  public RepeatFeatureProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,RepeatFeatureProvenance.class);
  }

  public void setProvenance(RepeatFeatureProvenance val) {
    this.setRef("http://gbol.life#provenance",val,RepeatFeatureProvenance.class);
  }

  public String getRpt_family() {
    return this.getStringLit("http://gbol.life#rpt_family",true);
  }

  public void setRpt_family(String val) {
    this.setStringLit("http://gbol.life#rpt_family",val);
  }

  public String getRpt_unit_seq() {
    return this.getStringLit("http://gbol.life#rpt_unit_seq",true);
  }

  public void setRpt_unit_seq(String val) {
    this.setStringLit("http://gbol.life#rpt_unit_seq",val);
  }
}
