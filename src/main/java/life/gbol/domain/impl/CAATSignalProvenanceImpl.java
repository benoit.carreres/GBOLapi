package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.CAATSignalProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class CAATSignalProvenanceImpl extends RegulationSiteProvenanceImpl implements CAATSignalProvenance {
  public static final String TypeIRI = "http://gbol.life#CAATSignalProvenance";

  protected CAATSignalProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static CAATSignalProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,CAATSignalProvenance.class);
      if(toRet == null) {
        toRet = new CAATSignalProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof CAATSignalProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.CAATSignalProvenanceImpl expected");
      }
      return (CAATSignalProvenance)toRet;
    }
  }

  public void validate() {
  }
}
