package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.rRNA;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class rRNAImpl extends MaturedRNAImpl implements rRNA {
  public static final String TypeIRI = "http://gbol.life#rRNA";

  protected rRNAImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static rRNA make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,rRNA.class);
      if(toRet == null) {
        toRet = new rRNAImpl(domain,iri);;
      }
      else if(!(toRet instanceof rRNA)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.rRNAImpl expected");
      }
      return (rRNA)toRet;
    }
  }

  public void validate() {
  }
}
