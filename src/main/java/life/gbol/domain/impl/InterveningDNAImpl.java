package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.InterveningDNA;
import life.gbol.domain.InterveningDNAProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class InterveningDNAImpl extends GeneticElementImpl implements InterveningDNA {
  public static final String TypeIRI = "http://gbol.life#InterveningDNA";

  protected InterveningDNAImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static InterveningDNA make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,InterveningDNA.class);
      if(toRet == null) {
        toRet = new InterveningDNAImpl(domain,iri);;
      }
      else if(!(toRet instanceof InterveningDNA)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.InterveningDNAImpl expected");
      }
      return (InterveningDNA)toRet;
    }
  }

  public void validate() {
  }

  public InterveningDNAProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,InterveningDNAProvenance.class);
  }

  public void setProvenance(InterveningDNAProvenance val) {
    this.setRef("http://gbol.life#provenance",val,InterveningDNAProvenance.class);
  }
}
