package life.gbol.domain.impl;

import java.lang.Integer;
import java.lang.String;
import java.util.List;
import life.gbol.domain.Book;
import life.gbol.domain.Person;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class BookImpl extends PublicationImpl implements Book {
  public static final String TypeIRI = "http://gbol.life#Book";

  protected BookImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Book make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Book.class);
      if(toRet == null) {
        toRet = new BookImpl(domain,iri);;
      }
      else if(!(toRet instanceof Book)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.BookImpl expected");
      }
      return (Book)toRet;
    }
  }

  public void validate() {
  }

  public String getBookTitle() {
    return this.getStringLit("http://gbol.life#bookTitle",true);
  }

  public void setBookTitle(String val) {
    this.setStringLit("http://gbol.life#bookTitle",val);
  }

  public String getPublisher() {
    return this.getStringLit("http://gbol.life#publisher",true);
  }

  public void setPublisher(String val) {
    this.setStringLit("http://gbol.life#publisher",val);
  }

  public String getLastPage() {
    return this.getStringLit("http://gbol.life#lastPage",true);
  }

  public void setLastPage(String val) {
    this.setStringLit("http://gbol.life#lastPage",val);
  }

  public Integer getYear() {
    return this.getIntegerLit("http://gbol.life#year",true);
  }

  public void setYear(Integer val) {
    this.setIntegerLit("http://gbol.life#year",val);
  }

  public String getFirstPage() {
    return this.getStringLit("http://gbol.life#firstPage",true);
  }

  public void setFirstPage(String val) {
    this.setStringLit("http://gbol.life#firstPage",val);
  }

  public void remEditors(Person val) {
    this.remRef("http://gbol.life#editors",val,true);
  }

  public List<? extends Person> getAllEditors() {
    return this.getRefSet("http://gbol.life#editors",true,Person.class);
  }

  public void addEditors(Person val) {
    this.addRef("http://gbol.life#editors",val);
  }
}
