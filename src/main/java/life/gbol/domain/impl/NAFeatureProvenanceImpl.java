package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.NAFeatureProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class NAFeatureProvenanceImpl extends FeatureProvenanceImpl implements NAFeatureProvenance {
  public static final String TypeIRI = "http://gbol.life#NAFeatureProvenance";

  protected NAFeatureProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static NAFeatureProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,NAFeatureProvenance.class);
      if(toRet == null) {
        toRet = new NAFeatureProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof NAFeatureProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.NAFeatureProvenanceImpl expected");
      }
      return (NAFeatureProvenance)toRet;
    }
  }

  public void validate() {
  }
}
