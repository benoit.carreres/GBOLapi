package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.DLoopProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class DLoopProvenanceImpl extends BiologicalRecognizedRegionProvenanceImpl implements DLoopProvenance {
  public static final String TypeIRI = "http://gbol.life#DLoopProvenance";

  protected DLoopProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static DLoopProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,DLoopProvenance.class);
      if(toRet == null) {
        toRet = new DLoopProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof DLoopProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.DLoopProvenanceImpl expected");
      }
      return (DLoopProvenance)toRet;
    }
  }

  public void validate() {
  }
}
