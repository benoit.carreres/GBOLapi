package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.NonGeneRelatedElements;
import life.gbol.domain.NonGeneRelatedElementsProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class NonGeneRelatedElementsImpl extends NAFeatureImpl implements NonGeneRelatedElements {
  public static final String TypeIRI = "http://gbol.life#NonGeneRelatedElements";

  protected NonGeneRelatedElementsImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static NonGeneRelatedElements make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,NonGeneRelatedElements.class);
      if(toRet == null) {
        toRet = new NonGeneRelatedElementsImpl(domain,iri);;
      }
      else if(!(toRet instanceof NonGeneRelatedElements)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.NonGeneRelatedElementsImpl expected");
      }
      return (NonGeneRelatedElements)toRet;
    }
  }

  public void validate() {
  }

  public NonGeneRelatedElementsProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,NonGeneRelatedElementsProvenance.class);
  }

  public void setProvenance(NonGeneRelatedElementsProvenance val) {
    this.setRef("http://gbol.life#provenance",val,NonGeneRelatedElementsProvenance.class);
  }
}
