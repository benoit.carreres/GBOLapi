package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.UncompleteNAObject;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class UncompleteNAObjectImpl extends DNAObjectImpl implements UncompleteNAObject {
  public static final String TypeIRI = "http://gbol.life#UncompleteNAObject";

  protected UncompleteNAObjectImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static UncompleteNAObject make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,UncompleteNAObject.class);
      if(toRet == null) {
        toRet = new UncompleteNAObjectImpl(domain,iri);;
      }
      else if(!(toRet instanceof UncompleteNAObject)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.UncompleteNAObjectImpl expected");
      }
      return (UncompleteNAObject)toRet;
    }
  }

  public void validate() {
  }
}
