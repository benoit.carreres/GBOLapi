package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.VariableSegmentProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class VariableSegmentProvenanceImpl extends ImmunoglobulinFeatureProvenanceImpl implements VariableSegmentProvenance {
  public static final String TypeIRI = "http://gbol.life#VariableSegmentProvenance";

  protected VariableSegmentProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static VariableSegmentProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,VariableSegmentProvenance.class);
      if(toRet == null) {
        toRet = new VariableSegmentProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof VariableSegmentProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.VariableSegmentProvenanceImpl expected");
      }
      return (VariableSegmentProvenance)toRet;
    }
  }

  public void validate() {
  }
}
