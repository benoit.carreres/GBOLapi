package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.TATASignalProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class TATASignalProvenanceImpl extends RegulationSiteProvenanceImpl implements TATASignalProvenance {
  public static final String TypeIRI = "http://gbol.life#TATASignalProvenance";

  protected TATASignalProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static TATASignalProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,TATASignalProvenance.class);
      if(toRet == null) {
        toRet = new TATASignalProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof TATASignalProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.TATASignalProvenanceImpl expected");
      }
      return (TATASignalProvenance)toRet;
    }
  }

  public void validate() {
  }
}
