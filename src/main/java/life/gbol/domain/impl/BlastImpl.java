package life.gbol.domain.impl;

import java.lang.Double;
import java.lang.Float;
import java.lang.Integer;
import java.lang.String;
import life.gbol.domain.Blast;
import life.gbol.domain.Cog;
import life.gbol.domain.XRef;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class BlastImpl extends ProteinFeatureProvenanceImpl implements Blast {
  public static final String TypeIRI = "http://gbol.life#Blast";

  protected BlastImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Blast make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Blast.class);
      if(toRet == null) {
        toRet = new BlastImpl(domain,iri);;
      }
      else if(!(toRet instanceof Blast)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.BlastImpl expected");
      }
      return (Blast)toRet;
    }
  }

  public void validate() {
  }

  public String getQueryid() {
    return this.getStringLit("http://gbol.life#queryid",true);
  }

  public void setQueryid(String val) {
    this.setStringLit("http://gbol.life#queryid",val);
  }

  public Integer getSstart() {
    return this.getIntegerLit("http://gbol.life#sstart",true);
  }

  public void setSstart(Integer val) {
    this.setIntegerLit("http://gbol.life#sstart",val);
  }

  public Double getEvalue() {
    return this.getDoubleLit("http://gbol.life#evalue",true);
  }

  public void setEvalue(Double val) {
    this.setDoubleLit("http://gbol.life#evalue",val);
  }

  public Integer getAlignment_length() {
    return this.getIntegerLit("http://gbol.life#alignment_length",true);
  }

  public void setAlignment_length(Integer val) {
    this.setIntegerLit("http://gbol.life#alignment_length",val);
  }

  public XRef getXref() {
    return this.getRef("http://gbol.life#xref",true,XRef.class);
  }

  public void setXref(XRef val) {
    this.setRef("http://gbol.life#xref",val,XRef.class);
  }

  public Cog getCog() {
    return this.getRef("http://gbol.life#cog",true,Cog.class);
  }

  public void setCog(Cog val) {
    this.setRef("http://gbol.life#cog",val,Cog.class);
  }

  public Float getPercidentity() {
    return this.getFloatLit("http://gbol.life#percidentity",true);
  }

  public void setPercidentity(Float val) {
    this.setFloatLit("http://gbol.life#percidentity",val);
  }

  public Float getBitscore() {
    return this.getFloatLit("http://gbol.life#bitscore",true);
  }

  public void setBitscore(Float val) {
    this.setFloatLit("http://gbol.life#bitscore",val);
  }

  public String getSubjectid() {
    return this.getStringLit("http://gbol.life#subjectid",true);
  }

  public void setSubjectid(String val) {
    this.setStringLit("http://gbol.life#subjectid",val);
  }

  public Integer getGaps() {
    return this.getIntegerLit("http://gbol.life#gaps",true);
  }

  public void setGaps(Integer val) {
    this.setIntegerLit("http://gbol.life#gaps",val);
  }

  public Integer getSend() {
    return this.getIntegerLit("http://gbol.life#send",true);
  }

  public void setSend(Integer val) {
    this.setIntegerLit("http://gbol.life#send",val);
  }

  public Integer getQend() {
    return this.getIntegerLit("http://gbol.life#qend",true);
  }

  public void setQend(Integer val) {
    this.setIntegerLit("http://gbol.life#qend",val);
  }

  public String getSubjectname() {
    return this.getStringLit("http://gbol.life#subjectname",true);
  }

  public void setSubjectname(String val) {
    this.setStringLit("http://gbol.life#subjectname",val);
  }

  public Integer getMismatches() {
    return this.getIntegerLit("http://gbol.life#mismatches",true);
  }

  public void setMismatches(Integer val) {
    this.setIntegerLit("http://gbol.life#mismatches",val);
  }

  public Integer getQstart() {
    return this.getIntegerLit("http://gbol.life#qstart",true);
  }

  public void setQstart(Integer val) {
    this.setIntegerLit("http://gbol.life#qstart",val);
  }
}
