package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.EnhancerProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class EnhancerProvenanceImpl extends RegulationSiteProvenanceImpl implements EnhancerProvenance {
  public static final String TypeIRI = "http://gbol.life#EnhancerProvenance";

  protected EnhancerProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static EnhancerProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,EnhancerProvenance.class);
      if(toRet == null) {
        toRet = new EnhancerProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof EnhancerProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.EnhancerProvenanceImpl expected");
      }
      return (EnhancerProvenance)toRet;
    }
  }

  public void validate() {
  }
}
