package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.MaturePeptideProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class MaturePeptideProvenanceImpl extends ProteinFeatureProvenanceImpl implements MaturePeptideProvenance {
  public static final String TypeIRI = "http://gbol.life#MaturePeptideProvenance";

  protected MaturePeptideProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static MaturePeptideProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,MaturePeptideProvenance.class);
      if(toRet == null) {
        toRet = new MaturePeptideProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof MaturePeptideProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.MaturePeptideProvenanceImpl expected");
      }
      return (MaturePeptideProvenance)toRet;
    }
  }

  public void validate() {
  }
}
