package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.InBetween;
import life.gbol.domain.Position;
import life.gbol.domain.StrandType;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class InBetweenImpl extends LocationImpl implements InBetween {
  public static final String TypeIRI = "http://gbol.life#InBetween";

  protected InBetweenImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static InBetween make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,InBetween.class);
      if(toRet == null) {
        toRet = new InBetweenImpl(domain,iri);;
      }
      else if(!(toRet instanceof InBetween)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.InBetweenImpl expected");
      }
      return (InBetween)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#after");
  }

  public Position getAfter() {
    return this.getRef("http://gbol.life#after",false,Position.class);
  }

  public void setAfter(Position val) {
    this.setRef("http://gbol.life#after",val,Position.class);
  }

  public StrandType getStrand() {
    return this.getEnum("http://gbol.life#strand",true,StrandType.class);
  }

  public void setStrand(StrandType val) {
    this.setEnum("http://gbol.life#strand",val,StrandType.class);
  }
}
