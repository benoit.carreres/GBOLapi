package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.RecognizedRegionProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class RecognizedRegionProvenanceImpl extends NonGeneRelatedElementsProvenanceImpl implements RecognizedRegionProvenance {
  public static final String TypeIRI = "http://gbol.life#RecognizedRegionProvenance";

  protected RecognizedRegionProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static RecognizedRegionProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,RecognizedRegionProvenance.class);
      if(toRet == null) {
        toRet = new RecognizedRegionProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof RecognizedRegionProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.RecognizedRegionProvenanceImpl expected");
      }
      return (RecognizedRegionProvenance)toRet;
    }
  }

  public void validate() {
  }
}
