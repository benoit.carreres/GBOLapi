package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.TranscriptFeature;
import life.gbol.domain.TranscriptFeatureProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class TranscriptFeatureImpl extends GeneRelatedElementsImpl implements TranscriptFeature {
  public static final String TypeIRI = "http://gbol.life#TranscriptFeature";

  protected TranscriptFeatureImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static TranscriptFeature make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,TranscriptFeature.class);
      if(toRet == null) {
        toRet = new TranscriptFeatureImpl(domain,iri);;
      }
      else if(!(toRet instanceof TranscriptFeature)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.TranscriptFeatureImpl expected");
      }
      return (TranscriptFeature)toRet;
    }
  }

  public void validate() {
  }

  public TranscriptFeatureProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,TranscriptFeatureProvenance.class);
  }

  public void setProvenance(TranscriptFeatureProvenance val) {
    this.setRef("http://gbol.life#provenance",val,TranscriptFeatureProvenance.class);
  }
}
