package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.AssemblyAnnotation;
import life.gbol.domain.AssemblyAnnotationProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class AssemblyAnnotationImpl extends SequenceAnnotationImpl implements AssemblyAnnotation {
  public static final String TypeIRI = "http://gbol.life#AssemblyAnnotation";

  protected AssemblyAnnotationImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static AssemblyAnnotation make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,AssemblyAnnotation.class);
      if(toRet == null) {
        toRet = new AssemblyAnnotationImpl(domain,iri);;
      }
      else if(!(toRet instanceof AssemblyAnnotation)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.AssemblyAnnotationImpl expected");
      }
      return (AssemblyAnnotation)toRet;
    }
  }

  public void validate() {
  }

  public AssemblyAnnotationProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,AssemblyAnnotationProvenance.class);
  }

  public void setProvenance(AssemblyAnnotationProvenance val) {
    this.setRef("http://gbol.life#provenance",val,AssemblyAnnotationProvenance.class);
  }
}
