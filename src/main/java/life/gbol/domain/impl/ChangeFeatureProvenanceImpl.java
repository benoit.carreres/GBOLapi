package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.ChangeFeatureProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ChangeFeatureProvenanceImpl extends SequenceAnnotationProvenanceImpl implements ChangeFeatureProvenance {
  public static final String TypeIRI = "http://gbol.life#ChangeFeatureProvenance";

  protected ChangeFeatureProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static ChangeFeatureProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,ChangeFeatureProvenance.class);
      if(toRet == null) {
        toRet = new ChangeFeatureProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof ChangeFeatureProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ChangeFeatureProvenanceImpl expected");
      }
      return (ChangeFeatureProvenance)toRet;
    }
  }

  public void validate() {
  }
}
