package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.ElectronicReference;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ElectronicReferenceImpl extends PublicationImpl implements ElectronicReference {
  public static final String TypeIRI = "http://gbol.life#ElectronicReference";

  protected ElectronicReferenceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static ElectronicReference make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,ElectronicReference.class);
      if(toRet == null) {
        toRet = new ElectronicReferenceImpl(domain,iri);;
      }
      else if(!(toRet instanceof ElectronicReference)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ElectronicReferenceImpl expected");
      }
      return (ElectronicReference)toRet;
    }
  }

  public void validate() {
  }

  public String getText() {
    return this.getStringLit("http://gbol.life#text",true);
  }

  public void setText(String val) {
    this.setStringLit("http://gbol.life#text",val);
  }
}
