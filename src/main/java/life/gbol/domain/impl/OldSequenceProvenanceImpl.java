package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.OldSequenceProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class OldSequenceProvenanceImpl extends AssemblyAnnotationProvenanceImpl implements OldSequenceProvenance {
  public static final String TypeIRI = "http://gbol.life#OldSequenceProvenance";

  protected OldSequenceProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static OldSequenceProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,OldSequenceProvenance.class);
      if(toRet == null) {
        toRet = new OldSequenceProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof OldSequenceProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.OldSequenceProvenanceImpl expected");
      }
      return (OldSequenceProvenance)toRet;
    }
  }

  public void validate() {
  }
}
