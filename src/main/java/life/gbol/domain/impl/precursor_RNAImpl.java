package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.precursor_RNA;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class precursor_RNAImpl extends TranscriptImpl implements precursor_RNA {
  public static final String TypeIRI = "http://gbol.life#precursor_RNA";

  protected precursor_RNAImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static precursor_RNA make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,precursor_RNA.class);
      if(toRet == null) {
        toRet = new precursor_RNAImpl(domain,iri);;
      }
      else if(!(toRet instanceof precursor_RNA)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.precursor_RNAImpl expected");
      }
      return (precursor_RNA)toRet;
    }
  }

  public void validate() {
  }
}
