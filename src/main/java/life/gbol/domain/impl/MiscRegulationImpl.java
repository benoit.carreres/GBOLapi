package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.MiscRegulation;
import life.gbol.domain.MiscRegulationProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class MiscRegulationImpl extends RegulationSiteImpl implements MiscRegulation {
  public static final String TypeIRI = "http://gbol.life#MiscRegulation";

  protected MiscRegulationImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static MiscRegulation make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,MiscRegulation.class);
      if(toRet == null) {
        toRet = new MiscRegulationImpl(domain,iri);;
      }
      else if(!(toRet instanceof MiscRegulation)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.MiscRegulationImpl expected");
      }
      return (MiscRegulation)toRet;
    }
  }

  public void validate() {
  }

  public MiscRegulationProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,MiscRegulationProvenance.class);
  }

  public void setProvenance(MiscRegulationProvenance val) {
    this.setRef("http://gbol.life#provenance",val,MiscRegulationProvenance.class);
  }
}
