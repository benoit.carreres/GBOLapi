package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.mRNA;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class mRNAImpl extends MaturedRNAImpl implements mRNA {
  public static final String TypeIRI = "http://gbol.life#mRNA";

  protected mRNAImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static mRNA make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,mRNA.class);
      if(toRet == null) {
        toRet = new mRNAImpl(domain,iri);;
      }
      else if(!(toRet instanceof mRNA)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.mRNAImpl expected");
      }
      return (mRNA)toRet;
    }
  }

  public void validate() {
  }
}
