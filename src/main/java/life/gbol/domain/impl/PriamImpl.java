package life.gbol.domain.impl;

import java.lang.Float;
import java.lang.Integer;
import java.lang.String;
import java.util.List;
import life.gbol.domain.Priam;
import life.gbol.domain.XRef;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class PriamImpl extends ProteinFeatureProvenanceImpl implements Priam {
  public static final String TypeIRI = "http://gbol.life#Priam";

  protected PriamImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Priam make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Priam.class);
      if(toRet == null) {
        toRet = new PriamImpl(domain,iri);;
      }
      else if(!(toRet instanceof Priam)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.PriamImpl expected");
      }
      return (Priam)toRet;
    }
  }

  public void validate() {
  }

  public Integer getQueryTo() {
    return this.getIntegerLit("http://gbol.life#queryTo",true);
  }

  public void setQueryTo(Integer val) {
    this.setIntegerLit("http://gbol.life#queryTo",val);
  }

  public Float getEvalue() {
    return this.getFloatLit("http://gbol.life#evalue",true);
  }

  public void setEvalue(Float val) {
    this.setFloatLit("http://gbol.life#evalue",val);
  }

  public void remXref(XRef val) {
    this.remRef("http://gbol.life#xref",val,true);
  }

  public List<? extends XRef> getAllXref() {
    return this.getRefSet("http://gbol.life#xref",true,XRef.class);
  }

  public void addXref(XRef val) {
    this.addRef("http://gbol.life#xref",val);
  }

  public String getQueryStrand() {
    return this.getStringLit("http://gbol.life#queryStrand",true);
  }

  public void setQueryStrand(String val) {
    this.setStringLit("http://gbol.life#queryStrand",val);
  }

  public Float getProfileProportion() {
    return this.getFloatLit("http://gbol.life#profileProportion",true);
  }

  public void setProfileProportion(Float val) {
    this.setFloatLit("http://gbol.life#profileProportion",val);
  }

  public Float getBitScore() {
    return this.getFloatLit("http://gbol.life#bitScore",true);
  }

  public void setBitScore(Float val) {
    this.setFloatLit("http://gbol.life#bitScore",val);
  }

  public Integer getAlignLength() {
    return this.getIntegerLit("http://gbol.life#alignLength",true);
  }

  public void setAlignLength(Integer val) {
    this.setIntegerLit("http://gbol.life#alignLength",val);
  }

  public Integer getQueryLength() {
    return this.getIntegerLit("http://gbol.life#queryLength",true);
  }

  public void setQueryLength(Integer val) {
    this.setIntegerLit("http://gbol.life#queryLength",val);
  }

  public Float getPositiveHitProbability() {
    return this.getFloatLit("http://gbol.life#positiveHitProbability",true);
  }

  public void setPositiveHitProbability(Float val) {
    this.setFloatLit("http://gbol.life#positiveHitProbability",val);
  }

  public Integer getProfileFrom() {
    return this.getIntegerLit("http://gbol.life#profileFrom",true);
  }

  public void setProfileFrom(Integer val) {
    this.setIntegerLit("http://gbol.life#profileFrom",val);
  }

  public String getIsBestOverlap() {
    return this.getStringLit("http://gbol.life#isBestOverlap",true);
  }

  public void setIsBestOverlap(String val) {
    this.setStringLit("http://gbol.life#isBestOverlap",val);
  }

  public String getFoundCatalyticPattern() {
    return this.getStringLit("http://gbol.life#foundCatalyticPattern",true);
  }

  public void setFoundCatalyticPattern(String val) {
    this.setStringLit("http://gbol.life#foundCatalyticPattern",val);
  }

  public Integer getProfileLength() {
    return this.getIntegerLit("http://gbol.life#profileLength",true);
  }

  public void setProfileLength(Integer val) {
    this.setIntegerLit("http://gbol.life#profileLength",val);
  }

  public Integer getQueryFrom() {
    return this.getIntegerLit("http://gbol.life#queryFrom",true);
  }

  public void setQueryFrom(Integer val) {
    this.setIntegerLit("http://gbol.life#queryFrom",val);
  }

  public String getProfileId() {
    return this.getStringLit("http://gbol.life#profileId",true);
  }

  public void setProfileId(String val) {
    this.setStringLit("http://gbol.life#profileId",val);
  }

  public Integer getProfileTo() {
    return this.getIntegerLit("http://gbol.life#profileTo",true);
  }

  public void setProfileTo(Integer val) {
    this.setIntegerLit("http://gbol.life#profileTo",val);
  }
}
