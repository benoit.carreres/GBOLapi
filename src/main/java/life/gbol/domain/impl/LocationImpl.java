package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Location;
import life.gbol.domain.SequenceObject;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;

/**
 * Code generated from http://gbol.life# ontology
 */
public class LocationImpl extends OWLThingImpl implements Location {
  public static final String TypeIRI = "http://gbol.life#Location";

  protected LocationImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Location make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Location.class);
      if(toRet == null) {
        toRet = new LocationImpl(domain,iri);;
      }
      else if(!(toRet instanceof Location)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.LocationImpl expected");
      }
      return (Location)toRet;
    }
  }

  public void validate() {
  }

  public SequenceObject getReference() {
    return this.getRef("http://gbol.life#reference",true,SequenceObject.class);
  }

  public void setReference(SequenceObject val) {
    this.setRef("http://gbol.life#reference",val,SequenceObject.class);
  }
}
