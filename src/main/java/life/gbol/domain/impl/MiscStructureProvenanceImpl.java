package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.MiscStructureProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class MiscStructureProvenanceImpl extends StructureFeatureProvenanceImpl implements MiscStructureProvenance {
  public static final String TypeIRI = "http://gbol.life#MiscStructureProvenance";

  protected MiscStructureProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static MiscStructureProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,MiscStructureProvenance.class);
      if(toRet == null) {
        toRet = new MiscStructureProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof MiscStructureProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.MiscStructureProvenanceImpl expected");
      }
      return (MiscStructureProvenance)toRet;
    }
  }

  public void validate() {
  }
}
