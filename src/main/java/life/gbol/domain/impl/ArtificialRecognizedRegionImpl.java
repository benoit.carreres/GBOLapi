package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.ArtificialRecognizedRegion;
import life.gbol.domain.ArtificialRecognizedRegionProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ArtificialRecognizedRegionImpl extends RecognizedRegionImpl implements ArtificialRecognizedRegion {
  public static final String TypeIRI = "http://gbol.life#ArtificialRecognizedRegion";

  protected ArtificialRecognizedRegionImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static ArtificialRecognizedRegion make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,ArtificialRecognizedRegion.class);
      if(toRet == null) {
        toRet = new ArtificialRecognizedRegionImpl(domain,iri);;
      }
      else if(!(toRet instanceof ArtificialRecognizedRegion)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ArtificialRecognizedRegionImpl expected");
      }
      return (ArtificialRecognizedRegion)toRet;
    }
  }

  public void validate() {
  }

  public ArtificialRecognizedRegionProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,ArtificialRecognizedRegionProvenance.class);
  }

  public void setProvenance(ArtificialRecognizedRegionProvenance val) {
    this.setRef("http://gbol.life#provenance",val,ArtificialRecognizedRegionProvenance.class);
  }
}
