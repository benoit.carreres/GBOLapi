package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.ProteinFeature;
import life.gbol.domain.ProteinFeatureProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ProteinFeatureImpl extends FeatureImpl implements ProteinFeature {
  public static final String TypeIRI = "http://gbol.life#ProteinFeature";

  protected ProteinFeatureImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static ProteinFeature make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,ProteinFeature.class);
      if(toRet == null) {
        toRet = new ProteinFeatureImpl(domain,iri);;
      }
      else if(!(toRet instanceof ProteinFeature)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ProteinFeatureImpl expected");
      }
      return (ProteinFeature)toRet;
    }
  }

  public void validate() {
  }

  public ProteinFeatureProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,ProteinFeatureProvenance.class);
  }

  public void setProvenance(ProteinFeatureProvenance val) {
    this.setRef("http://gbol.life#provenance",val,ProteinFeatureProvenance.class);
  }
}
