package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.SequenceAnnotationProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class SequenceAnnotationProvenanceImpl extends NAFeatureProvenanceImpl implements SequenceAnnotationProvenance {
  public static final String TypeIRI = "http://gbol.life#SequenceAnnotationProvenance";

  protected SequenceAnnotationProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static SequenceAnnotationProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,SequenceAnnotationProvenance.class);
      if(toRet == null) {
        toRet = new SequenceAnnotationProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof SequenceAnnotationProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.SequenceAnnotationProvenanceImpl expected");
      }
      return (SequenceAnnotationProvenance)toRet;
    }
  }

  public void validate() {
  }
}
