package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.TranscriptFeatureProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class TranscriptFeatureProvenanceImpl extends GeneRelatedElementsProvenanceImpl implements TranscriptFeatureProvenance {
  public static final String TypeIRI = "http://gbol.life#TranscriptFeatureProvenance";

  protected TranscriptFeatureProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static TranscriptFeatureProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,TranscriptFeatureProvenance.class);
      if(toRet == null) {
        toRet = new TranscriptFeatureProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof TranscriptFeatureProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.TranscriptFeatureProvenanceImpl expected");
      }
      return (TranscriptFeatureProvenance)toRet;
    }
  }

  public void validate() {
  }
}
