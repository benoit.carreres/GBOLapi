package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.TransitPeptideProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class TransitPeptideProvenanceImpl extends ProteinFeatureProvenanceImpl implements TransitPeptideProvenance {
  public static final String TypeIRI = "http://gbol.life#TransitPeptideProvenance";

  protected TransitPeptideProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static TransitPeptideProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,TransitPeptideProvenance.class);
      if(toRet == null) {
        toRet = new TransitPeptideProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof TransitPeptideProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.TransitPeptideProvenanceImpl expected");
      }
      return (TransitPeptideProvenance)toRet;
    }
  }

  public void validate() {
  }
}
