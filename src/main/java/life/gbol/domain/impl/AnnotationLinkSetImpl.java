package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.AnnotationLinkSet;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class AnnotationLinkSetImpl extends AnnotationResultImpl implements AnnotationLinkSet {
  public static final String TypeIRI = "http://gbol.life#AnnotationLinkSet";

  protected AnnotationLinkSetImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static AnnotationLinkSet make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,AnnotationLinkSet.class);
      if(toRet == null) {
        toRet = new AnnotationLinkSetImpl(domain,iri);;
      }
      else if(!(toRet instanceof AnnotationLinkSet)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.AnnotationLinkSetImpl expected");
      }
      return (AnnotationLinkSet)toRet;
    }
  }

  public void validate() {
  }
}
