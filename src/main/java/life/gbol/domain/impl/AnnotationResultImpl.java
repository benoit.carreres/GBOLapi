package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.AnnotationActivity;
import life.gbol.domain.AnnotationResult;
import life.gbol.domain.AnnotationSoftware;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.w3.ns.prov.domain.impl.EntityImpl;

/**
 * Code generated from http://gbol.life# ontology
 */
public class AnnotationResultImpl extends EntityImpl implements AnnotationResult {
  public static final String TypeIRI = "http://gbol.life#AnnotationResult";

  protected AnnotationResultImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static AnnotationResult make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,AnnotationResult.class);
      if(toRet == null) {
        toRet = new AnnotationResultImpl(domain,iri);;
      }
      else if(!(toRet instanceof AnnotationResult)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.AnnotationResultImpl expected");
      }
      return (AnnotationResult)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://www.w3.org/ns/prov#wasGeneratedBy");
    this.checkCardMin1("http://www.w3.org/ns/prov#wasAttributedTo");
  }

  public AnnotationActivity getWasGeneratedBy() {
    return this.getRef("http://www.w3.org/ns/prov#wasGeneratedBy",false,AnnotationActivity.class);
  }

  public void setWasGeneratedBy(AnnotationActivity val) {
    this.setRef("http://www.w3.org/ns/prov#wasGeneratedBy",val,AnnotationActivity.class);
  }

  public AnnotationSoftware getWasAttributedTo() {
    return this.getRef("http://www.w3.org/ns/prov#wasAttributedTo",false,AnnotationSoftware.class);
  }

  public void setWasAttributedTo(AnnotationSoftware val) {
    this.setRef("http://www.w3.org/ns/prov#wasAttributedTo",val,AnnotationSoftware.class);
  }
}
