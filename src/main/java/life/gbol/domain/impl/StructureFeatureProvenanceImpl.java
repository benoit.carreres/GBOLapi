package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.StructureFeatureProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class StructureFeatureProvenanceImpl extends NonGeneRelatedElementsProvenanceImpl implements StructureFeatureProvenance {
  public static final String TypeIRI = "http://gbol.life#StructureFeatureProvenance";

  protected StructureFeatureProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static StructureFeatureProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,StructureFeatureProvenance.class);
      if(toRet == null) {
        toRet = new StructureFeatureProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof StructureFeatureProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.StructureFeatureProvenanceImpl expected");
      }
      return (StructureFeatureProvenance)toRet;
    }
  }

  public void validate() {
  }
}
