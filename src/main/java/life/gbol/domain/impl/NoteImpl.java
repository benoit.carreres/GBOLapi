package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Note;
import life.gbol.domain.NoteProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class NoteImpl extends QualifierImpl implements Note {
  public static final String TypeIRI = "http://gbol.life#Note";

  protected NoteImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Note make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Note.class);
      if(toRet == null) {
        toRet = new NoteImpl(domain,iri);;
      }
      else if(!(toRet instanceof Note)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.NoteImpl expected");
      }
      return (Note)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#text");
  }

  public String getText() {
    return this.getStringLit("http://gbol.life#text",false);
  }

  public void setText(String val) {
    this.setStringLit("http://gbol.life#text",val);
  }

  public NoteProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,NoteProvenance.class);
  }

  public void setProvenance(NoteProvenance val) {
    this.setRef("http://gbol.life#provenance",val,NoteProvenance.class);
  }
}
