package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.AttenuatorProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class AttenuatorProvenanceImpl extends RegulationSiteProvenanceImpl implements AttenuatorProvenance {
  public static final String TypeIRI = "http://gbol.life#AttenuatorProvenance";

  protected AttenuatorProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static AttenuatorProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,AttenuatorProvenance.class);
      if(toRet == null) {
        toRet = new AttenuatorProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof AttenuatorProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.AttenuatorProvenanceImpl expected");
      }
      return (AttenuatorProvenance)toRet;
    }
  }

  public void validate() {
  }
}
