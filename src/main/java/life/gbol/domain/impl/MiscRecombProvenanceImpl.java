package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.MiscRecombProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class MiscRecombProvenanceImpl extends BiologicalRecognizedRegionProvenanceImpl implements MiscRecombProvenance {
  public static final String TypeIRI = "http://gbol.life#MiscRecombProvenance";

  protected MiscRecombProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static MiscRecombProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,MiscRecombProvenance.class);
      if(toRet == null) {
        toRet = new MiscRecombProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof MiscRecombProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.MiscRecombProvenanceImpl expected");
      }
      return (MiscRecombProvenance)toRet;
    }
  }

  public void validate() {
  }
}
