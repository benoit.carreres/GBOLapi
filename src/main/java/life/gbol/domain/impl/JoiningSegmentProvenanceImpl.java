package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.JoiningSegmentProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class JoiningSegmentProvenanceImpl extends ImmunoglobulinFeatureProvenanceImpl implements JoiningSegmentProvenance {
  public static final String TypeIRI = "http://gbol.life#JoiningSegmentProvenance";

  protected JoiningSegmentProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static JoiningSegmentProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,JoiningSegmentProvenance.class);
      if(toRet == null) {
        toRet = new JoiningSegmentProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof JoiningSegmentProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.JoiningSegmentProvenanceImpl expected");
      }
      return (JoiningSegmentProvenance)toRet;
    }
  }

  public void validate() {
  }
}
