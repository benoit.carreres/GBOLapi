package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Feature;
import life.gbol.domain.XRefFeatureBased;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class XRefFeatureBasedImpl extends XRefProvenanceImpl implements XRefFeatureBased {
  public static final String TypeIRI = "http://gbol.life#XRefFeatureBased";

  protected XRefFeatureBasedImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static XRefFeatureBased make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,XRefFeatureBased.class);
      if(toRet == null) {
        toRet = new XRefFeatureBasedImpl(domain,iri);;
      }
      else if(!(toRet instanceof XRefFeatureBased)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.XRefFeatureBasedImpl expected");
      }
      return (XRefFeatureBased)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#derivedFrom");
  }

  public Feature getDerivedFrom() {
    return this.getRef("http://gbol.life#derivedFrom",false,Feature.class);
  }

  public void setDerivedFrom(Feature val) {
    this.setRef("http://gbol.life#derivedFrom",val,Feature.class);
  }
}
