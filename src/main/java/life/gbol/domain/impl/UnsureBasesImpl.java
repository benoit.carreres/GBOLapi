package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.UnsureBases;
import life.gbol.domain.UnsureBasesProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class UnsureBasesImpl extends AssemblyAnnotationImpl implements UnsureBases {
  public static final String TypeIRI = "http://gbol.life#UnsureBases";

  protected UnsureBasesImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static UnsureBases make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,UnsureBases.class);
      if(toRet == null) {
        toRet = new UnsureBasesImpl(domain,iri);;
      }
      else if(!(toRet instanceof UnsureBases)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.UnsureBasesImpl expected");
      }
      return (UnsureBases)toRet;
    }
  }

  public void validate() {
  }

  public String getReplace() {
    return this.getStringLit("http://gbol.life#replace",true);
  }

  public void setReplace(String val) {
    this.setStringLit("http://gbol.life#replace",val);
  }

  public String getCompare() {
    return this.getStringLit("http://gbol.life#compare",true);
  }

  public void setCompare(String val) {
    this.setStringLit("http://gbol.life#compare",val);
  }

  public UnsureBasesProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,UnsureBasesProvenance.class);
  }

  public void setProvenance(UnsureBasesProvenance val) {
    this.setRef("http://gbol.life#provenance",val,UnsureBasesProvenance.class);
  }
}
