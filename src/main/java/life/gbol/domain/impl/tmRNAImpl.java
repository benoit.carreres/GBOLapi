package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.tmRNA;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class tmRNAImpl extends MaturedRNAImpl implements tmRNA {
  public static final String TypeIRI = "http://gbol.life#tmRNA";

  protected tmRNAImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static tmRNA make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,tmRNA.class);
      if(toRet == null) {
        toRet = new tmRNAImpl(domain,iri);;
      }
      else if(!(toRet instanceof tmRNA)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.tmRNAImpl expected");
      }
      return (tmRNA)toRet;
    }
  }

  public void validate() {
  }

  public String getTag_peptide() {
    return this.getStringLit("http://gbol.life#tag_peptide",true);
  }

  public void setTag_peptide(String val) {
    this.setStringLit("http://gbol.life#tag_peptide",val);
  }

  public String getAnticodon() {
    return this.getStringLit("http://gbol.life#anticodon",true);
  }

  public void setAnticodon(String val) {
    this.setStringLit("http://gbol.life#anticodon",val);
  }
}
