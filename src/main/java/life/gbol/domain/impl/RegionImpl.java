package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Position;
import life.gbol.domain.Region;
import life.gbol.domain.StrandPosition;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class RegionImpl extends LocationImpl implements Region {
  public static final String TypeIRI = "http://gbol.life#Region";

  protected RegionImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Region make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Region.class);
      if(toRet == null) {
        toRet = new RegionImpl(domain,iri);;
      }
      else if(!(toRet instanceof Region)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.RegionImpl expected");
      }
      return (Region)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#strand");
    this.checkCardMin1("http://gbol.life#end");
    this.checkCardMin1("http://gbol.life#begin");
  }

  public StrandPosition getStrand() {
    return this.getEnum("http://gbol.life#strand",false,StrandPosition.class);
  }

  public void setStrand(StrandPosition val) {
    this.setEnum("http://gbol.life#strand",val,StrandPosition.class);
  }

  public Position getEnd() {
    return this.getRef("http://gbol.life#end",false,Position.class);
  }

  public void setEnd(Position val) {
    this.setRef("http://gbol.life#end",val,Position.class);
  }

  public Position getBegin() {
    return this.getRef("http://gbol.life#begin",false,Position.class);
  }

  public void setBegin(Position val) {
    this.setRef("http://gbol.life#begin",val,Position.class);
  }
}
