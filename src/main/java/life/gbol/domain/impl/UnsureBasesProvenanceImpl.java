package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.UnsureBasesProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class UnsureBasesProvenanceImpl extends AssemblyAnnotationProvenanceImpl implements UnsureBasesProvenance {
  public static final String TypeIRI = "http://gbol.life#UnsureBasesProvenance";

  protected UnsureBasesProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static UnsureBasesProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,UnsureBasesProvenance.class);
      if(toRet == null) {
        toRet = new UnsureBasesProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof UnsureBasesProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.UnsureBasesProvenanceImpl expected");
      }
      return (UnsureBasesProvenance)toRet;
    }
  }

  public void validate() {
  }
}
