package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.SequenceConstructionItem;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;

/**
 * Code generated from http://gbol.life# ontology
 */
public class SequenceConstructionItemImpl extends OWLThingImpl implements SequenceConstructionItem {
  public static final String TypeIRI = "http://gbol.life#SequenceConstructionItem";

  protected SequenceConstructionItemImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static SequenceConstructionItem make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,SequenceConstructionItem.class);
      if(toRet == null) {
        toRet = new SequenceConstructionItemImpl(domain,iri);;
      }
      else if(!(toRet instanceof SequenceConstructionItem)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.SequenceConstructionItemImpl expected");
      }
      return (SequenceConstructionItem)toRet;
    }
  }

  public void validate() {
  }
}
