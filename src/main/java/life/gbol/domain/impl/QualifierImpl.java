package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Qualifier;
import life.gbol.domain.QualifierProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;

/**
 * Code generated from http://gbol.life# ontology
 */
public class QualifierImpl extends OWLThingImpl implements Qualifier {
  public static final String TypeIRI = "http://gbol.life#Qualifier";

  protected QualifierImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Qualifier make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Qualifier.class);
      if(toRet == null) {
        toRet = new QualifierImpl(domain,iri);;
      }
      else if(!(toRet instanceof Qualifier)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.QualifierImpl expected");
      }
      return (Qualifier)toRet;
    }
  }

  public void validate() {
  }

  public QualifierProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,QualifierProvenance.class);
  }

  public void setProvenance(QualifierProvenance val) {
    this.setRef("http://gbol.life#provenance",val,QualifierProvenance.class);
  }
}
