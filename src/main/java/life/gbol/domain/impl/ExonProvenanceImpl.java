package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.ExonProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ExonProvenanceImpl extends GeneticElementProvenanceImpl implements ExonProvenance {
  public static final String TypeIRI = "http://gbol.life#ExonProvenance";

  protected ExonProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static ExonProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,ExonProvenance.class);
      if(toRet == null) {
        toRet = new ExonProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof ExonProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ExonProvenanceImpl expected");
      }
      return (ExonProvenance)toRet;
    }
  }

  public void validate() {
  }
}
