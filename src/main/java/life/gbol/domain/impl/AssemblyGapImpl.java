package life.gbol.domain.impl;

import java.lang.Integer;
import java.lang.String;
import life.gbol.domain.AssemblyGap;
import life.gbol.domain.AssemblyGapProvenance;
import life.gbol.domain.GapType;
import life.gbol.domain.LinkageEvidence;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class AssemblyGapImpl extends AssemblyAnnotationImpl implements AssemblyGap {
  public static final String TypeIRI = "http://gbol.life#AssemblyGap";

  protected AssemblyGapImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static AssemblyGap make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,AssemblyGap.class);
      if(toRet == null) {
        toRet = new AssemblyGapImpl(domain,iri);;
      }
      else if(!(toRet instanceof AssemblyGap)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.AssemblyGapImpl expected");
      }
      return (AssemblyGap)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#gap_type");
    this.checkCardMin1("http://gbol.life#estimated_length");
  }

  public GapType getGap_type() {
    return this.getEnum("http://gbol.life#gap_type",false,GapType.class);
  }

  public void setGap_type(GapType val) {
    this.setEnum("http://gbol.life#gap_type",val,GapType.class);
  }

  public LinkageEvidence getLinkage_evidence() {
    return this.getEnum("http://gbol.life#linkage_evidence",true,LinkageEvidence.class);
  }

  public void setLinkage_evidence(LinkageEvidence val) {
    this.setEnum("http://gbol.life#linkage_evidence",val,LinkageEvidence.class);
  }

  public Integer getEstimated_length() {
    return this.getIntegerLit("http://gbol.life#estimated_length",false);
  }

  public void setEstimated_length(Integer val) {
    this.setIntegerLit("http://gbol.life#estimated_length",val);
  }

  public AssemblyGapProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,AssemblyGapProvenance.class);
  }

  public void setProvenance(AssemblyGapProvenance val) {
    this.setRef("http://gbol.life#provenance",val,AssemblyGapProvenance.class);
  }
}
