package life.gbol.domain.impl;

import java.lang.Integer;
import java.lang.String;
import life.gbol.domain.InRangePosition;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class InRangePositionImpl extends FuzzyPositionImpl implements InRangePosition {
  public static final String TypeIRI = "http://gbol.life#InRangePosition";

  protected InRangePositionImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static InRangePosition make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,InRangePosition.class);
      if(toRet == null) {
        toRet = new InRangePositionImpl(domain,iri);;
      }
      else if(!(toRet instanceof InRangePosition)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.InRangePositionImpl expected");
      }
      return (InRangePosition)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#end");
    this.checkCardMin1("http://gbol.life#begin");
  }

  public Integer getEnd() {
    return this.getIntegerLit("http://gbol.life#end",false);
  }

  public void setEnd(Integer val) {
    this.setIntegerLit("http://gbol.life#end",val);
  }

  public Integer getBegin() {
    return this.getIntegerLit("http://gbol.life#begin",false);
  }

  public void setBegin(Integer val) {
    this.setIntegerLit("http://gbol.life#begin",val);
  }
}
