package life.gbol.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.Contig;
import life.gbol.domain.Sample;
import life.gbol.domain.SequenceObject;
import life.gbol.domain.XRef;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;

/**
 * Code generated from http://gbol.life# ontology
 */
public class SequenceObjectImpl extends OWLThingImpl implements SequenceObject {
  public static final String TypeIRI = "http://gbol.life#SequenceObject";

  protected SequenceObjectImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static SequenceObject make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,SequenceObject.class);
      if(toRet == null) {
        toRet = new SequenceObjectImpl(domain,iri);;
      }
      else if(!(toRet instanceof SequenceObject)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.SequenceObjectImpl expected");
      }
      return (SequenceObject)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#sequence");
    this.checkCardMin1("http://gbol.life#sha384");
  }

  public void remNote(String val) {
    this.remStringLit("http://gbol.life#note",val,true);
  }

  public List<? extends String> getAllNote() {
    return this.getStringLitSet("http://gbol.life#note",true);
  }

  public void addNote(String val) {
    this.addStringLit("http://gbol.life#note",val);
  }

  public String getSequence() {
    return this.getStringLit("http://gbol.life#sequence",false);
  }

  public void setSequence(String val) {
    this.setStringLit("http://gbol.life#sequence",val);
  }

  public void remXref(XRef val) {
    this.remRef("http://gbol.life#xref",val,true);
  }

  public List<? extends XRef> getAllXref() {
    return this.getRefSet("http://gbol.life#xref",true,XRef.class);
  }

  public void addXref(XRef val) {
    this.addRef("http://gbol.life#xref",val);
  }

  public String getGiaccession() {
    return this.getStringLit("http://gbol.life#giaccession",true);
  }

  public void setGiaccession(String val) {
    this.setStringLit("http://gbol.life#giaccession",val);
  }

  public Contig getContig() {
    return this.getRef("http://gbol.life#contig",true,Contig.class);
  }

  public void setContig(Contig val) {
    this.setRef("http://gbol.life#contig",val,Contig.class);
  }

  public String getSha384() {
    return this.getStringLit("http://gbol.life#sha384",false);
  }

  public void setSha384(String val) {
    this.setStringLit("http://gbol.life#sha384",val);
  }

  public Sample getSample() {
    return this.getRef("http://gbol.life#sample",true,Sample.class);
  }

  public void setSample(Sample val) {
    this.setRef("http://gbol.life#sample",val,Sample.class);
  }

  public String getProduct() {
    return this.getStringLit("http://gbol.life#product",true);
  }

  public void setProduct(String val) {
    this.setStringLit("http://gbol.life#product",val);
  }

  public String getInference() {
    return this.getStringLit("http://gbol.life#inference",true);
  }

  public void setInference(String val) {
    this.setStringLit("http://gbol.life#inference",val);
  }
}
