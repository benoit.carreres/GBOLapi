package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.PolyASignalProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class PolyASignalProvenanceImpl extends RegulationSiteProvenanceImpl implements PolyASignalProvenance {
  public static final String TypeIRI = "http://gbol.life#PolyASignalProvenance";

  protected PolyASignalProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static PolyASignalProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,PolyASignalProvenance.class);
      if(toRet == null) {
        toRet = new PolyASignalProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof PolyASignalProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.PolyASignalProvenanceImpl expected");
      }
      return (PolyASignalProvenance)toRet;
    }
  }

  public void validate() {
  }
}
