package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.ItemGapUnknown;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ItemGapUnknownImpl extends SequenceConstructionItemImpl implements ItemGapUnknown {
  public static final String TypeIRI = "http://gbol.life#ItemGapUnknown";

  protected ItemGapUnknownImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static ItemGapUnknown make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,ItemGapUnknown.class);
      if(toRet == null) {
        toRet = new ItemGapUnknownImpl(domain,iri);;
      }
      else if(!(toRet instanceof ItemGapUnknown)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ItemGapUnknownImpl expected");
      }
      return (ItemGapUnknown)toRet;
    }
  }

  public void validate() {
  }
}
