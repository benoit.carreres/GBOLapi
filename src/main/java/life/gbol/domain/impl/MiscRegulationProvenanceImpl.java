package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.MiscRegulationProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class MiscRegulationProvenanceImpl extends RegulationSiteProvenanceImpl implements MiscRegulationProvenance {
  public static final String TypeIRI = "http://gbol.life#MiscRegulationProvenance";

  protected MiscRegulationProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static MiscRegulationProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,MiscRegulationProvenance.class);
      if(toRet == null) {
        toRet = new MiscRegulationProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof MiscRegulationProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.MiscRegulationProvenanceImpl expected");
      }
      return (MiscRegulationProvenance)toRet;
    }
  }

  public void validate() {
  }
}
