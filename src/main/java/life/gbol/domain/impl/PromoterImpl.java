package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Promoter;
import life.gbol.domain.PromoterProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class PromoterImpl extends RegulationSiteImpl implements Promoter {
  public static final String TypeIRI = "http://gbol.life#Promoter";

  protected PromoterImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Promoter make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Promoter.class);
      if(toRet == null) {
        toRet = new PromoterImpl(domain,iri);;
      }
      else if(!(toRet instanceof Promoter)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.PromoterImpl expected");
      }
      return (Promoter)toRet;
    }
  }

  public void validate() {
  }

  public PromoterProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,PromoterProvenance.class);
  }

  public void setProvenance(PromoterProvenance val) {
    this.setRef("http://gbol.life#provenance",val,PromoterProvenance.class);
  }
}
