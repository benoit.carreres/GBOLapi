package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.CDSProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class CDSProvenanceImpl extends TranscriptFeatureProvenanceImpl implements CDSProvenance {
  public static final String TypeIRI = "http://gbol.life#CDSProvenance";

  protected CDSProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static CDSProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,CDSProvenance.class);
      if(toRet == null) {
        toRet = new CDSProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof CDSProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.CDSProvenanceImpl expected");
      }
      return (CDSProvenance)toRet;
    }
  }

  public void validate() {
  }
}
