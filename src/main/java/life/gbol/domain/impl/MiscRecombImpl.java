package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.MiscRecomb;
import life.gbol.domain.MiscRecombProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class MiscRecombImpl extends BiologicalRecognizedRegionImpl implements MiscRecomb {
  public static final String TypeIRI = "http://gbol.life#MiscRecomb";

  protected MiscRecombImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static MiscRecomb make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,MiscRecomb.class);
      if(toRet == null) {
        toRet = new MiscRecombImpl(domain,iri);;
      }
      else if(!(toRet instanceof MiscRecomb)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.MiscRecombImpl expected");
      }
      return (MiscRecomb)toRet;
    }
  }

  public void validate() {
  }

  public MiscRecombProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,MiscRecombProvenance.class);
  }

  public void setProvenance(MiscRecombProvenance val) {
    this.setRef("http://gbol.life#provenance",val,MiscRecombProvenance.class);
  }
}
