package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.StructureFeature;
import life.gbol.domain.StructureFeatureProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class StructureFeatureImpl extends NonGeneRelatedElementsImpl implements StructureFeature {
  public static final String TypeIRI = "http://gbol.life#StructureFeature";

  protected StructureFeatureImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static StructureFeature make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,StructureFeature.class);
      if(toRet == null) {
        toRet = new StructureFeatureImpl(domain,iri);;
      }
      else if(!(toRet instanceof StructureFeature)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.StructureFeatureImpl expected");
      }
      return (StructureFeature)toRet;
    }
  }

  public void validate() {
  }

  public StructureFeatureProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,StructureFeatureProvenance.class);
  }

  public void setProvenance(StructureFeatureProvenance val) {
    this.setRef("http://gbol.life#provenance",val,StructureFeatureProvenance.class);
  }
}
