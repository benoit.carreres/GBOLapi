package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.FivePrimeUTRProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class FivePrimeUTRProvenanceImpl extends TranscriptFeatureProvenanceImpl implements FivePrimeUTRProvenance {
  public static final String TypeIRI = "http://gbol.life#FivePrimeUTRProvenance";

  protected FivePrimeUTRProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static FivePrimeUTRProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,FivePrimeUTRProvenance.class);
      if(toRet == null) {
        toRet = new FivePrimeUTRProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof FivePrimeUTRProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.FivePrimeUTRProvenanceImpl expected");
      }
      return (FivePrimeUTRProvenance)toRet;
    }
  }

  public void validate() {
  }
}
