package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.PolyASignal;
import life.gbol.domain.PolyASignalProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class PolyASignalImpl extends RegulationSiteImpl implements PolyASignal {
  public static final String TypeIRI = "http://gbol.life#PolyASignal";

  protected PolyASignalImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static PolyASignal make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,PolyASignal.class);
      if(toRet == null) {
        toRet = new PolyASignalImpl(domain,iri);;
      }
      else if(!(toRet instanceof PolyASignal)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.PolyASignalImpl expected");
      }
      return (PolyASignal)toRet;
    }
  }

  public void validate() {
  }

  public PolyASignalProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,PolyASignalProvenance.class);
  }

  public void setProvenance(PolyASignalProvenance val) {
    this.setRef("http://gbol.life#provenance",val,PolyASignalProvenance.class);
  }
}
