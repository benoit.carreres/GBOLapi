package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.TelomereProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class TelomereProvenanceImpl extends RepeatFeatureProvenanceImpl implements TelomereProvenance {
  public static final String TypeIRI = "http://gbol.life#TelomereProvenance";

  protected TelomereProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static TelomereProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,TelomereProvenance.class);
      if(toRet == null) {
        toRet = new TelomereProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof TelomereProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.TelomereProvenanceImpl expected");
      }
      return (TelomereProvenance)toRet;
    }
  }

  public void validate() {
  }
}
