package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.PrimerBinding;
import life.gbol.domain.PrimerBindingProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class PrimerBindingImpl extends ArtificialRecognizedRegionImpl implements PrimerBinding {
  public static final String TypeIRI = "http://gbol.life#PrimerBinding";

  protected PrimerBindingImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static PrimerBinding make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,PrimerBinding.class);
      if(toRet == null) {
        toRet = new PrimerBindingImpl(domain,iri);;
      }
      else if(!(toRet instanceof PrimerBinding)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.PrimerBindingImpl expected");
      }
      return (PrimerBinding)toRet;
    }
  }

  public void validate() {
  }

  public PrimerBindingProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,PrimerBindingProvenance.class);
  }

  public void setProvenance(PrimerBindingProvenance val) {
    this.setRef("http://gbol.life#provenance",val,PrimerBindingProvenance.class);
  }
}
