package life.gbol.domain.impl;

import java.lang.Boolean;
import java.lang.Double;
import java.lang.Integer;
import java.lang.String;
import java.time.LocalDate;
import java.util.List;
import life.gbol.domain.PublishedDataset;
import life.gbol.domain.Sample;
import life.gbol.domain.XRef;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;

/**
 * Code generated from http://gbol.life# ontology
 */
public class SampleImpl extends OWLThingImpl implements Sample {
  public static final String TypeIRI = "http://gbol.life#Sample";

  protected SampleImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Sample make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Sample.class);
      if(toRet == null) {
        toRet = new SampleImpl(domain,iri);;
      }
      else if(!(toRet instanceof Sample)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.SampleImpl expected");
      }
      return (Sample)toRet;
    }
  }

  public void validate() {
  }

  public Boolean getFocus() {
    return this.getBooleanLit("http://gbol.life#focus",true);
  }

  public void setFocus(Boolean val) {
    this.setBooleanLit("http://gbol.life#focus",val);
  }

  public String getSerovar() {
    return this.getStringLit("http://gbol.life#serovar",true);
  }

  public void setSerovar(String val) {
    this.setStringLit("http://gbol.life#serovar",val);
  }

  public String getLat_lon() {
    return this.getStringLit("http://gbol.life#lat_lon",true);
  }

  public void setLat_lon(String val) {
    this.setStringLit("http://gbol.life#lat_lon",val);
  }

  public String getEcotype() {
    return this.getStringLit("http://gbol.life#ecotype",true);
  }

  public void setEcotype(String val) {
    this.setStringLit("http://gbol.life#ecotype",val);
  }

  public void remXref(XRef val) {
    this.remRef("http://gbol.life#xref",val,true);
  }

  public List<? extends XRef> getAllXref() {
    return this.getRefSet("http://gbol.life#xref",true,XRef.class);
  }

  public void addXref(XRef val) {
    this.addRef("http://gbol.life#xref",val);
  }

  public Boolean getTransgenic() {
    return this.getBooleanLit("http://gbol.life#transgenic",true);
  }

  public void setTransgenic(Boolean val) {
    this.setBooleanLit("http://gbol.life#transgenic",val);
  }

  public String getClone_lib() {
    return this.getStringLit("http://gbol.life#clone_lib",true);
  }

  public void setClone_lib(String val) {
    this.setStringLit("http://gbol.life#clone_lib",val);
  }

  public String getIsolate() {
    return this.getStringLit("http://gbol.life#isolate",true);
  }

  public void setIsolate(String val) {
    this.setStringLit("http://gbol.life#isolate",val);
  }

  public String getCultivar() {
    return this.getStringLit("http://gbol.life#cultivar",true);
  }

  public void setCultivar(String val) {
    this.setStringLit("http://gbol.life#cultivar",val);
  }

  public Boolean getGermline() {
    return this.getBooleanLit("http://gbol.life#germline",true);
  }

  public void setGermline(Boolean val) {
    this.setBooleanLit("http://gbol.life#germline",val);
  }

  public LocalDate getHolddate() {
    return this.getDateLit("http://gbol.life#holddate",true);
  }

  public void setHolddate(LocalDate val) {
    this.setDateLit("http://gbol.life#holddate",val);
  }

  public LocalDate getCollection_date() {
    return this.getDateLit("http://gbol.life#collection_date",true);
  }

  public void setCollection_date(LocalDate val) {
    this.setDateLit("http://gbol.life#collection_date",val);
  }

  public String getSerotype() {
    return this.getStringLit("http://gbol.life#serotype",true);
  }

  public void setSerotype(String val) {
    this.setStringLit("http://gbol.life#serotype",val);
  }

  public String getLab_host() {
    return this.getStringLit("http://gbol.life#lab_host",true);
  }

  public void setLab_host(String val) {
    this.setStringLit("http://gbol.life#lab_host",val);
  }

  public Boolean getMacronuclear() {
    return this.getBooleanLit("http://gbol.life#macronuclear",true);
  }

  public void setMacronuclear(Boolean val) {
    this.setBooleanLit("http://gbol.life#macronuclear",val);
  }

  public Boolean getProviral() {
    return this.getBooleanLit("http://gbol.life#proviral",true);
  }

  public void setProviral(Boolean val) {
    this.setBooleanLit("http://gbol.life#proviral",val);
  }

  public String getVariety() {
    return this.getStringLit("http://gbol.life#variety",true);
  }

  public void setVariety(String val) {
    this.setStringLit("http://gbol.life#variety",val);
  }

  public String getDev_stage() {
    return this.getStringLit("http://gbol.life#dev_stage",true);
  }

  public void setDev_stage(String val) {
    this.setStringLit("http://gbol.life#dev_stage",val);
  }

  public String getCountry() {
    return this.getStringLit("http://gbol.life#country",true);
  }

  public void setCountry(String val) {
    this.setStringLit("http://gbol.life#country",val);
  }

  public LocalDate getFirst_public() {
    return this.getDateLit("http://gbol.life#first_public",true);
  }

  public void setFirst_public(LocalDate val) {
    this.setDateLit("http://gbol.life#first_public",val);
  }

  public String getTissue_lib() {
    return this.getStringLit("http://gbol.life#tissue_lib",true);
  }

  public void setTissue_lib(String val) {
    this.setStringLit("http://gbol.life#tissue_lib",val);
  }

  public String getCollected_by() {
    return this.getStringLit("http://gbol.life#collected_by",true);
  }

  public void setCollected_by(String val) {
    this.setStringLit("http://gbol.life#collected_by",val);
  }

  public Integer getFirst_public_release() {
    return this.getIntegerLit("http://gbol.life#first_public_release",true);
  }

  public void setFirst_public_release(Integer val) {
    this.setIntegerLit("http://gbol.life#first_public_release",val);
  }

  public String getCell_type() {
    return this.getStringLit("http://gbol.life#cell_type",true);
  }

  public void setCell_type(String val) {
    this.setStringLit("http://gbol.life#cell_type",val);
  }

  public String getPCR_primers() {
    return this.getStringLit("http://gbol.life#PCR_primers",true);
  }

  public void setPCR_primers(String val) {
    this.setStringLit("http://gbol.life#PCR_primers",val);
  }

  public String getCulture_collection() {
    return this.getStringLit("http://gbol.life#culture_collection",true);
  }

  public void setCulture_collection(String val) {
    this.setStringLit("http://gbol.life#culture_collection",val);
  }

  public LocalDate getLast_updated() {
    return this.getDateLit("http://gbol.life#last_updated",true);
  }

  public void setLast_updated(LocalDate val) {
    this.setDateLit("http://gbol.life#last_updated",val);
  }

  public String getBio_material() {
    return this.getStringLit("http://gbol.life#bio_material",true);
  }

  public void setBio_material(String val) {
    this.setStringLit("http://gbol.life#bio_material",val);
  }

  public Integer getLast_updated_release() {
    return this.getIntegerLit("http://gbol.life#last_updated_release",true);
  }

  public void setLast_updated_release(Integer val) {
    this.setIntegerLit("http://gbol.life#last_updated_release",val);
  }

  public String getSegment() {
    return this.getStringLit("http://gbol.life#segment",true);
  }

  public void setSegment(String val) {
    this.setStringLit("http://gbol.life#segment",val);
  }

  public String getIdentified_by() {
    return this.getStringLit("http://gbol.life#identified_by",true);
  }

  public void setIdentified_by(String val) {
    this.setStringLit("http://gbol.life#identified_by",val);
  }

  public Boolean getRearranged() {
    return this.getBooleanLit("http://gbol.life#rearranged",true);
  }

  public void setRearranged(Boolean val) {
    this.setBooleanLit("http://gbol.life#rearranged",val);
  }

  public String getPop_variant() {
    return this.getStringLit("http://gbol.life#pop_variant",true);
  }

  public void setPop_variant(String val) {
    this.setStringLit("http://gbol.life#pop_variant",val);
  }

  public String getClone() {
    return this.getStringLit("http://gbol.life#clone",true);
  }

  public void setClone(String val) {
    this.setStringLit("http://gbol.life#clone",val);
  }

  public String getSex() {
    return this.getStringLit("http://gbol.life#sex",true);
  }

  public void setSex(String val) {
    this.setStringLit("http://gbol.life#sex",val);
  }

  public String getTissue_type() {
    return this.getStringLit("http://gbol.life#tissue_type",true);
  }

  public void setTissue_type(String val) {
    this.setStringLit("http://gbol.life#tissue_type",val);
  }

  public String getType_material() {
    return this.getStringLit("http://gbol.life#type_material",true);
  }

  public void setType_material(String val) {
    this.setStringLit("http://gbol.life#type_material",val);
  }

  public Boolean getEnvironmental_sample() {
    return this.getBooleanLit("http://gbol.life#environmental_sample",true);
  }

  public void setEnvironmental_sample(Boolean val) {
    this.setBooleanLit("http://gbol.life#environmental_sample",val);
  }

  public String getHaplogroup() {
    return this.getStringLit("http://gbol.life#haplogroup",true);
  }

  public void setHaplogroup(String val) {
    this.setStringLit("http://gbol.life#haplogroup",val);
  }

  public String getHaplotype() {
    return this.getStringLit("http://gbol.life#haplotype",true);
  }

  public void setHaplotype(String val) {
    this.setStringLit("http://gbol.life#haplotype",val);
  }

  public void remKeywords(String val) {
    this.remStringLit("http://gbol.life#keywords",val,true);
  }

  public List<? extends String> getAllKeywords() {
    return this.getStringLitSet("http://gbol.life#keywords",true);
  }

  public void addKeywords(String val) {
    this.addStringLit("http://gbol.life#keywords",val);
  }

  public LocalDate getFfdate() {
    return this.getDateLit("http://gbol.life#ffdate",true);
  }

  public void setFfdate(LocalDate val) {
    this.setDateLit("http://gbol.life#ffdate",val);
  }

  public Double getAltitude() {
    return this.getDoubleLit("http://gbol.life#altitude",true);
  }

  public void setAltitude(Double val) {
    this.setDoubleLit("http://gbol.life#altitude",val);
  }

  public String getIsolation_source() {
    return this.getStringLit("http://gbol.life#isolation_source",true);
  }

  public void setIsolation_source(String val) {
    this.setStringLit("http://gbol.life#isolation_source",val);
  }

  public String getHost() {
    return this.getStringLit("http://gbol.life#host",true);
  }

  public void setHost(String val) {
    this.setStringLit("http://gbol.life#host",val);
  }

  public void remPublisheddataset(PublishedDataset val) {
    this.remRef("http://gbol.life#publisheddataset",val,true);
  }

  public List<? extends PublishedDataset> getAllPublisheddataset() {
    return this.getRefSet("http://gbol.life#publisheddataset",true,PublishedDataset.class);
  }

  public void addPublisheddataset(PublishedDataset val) {
    this.addRef("http://gbol.life#publisheddataset",val);
  }

  public String getCell_line() {
    return this.getStringLit("http://gbol.life#cell_line",true);
  }

  public void setCell_line(String val) {
    this.setStringLit("http://gbol.life#cell_line",val);
  }

  public String getMating_type() {
    return this.getStringLit("http://gbol.life#mating_type",true);
  }

  public void setMating_type(String val) {
    this.setStringLit("http://gbol.life#mating_type",val);
  }
}
