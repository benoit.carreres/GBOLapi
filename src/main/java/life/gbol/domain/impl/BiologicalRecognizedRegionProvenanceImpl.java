package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.BiologicalRecognizedRegionProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class BiologicalRecognizedRegionProvenanceImpl extends RecognizedRegionProvenanceImpl implements BiologicalRecognizedRegionProvenance {
  public static final String TypeIRI = "http://gbol.life#BiologicalRecognizedRegionProvenance";

  protected BiologicalRecognizedRegionProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static BiologicalRecognizedRegionProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,BiologicalRecognizedRegionProvenance.class);
      if(toRet == null) {
        toRet = new BiologicalRecognizedRegionProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof BiologicalRecognizedRegionProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.BiologicalRecognizedRegionProvenanceImpl expected");
      }
      return (BiologicalRecognizedRegionProvenance)toRet;
    }
  }

  public void validate() {
  }
}
