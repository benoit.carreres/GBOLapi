package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.MobileElementProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class MobileElementProvenanceImpl extends RepeatFeatureProvenanceImpl implements MobileElementProvenance {
  public static final String TypeIRI = "http://gbol.life#MobileElementProvenance";

  protected MobileElementProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static MobileElementProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,MobileElementProvenance.class);
      if(toRet == null) {
        toRet = new MobileElementProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof MobileElementProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.MobileElementProvenanceImpl expected");
      }
      return (MobileElementProvenance)toRet;
    }
  }

  public void validate() {
  }
}
