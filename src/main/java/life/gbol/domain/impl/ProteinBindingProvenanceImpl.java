package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.ProteinBindingProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ProteinBindingProvenanceImpl extends BiologicalRecognizedRegionProvenanceImpl implements ProteinBindingProvenance {
  public static final String TypeIRI = "http://gbol.life#ProteinBindingProvenance";

  protected ProteinBindingProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static ProteinBindingProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,ProteinBindingProvenance.class);
      if(toRet == null) {
        toRet = new ProteinBindingProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof ProteinBindingProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ProteinBindingProvenanceImpl expected");
      }
      return (ProteinBindingProvenance)toRet;
    }
  }

  public void validate() {
  }
}
