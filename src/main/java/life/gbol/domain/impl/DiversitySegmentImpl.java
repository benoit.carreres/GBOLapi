package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.DiversitySegment;
import life.gbol.domain.DiversitySegmentProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class DiversitySegmentImpl extends ImmunoglobulinFeatureImpl implements DiversitySegment {
  public static final String TypeIRI = "http://gbol.life#DiversitySegment";

  protected DiversitySegmentImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static DiversitySegment make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,DiversitySegment.class);
      if(toRet == null) {
        toRet = new DiversitySegmentImpl(domain,iri);;
      }
      else if(!(toRet instanceof DiversitySegment)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.DiversitySegmentImpl expected");
      }
      return (DiversitySegment)toRet;
    }
  }

  public void validate() {
  }

  public DiversitySegmentProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,DiversitySegmentProvenance.class);
  }

  public void setProvenance(DiversitySegmentProvenance val) {
    this.setRef("http://gbol.life#provenance",val,DiversitySegmentProvenance.class);
  }
}
