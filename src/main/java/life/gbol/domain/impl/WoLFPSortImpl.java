package life.gbol.domain.impl;

import java.lang.Double;
import java.lang.String;
import life.gbol.domain.Compartments;
import life.gbol.domain.WoLFPSort;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class WoLFPSortImpl extends ProteinFeatureProvenanceImpl implements WoLFPSort {
  public static final String TypeIRI = "http://gbol.life#WoLFPSort";

  protected WoLFPSortImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static WoLFPSort make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,WoLFPSort.class);
      if(toRet == null) {
        toRet = new WoLFPSortImpl(domain,iri);;
      }
      else if(!(toRet instanceof WoLFPSort)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.WoLFPSortImpl expected");
      }
      return (WoLFPSort)toRet;
    }
  }

  public void validate() {
  }

  public Double getScore() {
    return this.getDoubleLit("http://gbol.life#score",true);
  }

  public void setScore(Double val) {
    this.setDoubleLit("http://gbol.life#score",val);
  }

  public Compartments getCompartment() {
    return this.getEnum("http://gbol.life#compartment",true,Compartments.class);
  }

  public void setCompartment(Compartments val) {
    this.setEnum("http://gbol.life#compartment",val,Compartments.class);
  }
}
