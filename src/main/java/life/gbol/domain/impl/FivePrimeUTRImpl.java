package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.FivePrimeUTR;
import life.gbol.domain.FivePrimeUTRProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class FivePrimeUTRImpl extends TranscriptFeatureImpl implements FivePrimeUTR {
  public static final String TypeIRI = "http://gbol.life#FivePrimeUTR";

  protected FivePrimeUTRImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static FivePrimeUTR make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,FivePrimeUTR.class);
      if(toRet == null) {
        toRet = new FivePrimeUTRImpl(domain,iri);;
      }
      else if(!(toRet instanceof FivePrimeUTR)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.FivePrimeUTRImpl expected");
      }
      return (FivePrimeUTR)toRet;
    }
  }

  public void validate() {
  }

  public FivePrimeUTRProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,FivePrimeUTRProvenance.class);
  }

  public void setProvenance(FivePrimeUTRProvenance val) {
    this.setRef("http://gbol.life#provenance",val,FivePrimeUTRProvenance.class);
  }
}
