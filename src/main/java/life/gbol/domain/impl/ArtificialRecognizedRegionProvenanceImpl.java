package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.ArtificialRecognizedRegionProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ArtificialRecognizedRegionProvenanceImpl extends RecognizedRegionProvenanceImpl implements ArtificialRecognizedRegionProvenance {
  public static final String TypeIRI = "http://gbol.life#ArtificialRecognizedRegionProvenance";

  protected ArtificialRecognizedRegionProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static ArtificialRecognizedRegionProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,ArtificialRecognizedRegionProvenance.class);
      if(toRet == null) {
        toRet = new ArtificialRecognizedRegionProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof ArtificialRecognizedRegionProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ArtificialRecognizedRegionProvenanceImpl expected");
      }
      return (ArtificialRecognizedRegionProvenance)toRet;
    }
  }

  public void validate() {
  }
}
