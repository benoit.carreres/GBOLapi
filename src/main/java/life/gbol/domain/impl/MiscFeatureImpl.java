package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.MiscFeature;
import life.gbol.domain.MiscFeatureProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class MiscFeatureImpl extends NAFeatureImpl implements MiscFeature {
  public static final String TypeIRI = "http://gbol.life#MiscFeature";

  protected MiscFeatureImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static MiscFeature make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,MiscFeature.class);
      if(toRet == null) {
        toRet = new MiscFeatureImpl(domain,iri);;
      }
      else if(!(toRet instanceof MiscFeature)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.MiscFeatureImpl expected");
      }
      return (MiscFeature)toRet;
    }
  }

  public void validate() {
  }

  public MiscFeatureProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,MiscFeatureProvenance.class);
  }

  public void setProvenance(MiscFeatureProvenance val) {
    this.setRef("http://gbol.life#provenance",val,MiscFeatureProvenance.class);
  }
}
