package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.SequenceAnnotation;
import life.gbol.domain.SequenceAnnotationProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class SequenceAnnotationImpl extends NAFeatureImpl implements SequenceAnnotation {
  public static final String TypeIRI = "http://gbol.life#SequenceAnnotation";

  protected SequenceAnnotationImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static SequenceAnnotation make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,SequenceAnnotation.class);
      if(toRet == null) {
        toRet = new SequenceAnnotationImpl(domain,iri);;
      }
      else if(!(toRet instanceof SequenceAnnotation)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.SequenceAnnotationImpl expected");
      }
      return (SequenceAnnotation)toRet;
    }
  }

  public void validate() {
  }

  public SequenceAnnotationProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,SequenceAnnotationProvenance.class);
  }

  public void setProvenance(SequenceAnnotationProvenance val) {
    this.setRef("http://gbol.life#provenance",val,SequenceAnnotationProvenance.class);
  }
}
