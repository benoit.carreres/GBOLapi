package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.VariableRegionProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class VariableRegionProvenanceImpl extends ImmunoglobulinFeatureProvenanceImpl implements VariableRegionProvenance {
  public static final String TypeIRI = "http://gbol.life#VariableRegionProvenance";

  protected VariableRegionProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static VariableRegionProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,VariableRegionProvenance.class);
      if(toRet == null) {
        toRet = new VariableRegionProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof VariableRegionProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.VariableRegionProvenanceImpl expected");
      }
      return (VariableRegionProvenance)toRet;
    }
  }

  public void validate() {
  }
}
