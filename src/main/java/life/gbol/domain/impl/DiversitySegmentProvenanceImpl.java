package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.DiversitySegmentProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class DiversitySegmentProvenanceImpl extends ImmunoglobulinFeatureProvenanceImpl implements DiversitySegmentProvenance {
  public static final String TypeIRI = "http://gbol.life#DiversitySegmentProvenance";

  protected DiversitySegmentProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static DiversitySegmentProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,DiversitySegmentProvenance.class);
      if(toRet == null) {
        toRet = new DiversitySegmentProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof DiversitySegmentProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.DiversitySegmentProvenanceImpl expected");
      }
      return (DiversitySegmentProvenance)toRet;
    }
  }

  public void validate() {
  }
}
