package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Operon;
import life.gbol.domain.OperonProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class OperonImpl extends GeneticElementImpl implements Operon {
  public static final String TypeIRI = "http://gbol.life#Operon";

  protected OperonImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Operon make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Operon.class);
      if(toRet == null) {
        toRet = new OperonImpl(domain,iri);;
      }
      else if(!(toRet instanceof Operon)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.OperonImpl expected");
      }
      return (Operon)toRet;
    }
  }

  public void validate() {
  }

  public OperonProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,OperonProvenance.class);
  }

  public void setProvenance(OperonProvenance val) {
    this.setRef("http://gbol.life#provenance",val,OperonProvenance.class);
  }
}
