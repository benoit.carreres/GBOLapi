package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.ReplicationOriginProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ReplicationOriginProvenanceImpl extends BiologicalRecognizedRegionProvenanceImpl implements ReplicationOriginProvenance {
  public static final String TypeIRI = "http://gbol.life#ReplicationOriginProvenance";

  protected ReplicationOriginProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static ReplicationOriginProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,ReplicationOriginProvenance.class);
      if(toRet == null) {
        toRet = new ReplicationOriginProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof ReplicationOriginProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ReplicationOriginProvenanceImpl expected");
      }
      return (ReplicationOriginProvenance)toRet;
    }
  }

  public void validate() {
  }
}
