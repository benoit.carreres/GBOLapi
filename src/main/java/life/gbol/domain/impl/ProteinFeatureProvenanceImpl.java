package life.gbol.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.Location;
import life.gbol.domain.ProteinFeatureProvenance;
import life.gbol.domain.XRef;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ProteinFeatureProvenanceImpl extends FeatureProvenanceImpl implements ProteinFeatureProvenance {
  public static final String TypeIRI = "http://gbol.life#ProteinFeatureProvenance";

  protected ProteinFeatureProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static ProteinFeatureProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,ProteinFeatureProvenance.class);
      if(toRet == null) {
        toRet = new ProteinFeatureProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof ProteinFeatureProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ProteinFeatureProvenanceImpl expected");
      }
      return (ProteinFeatureProvenance)toRet;
    }
  }

  public void validate() {
  }

  public void remXref(XRef val) {
    this.remRef("http://gbol.life#xref",val,true);
  }

  public List<? extends XRef> getAllXref() {
    return this.getRefSet("http://gbol.life#xref",true,XRef.class);
  }

  public void addXref(XRef val) {
    this.addRef("http://gbol.life#xref",val);
  }

  public Location getLocation() {
    return this.getRef("http://gbol.life#location",true,Location.class);
  }

  public void setLocation(Location val) {
    this.setRef("http://gbol.life#location",val,Location.class);
  }
}
