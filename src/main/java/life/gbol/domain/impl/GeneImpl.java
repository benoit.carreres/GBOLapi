package life.gbol.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.Exon;
import life.gbol.domain.Gene;
import life.gbol.domain.GeneProvenance;
import life.gbol.domain.Intron;
import life.gbol.domain.MiscRna;
import life.gbol.domain.Transcript;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class GeneImpl extends GeneticElementImpl implements Gene {
  public static final String TypeIRI = "http://gbol.life#Gene";

  protected GeneImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Gene make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Gene.class);
      if(toRet == null) {
        toRet = new GeneImpl(domain,iri);;
      }
      else if(!(toRet instanceof Gene)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.GeneImpl expected");
      }
      return (Gene)toRet;
    }
  }

  public void validate() {
  }

  public void remExon(Exon val) {
    this.remRef("http://gbol.life#exon",val,true);
  }

  public List<? extends Exon> getAllExon() {
    return this.getRefSet("http://gbol.life#exon",true,Exon.class);
  }

  public void addExon(Exon val) {
    this.addRef("http://gbol.life#exon",val);
  }

  public void remTranscript(Transcript val) {
    this.remRef("http://gbol.life#transcript",val,true);
  }

  public List<? extends Transcript> getAllTranscript() {
    return this.getRefSet("http://gbol.life#transcript",true,Transcript.class);
  }

  public void addTranscript(Transcript val) {
    this.addRef("http://gbol.life#transcript",val);
  }

  public GeneProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,GeneProvenance.class);
  }

  public void setProvenance(GeneProvenance val) {
    this.setRef("http://gbol.life#provenance",val,GeneProvenance.class);
  }

  public void remIntron(Intron val) {
    this.remRef("http://gbol.life#intron",val,true);
  }

  public List<? extends Intron> getAllIntron() {
    return this.getRefSet("http://gbol.life#intron",true,Intron.class);
  }

  public void addIntron(Intron val) {
    this.addRef("http://gbol.life#intron",val);
  }

  public void remMisc_rna(MiscRna val) {
    this.remRef("http://gbol.life#misc_rna",val,true);
  }

  public List<? extends MiscRna> getAllMisc_rna() {
    return this.getRefSet("http://gbol.life#misc_rna",true,MiscRna.class);
  }

  public void addMisc_rna(MiscRna val) {
    this.addRef("http://gbol.life#misc_rna",val);
  }
}
