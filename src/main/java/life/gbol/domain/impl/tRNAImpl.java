package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.tRNA;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class tRNAImpl extends MaturedRNAImpl implements tRNA {
  public static final String TypeIRI = "http://gbol.life#tRNA";

  protected tRNAImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static tRNA make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,tRNA.class);
      if(toRet == null) {
        toRet = new tRNAImpl(domain,iri);;
      }
      else if(!(toRet instanceof tRNA)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.tRNAImpl expected");
      }
      return (tRNA)toRet;
    }
  }

  public void validate() {
  }

  public String getAnticodon() {
    return this.getStringLit("http://gbol.life#anticodon",true);
  }

  public void setAnticodon(String val) {
    this.setStringLit("http://gbol.life#anticodon",val);
  }
}
