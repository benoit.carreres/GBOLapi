package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Scaffold;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ScaffoldImpl extends UncompleteNAObjectImpl implements Scaffold {
  public static final String TypeIRI = "http://gbol.life#Scaffold";

  protected ScaffoldImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Scaffold make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Scaffold.class);
      if(toRet == null) {
        toRet = new ScaffoldImpl(domain,iri);;
      }
      else if(!(toRet instanceof Scaffold)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ScaffoldImpl expected");
      }
      return (Scaffold)toRet;
    }
  }

  public void validate() {
  }
}
