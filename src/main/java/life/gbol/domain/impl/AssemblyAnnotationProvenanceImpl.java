package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.AssemblyAnnotationProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class AssemblyAnnotationProvenanceImpl extends SequenceAnnotationProvenanceImpl implements AssemblyAnnotationProvenance {
  public static final String TypeIRI = "http://gbol.life#AssemblyAnnotationProvenance";

  protected AssemblyAnnotationProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static AssemblyAnnotationProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,AssemblyAnnotationProvenance.class);
      if(toRet == null) {
        toRet = new AssemblyAnnotationProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof AssemblyAnnotationProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.AssemblyAnnotationProvenanceImpl expected");
      }
      return (AssemblyAnnotationProvenance)toRet;
    }
  }

  public void validate() {
  }
}
