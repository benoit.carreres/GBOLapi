package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.TransitPeptide;
import life.gbol.domain.TransitPeptideProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class TransitPeptideImpl extends ProteinFeatureImpl implements TransitPeptide {
  public static final String TypeIRI = "http://gbol.life#TransitPeptide";

  protected TransitPeptideImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static TransitPeptide make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,TransitPeptide.class);
      if(toRet == null) {
        toRet = new TransitPeptideImpl(domain,iri);;
      }
      else if(!(toRet instanceof TransitPeptide)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.TransitPeptideImpl expected");
      }
      return (TransitPeptide)toRet;
    }
  }

  public void validate() {
  }

  public TransitPeptideProvenance getProvenance() {
    return this.getRef("http://gbol.life#provenance",true,TransitPeptideProvenance.class);
  }

  public void setProvenance(TransitPeptideProvenance val) {
    this.setRef("http://gbol.life#provenance",val,TransitPeptideProvenance.class);
  }
}
