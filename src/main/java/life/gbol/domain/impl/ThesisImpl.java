package life.gbol.domain.impl;

import java.lang.Integer;
import java.lang.String;
import life.gbol.domain.Thesis;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class ThesisImpl extends PublicationImpl implements Thesis {
  public static final String TypeIRI = "http://gbol.life#Thesis";

  protected ThesisImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static Thesis make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,Thesis.class);
      if(toRet == null) {
        toRet = new ThesisImpl(domain,iri);;
      }
      else if(!(toRet instanceof Thesis)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.ThesisImpl expected");
      }
      return (Thesis)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://gbol.life#institute");
    this.checkCardMin1("http://gbol.life#year");
  }

  public String getInstitute() {
    return this.getStringLit("http://gbol.life#institute",false);
  }

  public void setInstitute(String val) {
    this.setStringLit("http://gbol.life#institute",val);
  }

  public Integer getYear() {
    return this.getIntegerLit("http://gbol.life#year",false);
  }

  public void setYear(Integer val) {
    this.setIntegerLit("http://gbol.life#year",val);
  }
}
