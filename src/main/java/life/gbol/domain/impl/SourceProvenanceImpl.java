package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.SourceProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public class SourceProvenanceImpl extends NAFeatureProvenanceImpl implements SourceProvenance {
  public static final String TypeIRI = "http://gbol.life#SourceProvenance";

  protected SourceProvenanceImpl(Domain domain, String iri) {
    super(domain,iri);
  }

  public static SourceProvenance make(Domain domain, String iri) {
    synchronized(domain) {
      Object toRet = domain.getObject(iri,SourceProvenance.class);
      if(toRet == null) {
        toRet = new SourceProvenanceImpl(domain,iri);;
      }
      else if(!(toRet instanceof SourceProvenance)) {
        throw new RuntimeException("Instance of life.gbol.domain.impl.SourceProvenanceImpl expected");
      }
      return (SourceProvenance)toRet;
    }
  }

  public void validate() {
  }
}
