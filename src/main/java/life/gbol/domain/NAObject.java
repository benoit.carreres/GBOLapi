package life.gbol.domain;

import java.lang.String;
import java.util.List;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface NAObject extends SequenceObject {
  String getAnticodonmessage();

  void setAnticodonmessage(String val);

  void remFeature(Feature val);

  List<? extends Feature> getAllFeature();

  void addFeature(Feature val);

  String getScientificname();

  void setScientificname(String val);

  StrandType getStrandType();

  void setStrandType(StrandType val);

  Topology getTopology();

  void setTopology(Topology val);

  String getSecondaryAccession();

  void setSecondaryAccession(String val);

  String getMol_type();

  void setMol_type(String val);

  String getCommonname();

  void setCommonname(String val);

  String getDescription();

  void setDescription(String val);

  String getAccession();

  void setAccession(String val);

  String getCitation();

  void setCitation(String val);

  String getDataclass();

  void setDataclass(String val);

  String getDivision();

  void setDivision(String val);
}
