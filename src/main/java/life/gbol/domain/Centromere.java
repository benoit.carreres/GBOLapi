package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Centromere extends RepeatFeature {
  CentromereProvenance getProvenance();

  void setProvenance(CentromereProvenance val);
}
