package life.gbol.domain;

import java.lang.Integer;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface AssemblyGap extends AssemblyAnnotation {
  GapType getGap_type();

  void setGap_type(GapType val);

  LinkageEvidence getLinkage_evidence();

  void setLinkage_evidence(LinkageEvidence val);

  Integer getEstimated_length();

  void setEstimated_length(Integer val);

  AssemblyGapProvenance getProvenance();

  void setProvenance(AssemblyGapProvenance val);
}
