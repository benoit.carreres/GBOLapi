package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface MaturePeptide extends ProteinFeature {
  MaturePeptideProvenance getProvenance();

  void setProvenance(MaturePeptideProvenance val);
}
