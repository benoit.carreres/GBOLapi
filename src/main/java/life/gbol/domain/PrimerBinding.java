package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface PrimerBinding extends ArtificialRecognizedRegion {
  PrimerBindingProvenance getProvenance();

  void setProvenance(PrimerBindingProvenance val);
}
