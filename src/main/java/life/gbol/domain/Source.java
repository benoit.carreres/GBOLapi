package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Source extends NAFeature {
  SourceProvenance getProvenance();

  void setProvenance(SourceProvenance val);
}
