package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface RibosomalBindingSite extends RegulationSite {
  RibosomalBindingSiteProvenance getProvenance();

  void setProvenance(RibosomalBindingSiteProvenance val);
}
