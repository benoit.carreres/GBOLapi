package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface SequenceTaggedSite extends ArtificialRecognizedRegion {
  SequenceTaggedSiteProvenance getProvenance();

  void setProvenance(SequenceTaggedSiteProvenance val);
}
