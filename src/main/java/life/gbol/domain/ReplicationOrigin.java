package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface ReplicationOrigin extends BiologicalRecognizedRegion {
  String getDirection();

  void setDirection(String val);

  ReplicationOriginProvenance getProvenance();

  void setProvenance(ReplicationOriginProvenance val);
}
