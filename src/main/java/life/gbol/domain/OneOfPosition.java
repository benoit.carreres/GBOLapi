package life.gbol.domain;

import java.lang.Integer;
import java.util.List;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface OneOfPosition extends FuzzyPosition {
  void remPosition(Integer val);

  List<? extends Integer> getAllPosition();

  void addPosition(Integer val);
}
