package life.gbol.domain;

import java.lang.Double;
import java.lang.Integer;
import java.lang.String;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface HMM extends ProteinFeatureProvenance {
  Double getTDbias();

  void setTDbias(Double val);

  Double getTDcevalue();

  void setTDcevalue(Double val);

  Double getTDievalue();

  void setTDievalue(Double val);

  String getQuery_accession();

  void setQuery_accession(String val);

  Double getFSscore();

  void setFSscore(Double val);

  Integer getQlen();

  void setQlen(Integer val);

  Double getFSevalue();

  void setFSevalue(Double val);

  Double getTDscore();

  void setTDscore(Double val);

  Double getFSbias();

  void setFSbias(Double val);

  String getDescription();

  void setDescription(String val);

  Integer getTDdomain_number();

  void setTDdomain_number(Integer val);

  Location getENVlocation();

  void setENVlocation(Location val);

  Location getALIlocation();

  void setALIlocation(Location val);

  String getQuery_name();

  void setQuery_name(String val);

  Integer getTDdomains_total();

  void setTDdomains_total(Integer val);

  Double getAccuracy();

  void setAccuracy(Double val);

  Location getHMMlocation();

  void setHMMlocation(Location val);
}
