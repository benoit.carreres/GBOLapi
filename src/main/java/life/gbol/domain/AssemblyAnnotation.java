package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface AssemblyAnnotation extends SequenceAnnotation {
  AssemblyAnnotationProvenance getProvenance();

  void setProvenance(AssemblyAnnotationProvenance val);
}
