package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface MiscBinding extends BiologicalRecognizedRegion {
  MiscBindingProvenance getProvenance();

  void setProvenance(MiscBindingProvenance val);
}
