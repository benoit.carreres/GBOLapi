package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Repeat extends TranscriptFeature {
  Region getRegion();

  void setRegion(Region val);
}
