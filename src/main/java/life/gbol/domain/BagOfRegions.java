package life.gbol.domain;

import java.util.List;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface BagOfRegions extends CollectionOfRegions {
  void remMembers(Region val);

  List<? extends Region> getAllMembers();

  void addMembers(Region val);
}
