package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface JoiningSegment extends ImmunoglobulinFeature {
  JoiningSegmentProvenance getProvenance();

  void setProvenance(JoiningSegmentProvenance val);
}
