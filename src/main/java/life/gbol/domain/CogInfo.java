package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface CogInfo extends ProteinFeatureProvenance {
  String getLetter();

  void setLetter(String val);

  String getLabel();

  void setLabel(String val);

  String getDescription();

  void setDescription(String val);
}
