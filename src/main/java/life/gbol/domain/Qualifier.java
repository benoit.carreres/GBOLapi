package life.gbol.domain;

import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Qualifier extends OWLThing {
  QualifierProvenance getProvenance();

  void setProvenance(QualifierProvenance val);
}
