package life.gbol.domain;

import java.util.List;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Gene extends GeneticElement {
  void remExon(Exon val);

  List<? extends Exon> getAllExon();

  void addExon(Exon val);

  void remTranscript(Transcript val);

  List<? extends Transcript> getAllTranscript();

  void addTranscript(Transcript val);

  GeneProvenance getProvenance();

  void setProvenance(GeneProvenance val);

  void remIntron(Intron val);

  List<? extends Intron> getAllIntron();

  void addIntron(Intron val);

  void remMisc_rna(MiscRna val);

  List<? extends MiscRna> getAllMisc_rna();

  void addMisc_rna(MiscRna val);
}
