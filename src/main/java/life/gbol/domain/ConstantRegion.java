package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface ConstantRegion extends ImmunoglobulinFeature {
  ConstantRegionProvenance getProvenance();

  void setProvenance(ConstantRegionProvenance val);
}
