package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Terminator extends RegulationSite {
  TerminatorProvenance getProvenance();

  void setProvenance(TerminatorProvenance val);
}
