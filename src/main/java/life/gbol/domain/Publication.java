package life.gbol.domain;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Publication extends OWLThing {
  void remXRefs(XRef val);

  List<? extends XRef> getAllXRefs();

  void addXRefs(XRef val);

  String getId();

  void setId(String val);

  String getConsortium();

  void setConsortium(String val);

  String getTitle();

  void setTitle(String val);

  void remAuthors(Person val);

  List<? extends Person> getAllAuthors();

  void addAuthors(Person val);
}
