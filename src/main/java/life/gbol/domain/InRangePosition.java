package life.gbol.domain;

import java.lang.Integer;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface InRangePosition extends FuzzyPosition {
  Integer getEnd();

  void setEnd(Integer val);

  Integer getBegin();

  void setBegin(Integer val);
}
