package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Enhancer extends RegulationSite {
  EnhancerProvenance getProvenance();

  void setProvenance(EnhancerProvenance val);
}
