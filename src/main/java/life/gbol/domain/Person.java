package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Person extends OWLThing {
  String getId();

  void setId(String val);

  String getSurName();

  void setSurName(String val);

  String getFirstName();

  void setFirstName(String val);
}
