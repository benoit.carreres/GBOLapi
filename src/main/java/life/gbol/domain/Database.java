package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Database extends OWLThing {
  String getShortName();

  void setShortName(String val);

  String getBase();

  void setBase(String val);

  String getComment();

  void setComment(String val);
}
