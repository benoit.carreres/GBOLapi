package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface TranscriptFeature extends GeneRelatedElements {
  TranscriptFeatureProvenance getProvenance();

  void setProvenance(TranscriptFeatureProvenance val);
}
