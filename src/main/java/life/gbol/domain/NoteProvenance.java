package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface NoteProvenance extends QualifierProvenance {
  AnnotationResult getOrigin();

  void setOrigin(AnnotationResult val);
}
