package life.gbol.domain;

import java.lang.Double;
import java.lang.String;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Interproscan extends ProteinFeatureProvenance {
  String getSignature_description();

  void setSignature_description(String val);

  Double getScore();

  void setScore(Double val);

  String getVersion();

  void setVersion(String val);

  String getInterpro_description();

  void setInterpro_description(String val);

  String getAnalysis();

  void setAnalysis(String val);
}
