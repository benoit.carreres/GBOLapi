package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface RegulationSite extends BiologicalRecognizedRegion {
  String getRegulatory_class();

  void setRegulatory_class(String val);

  RegulationSiteProvenance getProvenance();

  void setProvenance(RegulationSiteProvenance val);
}
