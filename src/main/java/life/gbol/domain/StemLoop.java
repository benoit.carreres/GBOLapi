package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface StemLoop extends StructureFeature {
  StemLoopProvenance getProvenance();

  void setProvenance(StemLoopProvenance val);
}
