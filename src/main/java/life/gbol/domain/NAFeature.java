package life.gbol.domain;

import java.lang.Boolean;
import java.lang.Integer;
import java.lang.String;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface NAFeature extends Feature {
  String getOnt();

  void setOnt(String val);

  String getGene_type();

  void setGene_type(String val);

  String getTranscript_name();

  void setTranscript_name(String val);

  String getTag();

  void setTag(String val);

  String getExon_number();

  void setExon_number(String val);

  Integer getLevel();

  void setLevel(Integer val);

  String getHavana_transcript();

  void setHavana_transcript(String val);

  String getLocus_tag();

  void setLocus_tag(String val);

  String getGene_status();

  void setGene_status(String val);

  String getGene();

  void setGene(String val);

  String getProduct();

  void setProduct(String val);

  Boolean getPseudo();

  void setPseudo(Boolean val);

  Boolean getPartial();

  void setPartial(Boolean val);

  String getGene_id();

  void setGene_id(String val);

  String getGene_name();

  void setGene_name(String val);

  String getOld_locus_tag();

  void setOld_locus_tag(String val);

  String getGene_synonym();

  void setGene_synonym(String val);

  String getHavana_gene();

  void setHavana_gene(String val);

  String getTranscript_type();

  void setTranscript_type(String val);

  String getTranscript_id();

  void setTranscript_id(String val);

  String getTranscript_status();

  void setTranscript_status(String val);

  String getExon_id();

  void setExon_id(String val);

  NAFeatureProvenance getProvenance();

  void setProvenance(NAFeatureProvenance val);
}
