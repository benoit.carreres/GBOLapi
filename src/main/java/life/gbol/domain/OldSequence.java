package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface OldSequence extends AssemblyAnnotation {
  OldSequenceProvenance getProvenance();

  void setProvenance(OldSequenceProvenance val);
}
