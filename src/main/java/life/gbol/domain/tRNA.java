package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface tRNA extends MaturedRNA {
  String getAnticodon();

  void setAnticodon(String val);
}
