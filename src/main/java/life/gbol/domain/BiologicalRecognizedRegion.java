package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface BiologicalRecognizedRegion extends RecognizedRegion {
  String getBound_moiety();

  void setBound_moiety(String val);

  BiologicalRecognizedRegionProvenance getProvenance();

  void setProvenance(BiologicalRecognizedRegionProvenance val);
}
