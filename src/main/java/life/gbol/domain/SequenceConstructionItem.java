package life.gbol.domain;

import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface SequenceConstructionItem extends OWLThing {
}
