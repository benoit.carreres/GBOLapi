package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface TransitPeptide extends ProteinFeature {
  TransitPeptideProvenance getProvenance();

  void setProvenance(TransitPeptideProvenance val);
}
