package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Operon extends GeneticElement {
  OperonProvenance getProvenance();

  void setProvenance(OperonProvenance val);
}
