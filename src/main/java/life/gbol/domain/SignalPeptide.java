package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface SignalPeptide extends ProteinFeature {
  SignalPeptideProvenance getProvenance();

  void setProvenance(SignalPeptideProvenance val);
}
