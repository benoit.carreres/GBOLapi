package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Minus10Signal extends RegulationSite {
  Minus10SignalProvenance getProvenance();

  void setProvenance(Minus10SignalProvenance val);
}
