package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface ElectronicReference extends Publication {
  String getText();

  void setText(String val);
}
