package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface ItemSequence extends SequenceConstructionItem {
  UncompleteNAObject getSequence();

  void setSequence(UncompleteNAObject val);

  XRef getFromXRef();

  void setFromXRef(XRef val);
}
