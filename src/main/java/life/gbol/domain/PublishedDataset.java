package life.gbol.domain;

import java.lang.String;
import java.util.List;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface PublishedDataset extends Dataset {
  void remKeywords(String val);

  List<? extends String> getAllKeywords();

  void addKeywords(String val);

  String getDefinition();

  void setDefinition(String val);

  String getComment();

  void setComment(String val);

  void remDblinks(XRef val);

  List<? extends XRef> getAllDblinks();

  void addDblinks(XRef val);

  void remReferences(Reference val);

  List<? extends Reference> getAllReferences();

  void addReferences(Reference val);
}
