package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface VariableRegion extends ImmunoglobulinFeature {
  VariableRegionProvenance getProvenance();

  void setProvenance(VariableRegionProvenance val);
}
