package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface CAATSignal extends RegulationSite {
  CAATSignalProvenance getProvenance();

  void setProvenance(CAATSignalProvenance val);
}
