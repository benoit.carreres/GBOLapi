package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface RepeatFeature extends NonGeneRelatedElements {
  String getRpt_type();

  void setRpt_type(String val);

  String getRpt_unit_range();

  void setRpt_unit_range(String val);

  String getMobile_element();

  void setMobile_element(String val);

  RepeatFeatureProvenance getProvenance();

  void setProvenance(RepeatFeatureProvenance val);

  String getRpt_family();

  void setRpt_family(String val);

  String getRpt_unit_seq();

  void setRpt_unit_seq(String val);
}
