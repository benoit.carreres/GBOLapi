package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface TMHMM extends ProteinFeatureProvenance {
  String getSequence();

  void setSequence(String val);

  String getType();

  void setType(String val);

  Location getLocation();

  void setLocation(Location val);
}
