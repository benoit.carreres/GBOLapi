package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Note extends Qualifier {
  String getText();

  void setText(String val);

  NoteProvenance getProvenance();

  void setProvenance(NoteProvenance val);
}
