package life.gbol.domain;

import java.lang.String;
import java.time.LocalDate;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Submission extends Publication {
  LocalDate getDate();

  void setDate(LocalDate val);

  String getSubmitterAddress();

  void setSubmitterAddress(String val);
}
