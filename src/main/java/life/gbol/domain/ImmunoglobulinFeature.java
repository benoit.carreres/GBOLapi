package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface ImmunoglobulinFeature extends ProteinFeature {
  ImmunoglobulinFeatureProvenance getProvenance();

  void setProvenance(ImmunoglobulinFeatureProvenance val);
}
