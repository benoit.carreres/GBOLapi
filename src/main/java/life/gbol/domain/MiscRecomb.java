package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface MiscRecomb extends BiologicalRecognizedRegion {
  MiscRecombProvenance getProvenance();

  void setProvenance(MiscRecombProvenance val);
}
