package life.gbol.domain;

import java.util.List;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface CRISPRCassette extends Gene {
  void remSpacer(Spacer val);

  List<? extends Spacer> getAllSpacer();

  void addSpacer(Spacer val);

  void remRepeat(Repeat val);

  List<? extends Repeat> getAllRepeat();

  void addRepeat(Repeat val);
}
