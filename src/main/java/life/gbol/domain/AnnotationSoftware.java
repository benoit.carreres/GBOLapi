package life.gbol.domain;

import java.lang.String;
import org.w3.ns.prov.domain.SoftwareAgent;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface AnnotationSoftware extends SoftwareAgent {
  String getName();

  void setName(String val);

  String getVersion();

  void setVersion(String val);
}
