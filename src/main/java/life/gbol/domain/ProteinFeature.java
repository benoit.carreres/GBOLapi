package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface ProteinFeature extends Feature {
  ProteinFeatureProvenance getProvenance();

  void setProvenance(ProteinFeatureProvenance val);
}
