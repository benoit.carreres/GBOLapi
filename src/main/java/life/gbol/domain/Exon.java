package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Exon extends GeneticElement {
  ExonProvenance getProvenance();

  void setProvenance(ExonProvenance val);
}
