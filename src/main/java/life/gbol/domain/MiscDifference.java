package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface MiscDifference extends ChangeFeature {
  String getReplace();

  void setReplace(String val);

  String getCompare();

  void setCompare(String val);

  MiscDifferenceProvenance getProvenance();

  void setProvenance(MiscDifferenceProvenance val);
}
