package life.gbol.domain;

import java.util.List;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface ListOfRegions extends CollectionOfRegions {
  Region getMembers(int index);

  List<? extends Region> getAllMembers();

  void addMembers(Region val);

  void setMembers(Region val, int index);

  void remMembers(Region val);
}
