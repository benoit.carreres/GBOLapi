package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life# ontology
 */
public enum StrandPosition implements EnumClass {
  ReverseStrandPosition("http://gbol.life#ReverseStrandPosition",new StrandPosition[]{}),

  ForwardStrandPosition("http://gbol.life#ForwardStrandPosition",new StrandPosition[]{}),

  BothStrandsPosition("http://gbol.life#BothStrandsPosition",new StrandPosition[]{});

  private StrandPosition[] parents;

  private String iri;

  private StrandPosition(String iri, StrandPosition[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static StrandPosition make(String iri) {
    for(StrandPosition item : StrandPosition.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
