package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface XRef extends Qualifier {
  String getSecondaryAccession();

  void setSecondaryAccession(String val);

  Database getDb();

  void setDb(Database val);

  String getAccession();

  void setAccession(String val);

  XRefProvenance getProvenance();

  void setProvenance(XRefProvenance val);
}
