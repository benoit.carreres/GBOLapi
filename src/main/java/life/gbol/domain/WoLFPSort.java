package life.gbol.domain;

import java.lang.Double;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface WoLFPSort extends ProteinFeatureProvenance {
  Double getScore();

  void setScore(Double val);

  Compartments getCompartment();

  void setCompartment(Compartments val);
}
