package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface SwitchRegion extends ImmunoglobulinFeature {
  SwitchRegionProvenance getProvenance();

  void setProvenance(SwitchRegionProvenance val);
}
