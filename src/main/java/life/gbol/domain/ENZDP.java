package life.gbol.domain;

import java.lang.Float;
import java.lang.String;
import java.util.List;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface ENZDP extends ProteinFeatureProvenance {
  String getAmbigous();

  void setAmbigous(String val);

  String getRatio();

  void setRatio(String val);

  void remPattern(String val);

  List<? extends String> getAllPattern();

  void addPattern(String val);

  Float getMaxbitscore();

  void setMaxbitscore(Float val);

  String getSupport();

  void setSupport(String val);

  Float getLikelihoodscore();

  void setLikelihoodscore(Float val);
}
