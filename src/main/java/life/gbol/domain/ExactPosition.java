package life.gbol.domain;

import java.lang.Integer;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface ExactPosition extends Position {
  Integer getPosition();

  void setPosition(Integer val);
}
