package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface InterveningDNA extends GeneticElement {
  InterveningDNAProvenance getProvenance();

  void setProvenance(InterveningDNAProvenance val);
}
