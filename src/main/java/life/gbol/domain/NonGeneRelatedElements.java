package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface NonGeneRelatedElements extends NAFeature {
  NonGeneRelatedElementsProvenance getProvenance();

  void setProvenance(NonGeneRelatedElementsProvenance val);
}
