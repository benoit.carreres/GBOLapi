package life.gbol.domain;

import java.lang.Double;
import java.lang.String;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface RNAmmer extends GeneProvenance {
  Double getScore();

  void setScore(Double val);

  String getAttribute();

  void setAttribute(String val);
}
