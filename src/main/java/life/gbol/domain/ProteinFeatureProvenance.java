package life.gbol.domain;

import java.util.List;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface ProteinFeatureProvenance extends FeatureProvenance {
  void remXref(XRef val);

  List<? extends XRef> getAllXref();

  void addXref(XRef val);

  Location getLocation();

  void setLocation(Location val);
}
