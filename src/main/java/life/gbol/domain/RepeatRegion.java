package life.gbol.domain;

import java.lang.String;
import java.util.List;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface RepeatRegion extends RepeatFeature {
  RepeatRegionProvenance getProvenance();

  void setProvenance(RepeatRegionProvenance val);

  void remInference(String val);

  List<? extends String> getAllInference();

  void addInference(String val);
}
