package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface GeneRelatedElements extends NAFeature {
  String getTrans_splicing();

  void setTrans_splicing(String val);

  GeneRelatedElementsProvenance getProvenance();

  void setProvenance(GeneRelatedElementsProvenance val);

  String getPseudogene();

  void setPseudogene(String val);
}
