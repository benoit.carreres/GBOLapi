package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface PolyASite extends TranscriptFeature {
  PolyASiteProvenance getProvenance();

  void setProvenance(PolyASiteProvenance val);
}
