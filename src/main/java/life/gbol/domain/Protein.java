package life.gbol.domain;

import java.util.List;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Protein extends SequenceObject {
  void remFeature(ProteinFeature val);

  List<? extends ProteinFeature> getAllFeature();

  void addFeature(ProteinFeature val);
}
