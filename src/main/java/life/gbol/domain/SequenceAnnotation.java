package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface SequenceAnnotation extends NAFeature {
  SequenceAnnotationProvenance getProvenance();

  void setProvenance(SequenceAnnotationProvenance val);
}
