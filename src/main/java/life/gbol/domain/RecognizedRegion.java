package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface RecognizedRegion extends NonGeneRelatedElements {
  RecognizedRegionProvenance getProvenance();

  void setProvenance(RecognizedRegionProvenance val);
}
