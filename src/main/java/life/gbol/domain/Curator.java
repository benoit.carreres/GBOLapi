package life.gbol.domain;

import java.lang.String;
import org.w3.ns.prov.domain.Person;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Curator extends Person {
  String getName();

  void setName(String val);

  String getOrcid();

  void setOrcid(String val);
}
