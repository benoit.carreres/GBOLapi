package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface MiscFeature extends NAFeature {
  MiscFeatureProvenance getProvenance();

  void setProvenance(MiscFeatureProvenance val);
}
