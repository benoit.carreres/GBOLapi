package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface UnsureBases extends AssemblyAnnotation {
  String getReplace();

  void setReplace(String val);

  String getCompare();

  void setCompare(String val);

  UnsureBasesProvenance getProvenance();

  void setProvenance(UnsureBasesProvenance val);
}
