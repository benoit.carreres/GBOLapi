package life.gbol.domain;

import java.lang.Integer;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Gap extends AssemblyAnnotation {
  LinkageEvidence getLinkage_evidence();

  void setLinkage_evidence(LinkageEvidence val);

  Integer getEstimated_length();

  void setEstimated_length(Integer val);

  GapProvenance getProvenance();

  void setProvenance(GapProvenance val);
}
