package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface tmRNA extends MaturedRNA {
  String getTag_peptide();

  void setTag_peptide(String val);

  String getAnticodon();

  void setAnticodon(String val);
}
