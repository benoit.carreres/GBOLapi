package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface ncRNA extends MaturedRNA {
  String getNcrna_class();

  void setNcrna_class(String val);
}
