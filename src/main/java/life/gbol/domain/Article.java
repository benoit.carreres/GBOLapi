package life.gbol.domain;

import java.lang.Integer;
import java.lang.String;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Article extends Publication {
  String getIssue();

  void setIssue(String val);

  String getLastPage();

  void setLastPage(String val);

  String getVolume();

  void setVolume(String val);

  Integer getYear();

  void setYear(Integer val);

  String getFirstPage();

  void setFirstPage(String val);

  String getJournal();

  void setJournal(String val);
}
