package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface InBetween extends Location {
  Position getAfter();

  void setAfter(Position val);

  StrandType getStrand();

  void setStrand(StrandType val);
}
