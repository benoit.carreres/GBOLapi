package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface LongTerminalRepeat extends RepeatFeature {
  LongTerminalRepeatProvenance getProvenance();

  void setProvenance(LongTerminalRepeatProvenance val);
}
