package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Intron extends GeneticElement {
  IntronProvenance getProvenance();

  void setProvenance(IntronProvenance val);
}
