package life.gbol.domain;

import java.lang.Double;
import java.lang.Float;
import java.lang.Integer;
import java.lang.String;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Blast extends ProteinFeatureProvenance {
  String getQueryid();

  void setQueryid(String val);

  Integer getSstart();

  void setSstart(Integer val);

  Double getEvalue();

  void setEvalue(Double val);

  Integer getAlignment_length();

  void setAlignment_length(Integer val);

  XRef getXref();

  void setXref(XRef val);

  Cog getCog();

  void setCog(Cog val);

  Float getPercidentity();

  void setPercidentity(Float val);

  Float getBitscore();

  void setBitscore(Float val);

  String getSubjectid();

  void setSubjectid(String val);

  Integer getGaps();

  void setGaps(Integer val);

  Integer getSend();

  void setSend(Integer val);

  Integer getQend();

  void setQend(Integer val);

  String getSubjectname();

  void setSubjectname(String val);

  Integer getMismatches();

  void setMismatches(Integer val);

  Integer getQstart();

  void setQstart(Integer val);
}
