package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Base extends Location {
  StrandType getStrand();

  void setStrand(StrandType val);

  Position getPosition();

  void setPosition(Position val);
}
