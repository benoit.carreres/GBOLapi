package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Phobius extends ProteinFeatureProvenance {
  String getType();

  void setType(String val);

  String getDescription();

  void setDescription(String val);

  Location getLocation();

  void setLocation(Location val);
}
