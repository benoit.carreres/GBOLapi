package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface ProteinBinding extends BiologicalRecognizedRegion {
  ProteinBindingProvenance getProvenance();

  void setProvenance(ProteinBindingProvenance val);
}
