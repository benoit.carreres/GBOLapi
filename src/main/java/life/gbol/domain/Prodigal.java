package life.gbol.domain;

import java.lang.String;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Prodigal extends GeneProvenance {
  String getRbs_spacer();

  void setRbs_spacer(String val);

  String getRbs_motif();

  void setRbs_motif(String val);

  String getStart_type();

  void setStart_type(String val);

  String getGc_cont();

  void setGc_cont(String val);
}
