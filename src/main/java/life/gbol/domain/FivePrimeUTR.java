package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface FivePrimeUTR extends TranscriptFeature {
  FivePrimeUTRProvenance getProvenance();

  void setProvenance(FivePrimeUTRProvenance val);
}
