package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life# ontology
 */
public enum Compartments implements EnumClass {
  Vacuolar_membrane("http://gbol.life#Vacuolar_membrane",new Compartments[]{}),

  Nuclear("http://gbol.life#Nuclear",new Compartments[]{}),

  Chloroplast("http://gbol.life#Chloroplast",new Compartments[]{}),

  Endosomes("http://gbol.life#Endosomes",new Compartments[]{}),

  Plasma_membranes("http://gbol.life#Plasma_membranes",new Compartments[]{}),

  Cytoskeleton("http://gbol.life#Cytoskeleton",new Compartments[]{}),

  Extracellular("http://gbol.life#Extracellular",new Compartments[]{}),

  Endoplasmic_reticulum("http://gbol.life#Endoplasmic_reticulum",new Compartments[]{}),

  Golgi_apparatus("http://gbol.life#Golgi_apparatus",new Compartments[]{}),

  Nucleus("http://gbol.life#Nucleus",new Compartments[]{}),

  Cytosol("http://gbol.life#Cytosol",new Compartments[]{}),

  Lysosomes("http://gbol.life#Lysosomes",new Compartments[]{}),

  Peroxisomes("http://gbol.life#Peroxisomes",new Compartments[]{}),

  Mitochondria("http://gbol.life#Mitochondria",new Compartments[]{});

  private Compartments[] parents;

  private String iri;

  private Compartments(String iri, Compartments[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static Compartments make(String iri) {
    for(Compartments item : Compartments.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
