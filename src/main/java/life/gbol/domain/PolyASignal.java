package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface PolyASignal extends RegulationSite {
  PolyASignalProvenance getProvenance();

  void setProvenance(PolyASignalProvenance val);
}
