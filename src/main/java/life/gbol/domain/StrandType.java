package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life# ontology
 */
public enum StrandType implements EnumClass {
  SingleStrandedDNA("http://gbol.life#SingleStrandedDNA",new StrandType[]{}),

  DoubleStrandedDNA("http://gbol.life#DoubleStrandedDNA",new StrandType[]{}),

  DoubleStrandedRNA("http://gbol.life#DoubleStrandedRNA",new StrandType[]{}),

  ComplementaryDNA("http://gbol.life#ComplementaryDNA",new StrandType[]{DoubleStrandedDNA}),

  SingleStrandedRNA("http://gbol.life#SingleStrandedRNA",new StrandType[]{});

  private StrandType[] parents;

  private String iri;

  private StrandType(String iri, StrandType[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static StrandType make(String iri) {
    for(StrandType item : StrandType.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
