package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface MiscRegulation extends RegulationSite {
  MiscRegulationProvenance getProvenance();

  void setProvenance(MiscRegulationProvenance val);
}
