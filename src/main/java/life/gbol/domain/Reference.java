package life.gbol.domain;

import java.lang.Integer;
import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Reference extends OWLThing {
  void remLocations(Location val);

  List<? extends Location> getAllLocations();

  void addLocations(Location val);

  void remXref(XRef val);

  List<? extends XRef> getAllXref();

  void addXref(XRef val);

  String getComment();

  void setComment(String val);

  String getId();

  void setId(String val);

  Publication getPublication();

  void setPublication(Publication val);

  Integer getReferenceNumber();

  void setReferenceNumber(Integer val);
}
