package life.gbol.domain;

import org.w3.ns.prov.domain.Entity;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface AnnotationResult extends Entity {
  AnnotationActivity getWasGeneratedBy();

  void setWasGeneratedBy(AnnotationActivity val);

  AnnotationSoftware getWasAttributedTo();

  void setWasAttributedTo(AnnotationSoftware val);
}
