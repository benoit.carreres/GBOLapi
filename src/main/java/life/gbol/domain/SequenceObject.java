package life.gbol.domain;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface SequenceObject extends OWLThing {
  void remNote(String val);

  List<? extends String> getAllNote();

  void addNote(String val);

  String getSequence();

  void setSequence(String val);

  void remXref(XRef val);

  List<? extends XRef> getAllXref();

  void addXref(XRef val);

  String getGiaccession();

  void setGiaccession(String val);

  Contig getContig();

  void setContig(Contig val);

  String getSha384();

  void setSha384(String val);

  Sample getSample();

  void setSample(Sample val);

  String getProduct();

  void setProduct(String val);

  String getInference();

  void setInference(String val);
}
