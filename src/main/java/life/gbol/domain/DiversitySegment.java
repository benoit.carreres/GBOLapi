package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface DiversitySegment extends ImmunoglobulinFeature {
  DiversitySegmentProvenance getProvenance();

  void setProvenance(DiversitySegmentProvenance val);
}
