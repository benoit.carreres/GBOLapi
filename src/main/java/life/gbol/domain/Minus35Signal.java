package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Minus35Signal extends RegulationSite {
  Minus35SignalProvenance getProvenance();

  void setProvenance(Minus35SignalProvenance val);
}
