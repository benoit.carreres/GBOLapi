package life.gbol.domain;

import java.lang.Integer;
import java.lang.String;
import java.time.LocalDate;
import java.util.List;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Patent extends Publication {
  LocalDate getDate();

  void setDate(LocalDate val);

  String getPatentType();

  void setPatentType(String val);

  Integer getSequenceNumber();

  void setSequenceNumber(Integer val);

  String getPatentOffice();

  void setPatentOffice(String val);

  void remApplicants(String val);

  List<? extends String> getAllApplicants();

  void addApplicants(String val);

  String getPatenNumber();

  void setPatenNumber(String val);
}
