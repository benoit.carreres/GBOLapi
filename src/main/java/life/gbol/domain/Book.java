package life.gbol.domain;

import java.lang.Integer;
import java.lang.String;
import java.util.List;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Book extends Publication {
  String getBookTitle();

  void setBookTitle(String val);

  String getPublisher();

  void setPublisher(String val);

  String getLastPage();

  void setLastPage(String val);

  Integer getYear();

  void setYear(Integer val);

  String getFirstPage();

  void setFirstPage(String val);

  void remEditors(Person val);

  List<? extends Person> getAllEditors();

  void addEditors(Person val);
}
