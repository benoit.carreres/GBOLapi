package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface GapRegion extends ImmunoglobulinFeature {
  GapRegionProvenance getProvenance();

  void setProvenance(GapRegionProvenance val);
}
