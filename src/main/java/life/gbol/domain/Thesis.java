package life.gbol.domain;

import java.lang.Integer;
import java.lang.String;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Thesis extends Publication {
  String getInstitute();

  void setInstitute(String val);

  Integer getYear();

  void setYear(Integer val);
}
