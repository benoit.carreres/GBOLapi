package life.gbol.domain;

import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface ExonList extends OWLThing {
  Exon getExon(int index);

  List<? extends Exon> getAllExon();

  void addExon(Exon val);

  void setExon(Exon val, int index);

  void remExon(Exon val);
}
