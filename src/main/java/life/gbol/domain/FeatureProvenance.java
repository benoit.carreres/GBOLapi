package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface FeatureProvenance extends Provenance {
  AnnotationResult getOrigin();

  void setOrigin(AnnotationResult val);
}
