package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Attenuator extends RegulationSite {
  AttenuatorProvenance getProvenance();

  void setProvenance(AttenuatorProvenance val);
}
