package life.gbol.domain;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;
import org.w3.ns.prov.domain.Entity;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Provenance extends OWLThing {
  String getNote();

  void setNote(String val);

  Entity getOrigin();

  void setOrigin(Entity val);

  void remReference(Publication val);

  List<? extends Publication> getAllReference();

  void addReference(Publication val);
}
