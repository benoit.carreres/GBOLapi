package life.gbol.domain;

import java.lang.Integer;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface ItemGap extends SequenceConstructionItem {
  Integer getLength();

  void setLength(Integer val);
}
