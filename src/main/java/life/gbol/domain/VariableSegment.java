package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface VariableSegment extends ImmunoglobulinFeature {
  VariableSegmentProvenance getProvenance();

  void setProvenance(VariableSegmentProvenance val);
}
