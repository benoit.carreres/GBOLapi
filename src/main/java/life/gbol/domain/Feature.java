package life.gbol.domain;

import java.lang.Integer;
import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Feature extends OWLThing {
  void remNote(Note val);

  List<? extends Note> getAllNote();

  void addNote(Note val);

  String getFunction();

  void setFunction(String val);

  void remXref(XRef val);

  List<? extends XRef> getAllXref();

  void addXref(XRef val);

  Location getLocation();

  void setLocation(Location val);

  String getMap();

  void setMap(String val);

  Integer getNumber();

  void setNumber(Integer val);

  String getExperiment();

  void setExperiment(String val);

  String getStandard_name();

  void setStandard_name(String val);

  String getAllele();

  void setAllele(String val);

  Integer getCitation();

  void setCitation(Integer val);

  FeatureProvenance getProvenance();

  void setProvenance(FeatureProvenance val);

  String getOperon();

  void setOperon(String val);

  String getInference();

  void setInference(String val);
}
