package life.gbol.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://gbol.life# ontology
 */
public enum GapType implements EnumClass {
  CentromereGap("http://gbol.life#CentromereGap",new GapType[]{}),

  WithinScaffoldGap("http://gbol.life#WithinScaffoldGap",new GapType[]{}),

  BetweenScaffoldsGap("http://gbol.life#BetweenScaffoldsGap",new GapType[]{}),

  ShortArmGap("http://gbol.life#ShortArmGap",new GapType[]{}),

  TelemereGap("http://gbol.life#TelemereGap",new GapType[]{}),

  UnknownGap("http://gbol.life#UnknownGap",new GapType[]{}),

  RepeatWithinScaffoldGap("http://gbol.life#RepeatWithinScaffoldGap",new GapType[]{WithinScaffoldGap}),

  HeterochromatinGap("http://gbol.life#HeterochromatinGap",new GapType[]{}),

  RepeatBetweeenScaffoldsGap("http://gbol.life#RepeatBetweeenScaffoldsGap",new GapType[]{});

  private GapType[] parents;

  private String iri;

  private GapType(String iri, GapType[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static GapType make(String iri) {
    for(GapType item : GapType.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
