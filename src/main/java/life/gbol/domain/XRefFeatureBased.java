package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface XRefFeatureBased extends XRefProvenance {
  Feature getDerivedFrom();

  void setDerivedFrom(Feature val);
}
