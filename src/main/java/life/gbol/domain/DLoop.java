package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface DLoop extends BiologicalRecognizedRegion {
  DLoopProvenance getProvenance();

  void setProvenance(DLoopProvenance val);
}
