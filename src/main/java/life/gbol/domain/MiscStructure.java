package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface MiscStructure extends StructureFeature {
  MiscStructureProvenance getProvenance();

  void setProvenance(MiscStructureProvenance val);
}
