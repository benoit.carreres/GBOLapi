package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface ChangeFeature extends SequenceAnnotation {
  ChangeFeatureProvenance getProvenance();

  void setProvenance(ChangeFeatureProvenance val);
}
