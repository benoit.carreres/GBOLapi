package life.gbol.domain;

import java.time.LocalDate;
import org.w3.ns.prov.domain.Activity;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface AnnotationActivity extends Activity {
  LocalDate getStartedAtTime();

  void setStartedAtTime(LocalDate val);

  LocalDate getEndedAtTime();

  void setEndedAtTime(LocalDate val);
}
