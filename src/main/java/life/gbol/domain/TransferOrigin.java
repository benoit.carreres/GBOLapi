package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface TransferOrigin extends BiologicalRecognizedRegion {
  TransferOriginProvenance getProvenance();

  void setProvenance(TransferOriginProvenance val);
}
