package life.gbol.domain;

import java.lang.Boolean;
import java.lang.Integer;
import java.lang.String;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface CDS extends TranscriptFeature {
  String getTransl_except();

  void setTransl_except(String val);

  Boolean getRibosomal_slippage();

  void setRibosomal_slippage(Boolean val);

  Integer getCodon_start();

  void setCodon_start(Integer val);

  String getArtificial_location();

  void setArtificial_location(String val);

  String getProtein_id();

  void setProtein_id(String val);

  String getException();

  void setException(String val);

  Protein getProtein();

  void setProtein(Protein val);

  String getGene();

  void setGene(String val);

  Integer getTransl_table();

  void setTransl_table(Integer val);

  CDSProvenance getProvenance();

  void setProvenance(CDSProvenance val);
}
