package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Variation extends ChangeFeature {
  VariationProvenance getProvenance();

  void setProvenance(VariationProvenance val);
}
