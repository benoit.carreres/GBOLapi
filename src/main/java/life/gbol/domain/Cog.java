package life.gbol.domain;

import java.lang.Integer;
import java.lang.String;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Cog extends Blast {
  String getLetter();

  void setLetter(String val);

  String getLabel();

  void setLabel(String val);

  String getOrganism();

  void setOrganism(String val);

  String getProteinid();

  void setProteinid(String val);

  String getMembershipclass();

  void setMembershipclass(String val);

  String getDescription();

  void setDescription(String val);

  CogInfo getCoginfo();

  void setCoginfo(CogInfo val);

  String getCogid();

  void setCogid(String val);

  Integer getEnd();

  void setEnd(Integer val);

  Integer getProteinlength();

  void setProteinlength(Integer val);

  Integer getBegin();

  void setBegin(Integer val);
}
