package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface GeneticElement extends GeneRelatedElements {
  GeneticElementProvenance getProvenance();

  void setProvenance(GeneticElementProvenance val);
}
