package life.gbol.domain;

/**
 * Code generated from http://gbol.life# ontology
 */
public interface Telomere extends RepeatFeature {
  TelomereProvenance getProvenance();

  void setProvenance(TelomereProvenance val);
}
